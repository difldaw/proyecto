<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidad_de_Medida extends Model
{
    use SoftDeletes;

    //Campos llenables en tabla Unidad de Medida
    protected $fillable = ['nombre'];

    //Varable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "unidades_de_medida"
    protected $table = 'unidades_de_medida';

    /* Función que devuelve el model de Producto relacionada con la Unidad de Medida
    Función relación 1 (Producto) a N (Unidad de Medida) */
    public function productos()
    {
        return $this->hasMany('App\Producto');
    }
}
