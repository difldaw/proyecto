<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
//Route::resource('foodprogram','FoodProgramController');

Route::group(['middleware' => ['web']], function () {
    //
});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');
    Route::resource('usuario', 'UsuarioController');
    Route::get('/perfil', [
      'as' => 'profile', 'uses' => 'UsuarioController@perfil']);

    Route::get('/usuario/habilitar/{id}', 'UsuarioController@habilitaUsuario')->name('habilitaUsuario');
    Route::get('/perfil/{id}', 'UsuarioController@perfil')->name('perfil');

    //Carla
    Route::resource('escuela','EscuelaController');
    //Route::get('escuela/reac/{id}', 'EscuelaController@habilitaEscuela');
    Route::get('/escuela/habilitar/{id}', 'EscuelaController@habilitaEscuela')->name('habilitaEscuela');

    Route::resource('despensa','DespensaController');
    Route::get('/despensa/habilitar/{id}', 'DespensaController@habilitaDespensa')->name('habilitaDespensa');



    //Armando Productos
    Route::resource('producto', 'ProductoController');
    Route::get('producto/reac/{id}', 'ProductoController@reactivate');

    //Armando
    Route::resource('localidad', 'LocalidadController');
    Route::get('localidad/reac/{id}', 'LocalidadController@reactivate');


    //Alejandro
    Route::resource('programa','ProgramaController');
    Route::get('programa/reac/{id}', 'ProgramaController@habilitaPrograma');

    Route::resource('provedor','ProvedorController');
    Route::get('provedor/reac/{id}', 'ProvedorController@habilitaProvedor');

    Route::resource('contrato','ContratoController');
    Route::get('contrato/reac/{id}', 'ContratoController@habilitaContrato');

    //Armando, Ordenes
    Route::resource('ordenes','OrdendeComprasController');


});
