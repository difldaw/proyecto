<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contrato;
use App\Provedor;
use DB;

class ContratoController extends Controller
{
  /**
  *Display a listening of the resource.
  *
  *@return IlluminateHttpResponse
  */

  public function index(){
    //
    $todas = DB::table('contratos')->paginate(12);
    $contratos = DB::table('contratos')->whereNull('deleted_at')->paginate(12);
    return view('contratos.index', ['contratos' => $contratos, 'todas'=>$todas]);
    //$contrato = Contrato::paginate(10);

    //return $foodprogram;

    return view('contratos.index')->with('contratos', $contrato);


  }

  /**
  *Show the form for creating a new resource.
  *
  *@return \Illuminate\Http\Response
  */
  public function create(){

        $provedores = Provedor::all();
        return view('contratos.create')
                ->with('provedores', $provedores); //Regresa la vista del create
      //return view('contratos.create');


  }

  /**
  *Store a newly created resource in storage.
  *
  *@param \Illuminate\Http\Request $request
  *@return \Illuminate\Http\Response
  */

  public function store(Request $request){

    /*
    $foodprogram = new Foodprogram;
    $foodprogram->name = $request->name;
    $foodprogram->tipo = $request->tipo;
    $foodprogram->save();
    */
    $inputs = $request -> all();
    //return $inputs;
    $id=$request->input('provedor_id');
    $contrato = Contrato::create($inputs);
    //return redirect()->route('provider',[9]);
    //return redirect()->route('provider.index');
    return redirect()->action('ProvedorController@show', [$id]);
    //return redirect()->action('ContractController@index');
    //return redirect()->route('foodprogram.index');

  }

  /**
  *Display the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function show($id){
    $contrato = Contrato::find($id);
    return view('contratos.show')->with('contrato', $contrato);
  }

  /**
  *Show the form for editing the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function edit($id){
    $contrato = Contrato::find($id);
    //$foodprogram = FoodProgram::where('id', $id)->first();
    //return $foodprogram;
    return view('contratos.edit',compact('contrato', $contrato));
  }

  /**
  *Update the specified resource in storage
  *
  *@param \Illuminate\Http\Request $Request
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function update(Request $request, $id){

    $contrato = Contrato::find($id);

    $contrato->nombre = $request->nombre;
    $contrato->fechainicio = $request->fechainicio;
    $contrato->fechafin = $request->fechafin;
    $contrato->montoasignado = $request->montoasignado;
    $contrato->observacion = $request->observacion;
    $contrato->provedor_id = $request->provedor_id;

    $contrato->save();

    return redirect()->action('ProvedorController@show', [$contrato->provedor_id]);
  }

  public function habilitaContrato($id){
    $contrato= Contrato::withTrashed()->find($id);
    $contrato->deleted_at=NULL;
    $contrato->save();
    return view('contratos.show')->with('contrato',$contrato);
  }

  /**
  *Remove the specified resource from storage
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function destroy($id){
    //FoodProgram::destroy($id);
    $contrato = Contrato::find($id)->delete();
    return redirect()->route('provedor.index');
  }

}
