<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Municipio;
use DB;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $todos = User::withTrashed()->paginate(12);
      $usuarios = User::paginate(12);
      return view('usuarios.index',['todos'=>$todos, 'usuarios' => $usuarios]);
    }

    public function show($id){
      $usuario = User::withTrashed()->find($id);
      return view('usuarios.show')->with('usuario',$usuario);
    }

    public function create(){
      $municipios = Municipio::all();
      return view('usuarios.create')->with('municipios', $municipios);
    }

    public function edit($id){
      $municipios = Municipio::all();
      $usuario= User::find($id);
      return view('usuarios.edit', ['municipios'=> $municipios, 'usuario'=>$usuario]);
    }

    public function destroy($id){
      $usuario= User::find($id)->delete();
      $usuario = User::withTrashed()->find($id);
      return redirect()->action('UsuarioController@show',$usuario);
    }

    public function store(Request $request){
      $request->merge(['password' => bcrypt($request->password)]);
      $inputs = $request->all();
      $usuario = User::create($inputs);
      $usuario->save();
      return redirect()->action('UsuarioController@index');
    }


    public function habilitaUsuario($id){
      $usuario = User::withTrashed()->find($id);
      $usuario->deleted_at=NULL;
      $usuario->save();
      return redirect()->action('UsuarioController@show',$usuario);
    }

    public function update($id, Request $request){
      $nueva_pass = $request->get('password');
      $usuario = User::find($id);

      if(empty($nueva_pass)){
        $usuario->update($request->except('password'));
      }else{
        $request->merge(['password' => bcrypt($request->password)]);
        $inputs = $request->all();
        $usuario->update($request->all());
      }

      return redirect()->action('UsuarioController@show',$id);
    }

    public function perfil($id){
      $usuario = User::find($id);
      return view('usuarios.show')->with('usuario',$usuario);
    }


}
