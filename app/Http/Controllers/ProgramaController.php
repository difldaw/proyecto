<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Programa;
use App\Vulnerable;
use DB;

class ProgramaController extends Controller
{
  /**
  *Display a listening of the resource.
  *
  *@return IlluminateHttpResponse
  */

  public function index(){
    //

    $todas = DB::table('programas')->paginate(12);
    $programas = DB::table('programas')->whereNull('deleted_at')->paginate(12);
    return view('programas.index', ['programas' => $programas, 'todas'=>$todas]);

    //$programa = Programa::paginate(10);

    //return $foodprogram;

    return view('programas.index')->with('programas', $programa);


  }

  /**
  *Show the form for creating a new resource.
  *
  *@return \Illuminate\Http\Response
  */
  public function create(){

      $vulnerables = Vulnerable::all();
      return view('programas.create')
              ->with('vulnerables', $vulnerables);


  }

  /**
  *Store a newly created resource in storage.
  *
  *@param \Illuminate\Http\Request $request
  *@return \Illuminate\Http\Response
  */

  public function store(Request $request){


      $programa = new Programa;
      $programa->id_programa=$request->nombre;
      $programa->save();

      $input = $request->input();
      $arr = $input['id_vulnerable'];
      $programa->vulnerables()->attach($arr);

     return view('programas.show')->with('programa', $programa);
  }

  /**
  *Display the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function show($id){
    //$programa = Programa::find($id);
    //return view('programas.show')->with('programa', $programa);
    $programa= Programa::withTrashed()->where('id_programa', '=', $id)->first();

    return view('programas.show', ['programa'=> $programa ]);
  }

  /**
  *Show the form for editing the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function edit($id){
    $programa = Programa::find($id);
    //$foodprogram = FoodProgram::where('id', $id)->first();
    //return $foodprogram;
    return view('programas.edit',compact('programa', $programa));
  }

  /**
  *Update the specified resource in storage
  *
  *@param \Illuminate\Http\Request $Request
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function update(Request $request, $id){

    $programa = Programa::find($id);

    $programa->tipo = $request->tipo;
    $programa->vulnerable = $request->vulnerable;

    $programa->save();

    return redirect()->route('programa.index');
  }

  public function habilitaPrograma($id){
    $programa= Programa::withTrashed()->find($id);
    $programa->deleted_at=NULL;
    $programa->save();
    return view('programas.show')->with('programa',$programa);
  }

  /**
  *Remove the specified resource from storage
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function destroy($id){
    //FoodProgram::destroy($id);
    $programa = Programa::find($id)->delete();
    return redirect()->route('programa.index');
  }

}
