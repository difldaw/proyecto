<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Localidad;
use App\Municipio; //Model de Municipio para poder obtener la información de municipios
use App\Grado_de_Marginacion; //Model de Grados de Marginación para poder obtener la información de grados de marginación
use DB;

class LocalidadController extends Controller
{
  //Función para mostrar la vista "index"
  public function index(){
    $todas = Localidad::withTrashed()->paginate(10); //Se obtienen todas las localidades activa/inactivas
    $localidades = Localidad::whereNull('deleted_at')->paginate(10); //Se obtienen todas las localidades activas
    return view('localidades.index')
            ->with('localidades', $localidades)
            ->with('todas', $todas); //Regresa la vista index
  }

  //Función mostrar la vista "create"
  public function create(){
    //Se obtiene la información de los dos modelos
    $municipios = Municipio::all();
    $grados_de_marginacion = Grado_de_Marginacion::all();
    return view('localidades.create')
            ->with('municipios', $municipios)
            ->with('grados_de_marginacion', $grados_de_marginacion); //Regresa la vista del create
  }

  //Función para almacenar una localidad en la BD
  public function store(Request $request){
    //Se recibe los inputs que tiene el request
    $inputs = $request->all();
    //Se crea una localidad con los inputs
    $localidad = Localidad::Create($inputs);
    //Se salva la localidad
    $localidad->save();
    //Ejecuta la función index del LocalidadController
    return redirect()->action('LocalidadController@index');
  }

  //Función para mostrar la vista "show" de una localidad en específica
  public function show($id){
    //Se obtiene la localidad deseada
    $localidad = Localidad::withTrashed()->where('id', '=', $id)->first();
    return view('localidades.show')->with('localidad', $localidad);
  }

  //Función para mostrar la vista de "edit" de una localidad en específica
  public function edit($id){
    //Se obtiene la información de la localidad deseada
    $localidad = Localidad::where('id', '=', $id)->first();
    //Se obtiene tablas adicionales para los campos que se necesitan
    $municipios = Municipio::all();
    $grados_de_marginacion = Grado_de_Marginacion::all();

    return view('localidades.edit')->with('localidad', $localidad)
            ->with('municipios', $municipios)
            ->with('grados_de_marginacion', $grados_de_marginacion);
  }

  //Función para actualizar una localidad en la BD
  public function update(Request $request, $id){
    //Se obtiene la localidad que se quiere actualizar
    $localidad = Localidad::find($id);
    //Se actualiza cada campo
    $localidad->nombre = $request->nombre;
    $localidad->latitud = $request->latitud;
    $localidad->longitud = $request->longitud;
    $localidad->altitud = $request->altitud;
    $localidad->clave_carta = $request->clave_carta;
    $localidad->clave_localidad = $request->clave_localidad;
    $localidad->municipio_id = $request->municipio_id;
    $localidad->grado_de_marginacion_id = $request->grado_de_marginacion_id;
    //Función para guardar la modificación y actualizar sus relaciones
    $localidad->push();

    return view('localidades.show')->with('localidad', $localidad);
  }

  //Función que para hacer un Softdelete a una localidad en específica
  public function destroy($id){
    //Se obtiene y elimina la localidad deseada
    $localidad = Localidad::find($id)->delete();
    //Se ejecuta la función index de LocalidadController
    return redirect()->action('LocalidadController@index');
  }

  //Función para reactivar una localidad con Softdelete
  public function reactivate($id){
    //Se obtiene la localidad con Softdelete
    $localidad = Localidad::withTrashed()->find($id);
    //Se asigna NULL a la propiedad delete_at de la localidad
    $localidad->deleted_at = NULL;
    //Se guarda la modificación
    $localidad->save();
    return view('localidades.show')->with('localidad', $localidad);
  }

}
