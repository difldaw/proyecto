<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Escuela;
use DB;


class EscuelaController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index(){
        //consultas para las dos tablas con resultados eliminados y sin resultados eliminados
        $todas = DB::table('escuelas')->paginate(12);
        $escuelas = DB::table('escuelas')->whereNull('deleted_at')->paginate(12);
        return view('escuelas.index', ['escuelas' => $escuelas, 'todas'=>$todas]);
    }

    public function create(){
      return view('escuelas.create');
    }

    public function store(Request $request){
      $inputs= $request->all();
      $escuela= Escuela::Create($inputs);
      $escuela->save();
      return redirect()->action('EscuelaController@index');
    }

    public function show($id){
      $escuela= DB::table('escuelas')->where('id_escuela','=',$id)->first();
      return view('escuelas.show')->with('escuela',$escuela);

    }
    public function destroy($id){
      $escuela= Escuela::where('id_escuela','=',$id)->delete();
      return redirect()->action('EscuelaController@index');
    }

    public function edit($id){
      $escuela= Escuela::where('id_escuela','=',$id)->first();
      return view('escuelas.edit')->with('escuela',$escuela);
    }

    public function update(Request $request, $id){
      $escuela=  Escuela::find($id);
      $escuela->nombre=$request->nombre;
      $escuela->clave=$request->clave;
      $escuela->domicilio=$request->domicilio;
      $escuela->colonia=$request->colonia;
      $escuela->codigoPostal=$request->codigoPostal;
      $escuela->latitud=$request->latitud;
      $escuela->longitud=$request->longitud;
      $escuela->telefono=$request->telefono;
      $escuela->delegacion=$request->delegacion;
      $escuela->localidad_id=$request->localidad_id;
      $escuela->municipio_id=$request->municipio_id;
      $escuela->turno_id=$request->turno_id;
      $escuela->zona_id=$request->zona_id;
      $escuela->sector_id=$request->sector_id;
      $escuela->gradoEscolar_id=$request->gradoEscolar_id;
      $escuela->servicio_id=$request->servicio_id;
      $escuela->sostenimiento_id=$request->sostenimiento_id;
      $escuela->director=$request->director;
      $escuela->directorCongrupo=$request->directorCongrupo;
      $escuela->alumnosNinas=$request->alumnosNinas;
      $escuela->alumnosNinos=$request->alumnosNinos;
      $escuela->alumnosPrimero=$request->alumnosPrimero;
      $escuela->alumnosSegundo=$request->alumnosSegundo;
      $escuela->alumnosTercero=$request->alumnosTercero;
      $escuela->alumnosCuarto=$request->alumnosCuarto;
      $escuela->alumnosQuinto=$request->alumnosQuinto;
      $escuela->alumnosSexto=$request->alumnosSexto;
      $escuela->alumnosLactantes=$request->alumnosLactantes;
      $escuela->alumnosMaternal=$request->alumnosMaternal;
      $escuela->gruposPrimero=$request->gruposPrimero;
      $escuela->gruposSegundo=$request->gruposSegundo;
      $escuela->gruposTercero=$request->gruposTercero;
      $escuela->gruposCuarto=$request->gruposCuarto;
      $escuela->gruposQuinto=$request->gruposQuinto;
      $escuela->gruposSexto=$request->gruposSexto;
      $escuela->gruposLactantes=$request->gruposLactantes;
      $escuela->gruposMaternal=$request->gruposMaternal;
      $escuela->docentesMujeres=$request->docentesMujeres;
      $escuela->docentesHombres=$request->docentesHombres;
      $escuela->docentesPrimero=$request->docentesPrimero;
      $escuela->docentesSegundo=$request->docentesSegundo;
      $escuela->docentesTercero=$request->docentesTercero;
      $escuela->docentesCuarto=$request->docentesCuarto;
      $escuela->docentesQuinto=$request->docentesQuinto;
      $escuela->docentesSexto=$request->docentesSexto;
      $escuela->docentesLactantes=$request->docentesLactantes;
      $escuela->docentesMaternal=$request->docentesMaternal;
      $escuela->personal=$request->personal;
      $escuela->aulas=$request->aulas;
      $escuela->save();
      return view('escuelas.show')->with('escuela',$escuela);
    }

    //funcion para reestablecer columna de softdelete de escuelas
    public function habilitaEscuela($id){
      $escuela= Escuela::withTrashed()->find($id);
      $escuela->deleted_at=NULL;
      $escuela->save();
      return view('escuelas.show')->with('escuela',$escuela);
    }

    //funciones de obtencion de llaves foraneas para vistas deescuelas
    public function getMunicipio($id){
      $municipios = DB::table('municipios')->where('id_municipio', $id)->get();
      return $municipios;
    }

    public function getLocalidad($id){
      $localidades = DB::table('localidades')->where('id_localidad', $id)->get();
      return $localidades;
    }
    public function getZona($id){
      $zonas = DB::table('zonas')->where('id_zona', $id)->get();
      return $zonas;
    }
    public function getTurno($id){
      $turnos = DB::table('turnos')->where('id_turno', $id)->get();
      return $turnos;
    }
    public function getSector($id){
      $sectores = DB::table('sectores')->where('id_sector', $id)->get();
      return $sectores;
    }
    public function getGrado($id){
      $grados = DB::table('gradosescolares')->where('id_gradoEscolar', $id)->get();
      return $grados;
    }

    //funciones para obtencion de catalogos necesarios
    public function getMunicipios(){
      $municipios = DB::table('municipios')->get();
      return $municipios;
    }
    public function getLocalidades(){
      $localidades = DB::table('localidades')->get();
      return $localidades;
    }
    public function getZonas(){
      $zonas = DB::table('zonas')->get();
      return $zonas;
    }
    public function getTurnos(){
      $turnos = DB::table('turnos')->get();
      return $turnos;
    }
    public function getSectores(){
      $sectores = DB::table('sectores')->get();
      return $sectores;
    }
    public function getGrados(){
      $grados = DB::table('gradosescolares')->get();
      return $grados;
    }
    public function getServicios(){
      $servicios = DB::table('servicios')->get();
      return $servicios;
    }
    public function getSostenimientos(){
      $sostenimientos = DB::table('sostenimientos')->get();
      return $sostenimientos;
    }


}
