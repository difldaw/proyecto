<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Provedor;
use App\Estado;
use App\Contrato;
use DB;

class ProvedorController extends Controller
{
  /**
  *Display a listening of the resource.
  *
  *@return IlluminateHttpResponse
  */

  public function index(){
    //
    $todas = Provedor::withTrashed()->paginate(10);;
    $provedores = Provedor::whereNull('deleted_at')->paginate(10);
    return view('provedores.index', ['provedores' => $provedores, 'todas'=>$todas]);
    //$provedor = Provedor::paginate(10);

    //return $foodprogram;

    //return view('provedores.index')->with('provedores', $provedor);


  }

  /**
  *Show the form for creating a new resource.
  *
  *@return \Illuminate\Http\Response
  */
  public function create(){

    $estados = Estado::all();
    return view('provedores.create')
            ->with('estados', $estados); //Regresa la vista del create




  }

  /**
  *Store a newly created resource in storage.
  *
  *@param \Illuminate\Http\Request $request
  *@return \Illuminate\Http\Response
  */

  public function store(Request $request){

    /*
    $foodprogram = new Foodprogram;
    $foodprogram->name = $request->name;
    $foodprogram->tipo = $request->tipo;
    $foodprogram->save();
    */
    $inputs = $request -> all();
    //return $inputs;
    $provedor = Provedor::create($inputs);

    return redirect()->action('ProvedorController@index');
    //return redirect()->route('foodprogram.index');

  }

  /**
  *Display the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function show($id){

    $provedor= Provedor::withTrashed()->where('id_provedor', '=', $id)->first();


    $todas = Contrato::withTrashed()->paginate(10);;
    $contratos = Contrato::whereNull('deleted_at')->paginate(10);


    //$todas = DB::table('contratos')->paginate(12);
    //$contratos = DB::table('contratos')->whereNull('deleted_at')->paginate(12);
    return view('provedores.show', ['contratos' => $contratos, 'todas'=>$todas, 'provedor'=> $provedor ]);


    //$provedor = Provedor::find($id);
    //return view('provedores.show')->with('provedor', $provedor);
  }

  /**
  *Show the form for editing the specified resource.
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function edit($id){
    $provedor = Provedor::find($id);
    //$foodprogram = FoodProgram::where('id', $id)->first();
    //return $foodprogram;
    return view('provedores.edit',compact('provedor', $provedor));
  }

  /**
  *Update the specified resource in storage
  *
  *@param \Illuminate\Http\Request $Request
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function update(Request $request, $id){

    $provedor = Provedor::find($id);

    $provedor->nombre = $request->nombre;
    $provedor->rfc = $request->rfc;
    $provedor->razonsocial = $request->razonsocial;
    $provedor->correo = $request->correo;
    $provedor->telefono = $request->telefono;
    $provedor->domicilio = $request->domicilio;
    $provedor->estado = $request->estado;

    $provedor->save();

    return redirect()->route('provedor.index');
  }


  public function habilitaProvedor($id){
    $provedor= Provedor::withTrashed()->find($id);
    $provedor->deleted_at=NULL;
    $provedor->save();
    return redirect()->route('provedor.index');
  }

  public function habilitaContrato($id){
    $contrato= Contrato::withTrashed()->find($id);
    $contrato->deleted_at=NULL;
    $contrato->save();
    return view('provedores.show')->with('contrato',$contrato);
  }
  /**
  *Remove the specified resource from storage
  *
  *@param int $id
  *@return \Illuminate\Http\Response
  */

  public function destroy($id){
    //FoodProgram::destroy($id);
    $provedor = Provedor::find($id)->delete();
    return redirect()->route('provedor.index');
  }

}
