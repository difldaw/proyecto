<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Despensa;
use App\Programa;
use App\Producto;


class DespensaController extends Controller
{

    public function index(){

        $despensas = Despensa::withTrashed()->orderBy('id_despensa', 'desc')->paginate(8);
        return view('despensas.index')
                ->with('despensas', $despensas);

    }

    public function create(){

      $productos = Producto::all();
      $programas = Programa::all();
      return view('despensas.create')
              ->with('productos', $productos)
              ->with('programas', $programas); //Regresa la vista del create
    }



    public function store(Request $request){

      $despensa = new Despensa;
      $despensa->programa_id = $request->programa_id;
      $despensa->save();
      //relacion
      $productos = Producto::all();
      foreach($productos as $producto){

        $cantDiariaNino="cantDiariaNino".$producto->id;
        $diasEntrega="diasEntrega".$producto->id;
        if ($request->has($cantDiariaNino) && $request->has($diasEntrega) ) {
            $despensa->productos()->attach($producto->id, ['cantDiariaNino' => $request->$cantDiariaNino,'diasEntrega' => $request->$diasEntrega]);
        }
      }

     return view('despensas.show')->with('despensa', $despensa);
    }

    public function show($id){
      //Se obtiene la despensa deseada
      $despensa = Despensa::withTrashed()->where('id_despensa', '=', $id)->first();
      return view('despensas.show')->with('despensa', $despensa);

    }


    public function edit($id){

      $despensa = Despensa::where('id_despensa', '=', $id)->first();
      $productostodos = Producto::all();
      $programas = Programa::all();

      return view('despensas.edit')
              ->with('despensa', $despensa)
              ->with('productostodos', $productostodos)
              ->with('programas', $programas);

    }

    public function update(Request $request, $id){

      $despensa = Despensa::find($id);
      $productostodos = Producto::all();

      //tabla foreign key
      $despensa->programa_id = $request->programa_id;
      $despensa->push();
      $relacionadas=array();
      //RELACIONES
      foreach($productostodos as $productotodo){


        foreach($despensa->productos as $producto){
          $relacionadas[$producto->id]=$producto->id;
        }

          if(in_array($productotodo->id,$relacionadas)){

            $cantDiariaNino="cantDiariaNino".$productotodo->id;
            $diasEntrega="diasEntrega".$productotodo->id;
            if($request->has($cantDiariaNino) || $request->has($diasEntrega)){
              //dd($request->$cantDiariaNino);
              $pivots = array('cantDiariaNino' => $request->$cantDiariaNino ,'diasEntrega' => $request->$diasEntrega );
              $despensa->productos()->updateExistingPivot($productotodo->id,$pivots);
            }

          }
          //si no esta relacionada
          else{
            //dd("no");
            $cantDiariaNino="cantDiariaNino".$productotodo->id;
            $diasEntrega="diasEntrega".$productotodo->id;
            if($request->has($cantDiariaNino) && $request->has($diasEntrega)){
                  $despensa->productos()->attach($productotodo->id, ['cantDiariaNino' => $request->$cantDiariaNino,'diasEntrega' => $request->$diasEntrega]);
            }
            //si esta relacionada
          }


      }


      return redirect()->action('DespensaController@show',$despensa);
    }

    public function destroy($id){
      //Se obtiene y elimina el producto deseada
      $despensa = Despensa::find($id)->delete();
      //Se ejecuta la función index de ProductoController
      return redirect()->action('DespensaController@index');
    }
    //Función para reactivar un producto con Softdelete
    public function habilitaDespensa($id){
      $despensa= Despensa::withTrashed()->find($id);
      $despensa->deleted_at=NULL;
      $despensa->save();
      return view('despensas.show')->with('despensa',$despensa);
    }





}
