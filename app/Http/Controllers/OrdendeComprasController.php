<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ordendecompra;
use App\Despensa;
use App\Municipio;
use App\Programa;
use DB;

class OrdendeComprasController extends Controller
{
   //Función para mostrar la vista "index"
   public function index(){
     $ordenes_de_compra = Ordendecompra::paginate(10); //Se obtienen todas los ordenes de compra

     return view('ordenes.index')
             ->with('ordenes_de_compra', $ordenes_de_compra); //Regresa la vista index
   }

   //Función mostrar la vista "create"
   public function create(){
     //Se obtiene la información de los dos modelos
     $municipios = Municipio::all();
     $programas = Programa::all();
     $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                     'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
                     'Diciembre');
      $anios = array();
      for ($i=2000; $i <= date("Y") ; $i++) {
         array_push($anios, $i);
      }

     return view('ordenes.create')
             ->with('municipios', $municipios)
             ->with('programas', $programas)
             ->with('anios', $anios)
             ->with('meses', $meses); //Regresa la vista del create
   }

   //Función para almacenar un producto en la BD
   public function store(Request $request){
     //Se recibe los inputs que tiene el request
     $inputs = $request->all();
     //Se crea un producto con los inputs
     $orden_de_compra = Ordendecompra::Create($inputs);
     //Se salva
     $orden_de_compra->save();

     $programa_id = $orden_de_compra->programa_id;

     $despensa = Despensa::where('programa_id', $programa_id)->orderBy('id_despensa', 'desc')->first();
     $productos = $despensa->productos;

     foreach ($productos as $producto) {
        $orden_de_compra->productos()->attach($producto->id, ['precio' => $producto->precio]);
     }


     //Ejecuta la función index del OrdendeComprasController
     return redirect()->action('OrdendeComprasController@index');
   }

   //Función para mostrar la vista "show" de una orden en específica
   public function show($id){
     //Se obtiene el producto deseada
     $orden_de_compra = Ordendecompra::find($id)->first();
     $programa_id = $orden_de_compra->programa_id;
     $productos = $orden_de_compra->productos;
     $i = 0;
     $despensa = Despensa::where('programa_id', $programa_id)->orderBy('id_despensa', 'desc')->first();
     $datas = $despensa->productos;

     $cantidades = array();
     $precios = array();

     foreach ($datas as $data) {
        $cantidad = (($data->pivot->diasEntrega)*($data->pivot->cantDiariaNino)*($orden_de_compra->no_beneficiarios))/$data->masa_drenada;
        array_push($cantidades, $cantidad);
        array_push($precios, ($data->precio * $cantidad));
     }


     return view('ordenes.show')
            ->with('orden_de_compra', $orden_de_compra)
            ->with('productos', $productos)
            ->with('cantidades', $cantidades)
            ->with('datas', $datas)
            ->with('precios', $precios)
            ->with('i', $i);
   }

   //Función que para hacer un Softdelete a un producto en específica
   public function destroy($id){
     //Se obtiene y elimina el producto deseada
     $orden_de_compra = Ordendecompra::find($id)->first()->delete();
     //Se ejecuta la función index de OrdendeComprasController
     return redirect()->action('OrdendeComprasController@index');
   }
}
