<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producto;
use App\Presentacion;
use App\Unidad_de_Medida;
use App\Provedor;


class ProductoController extends Controller
{
  //Función para mostrar la vista "index"
  public function index(){
    $productos = Producto::withTrashed()->paginate(8); //Se obtienen todas los productos activos/inactivos
    return view('productos.index')
            ->with('productos', $productos); //Regresa la vista index
  }

  //Función mostrar la vista "create"
  public function create(){
    //Se obtiene la información de los dos modelos
    $presentaciones = Presentacion::all();
    $unidades_de_medida = Unidad_de_Medida::all();
    $provedores = Provedor::all();

    return view('productos.create')
            ->with('provedores', $provedores)
            ->with('presentaciones', $presentaciones)
            ->with('unidades_de_medida', $unidades_de_medida); //Regresa la vista del create
  }

  //Función para almacenar un producto en la BD
  public function store(Request $request){
    //Se recibe los inputs que tiene el request
    $inputs = $request->all();
    //Se crea un producto con los inputs
    $producto = Producto::Create($inputs);
    //Se salva la localidad
    $producto->save();
    //Ejecuta la función index del ProductoController
    return redirect()->action('ProductoController@index');
  }

  //Función para mostrar la vista "show" de un producto en específica
  public function show($id){
    //Se obtiene el producto deseada
    $producto = Producto::withTrashed()->where('id', '=', $id)->first();
    return view('productos.show')->with('producto', $producto);
  }

  //Función para mostrar la vista de "edit" de un producto en específica
  public function edit($id){
    //Se obtiene la información del producto deseada
    $producto = Producto::where('id', '=', $id)->first();
    //Se obtiene tablas adicionales para los campos que se necesitan
    $presentaciones = Presentacion::all();
    $unidades_de_medida = Unidad_de_Medida::all();
    $provedores = Provider::all();

    return view('productos.edit')->with('producto', $producto)
            ->with('provedores', $provedores)
            ->with('presentaciones', $presentaciones)
            ->with('unidades_de_medida', $unidades_de_medida);
  }

  //Función para actualizar un producto en la BD
  public function update(Request $request, $id){
    //Se obtiene el producto que se quiere actualizar
    $producto = Producto::find($id);
    //Se actualiza cada campo
    $producto->nombre = $request->nombre;
    $producto->contenido_neto = $request->contenido_neto;
    $producto->masa_drenada = $request->masa_drenada;
    $producto->precio = $request->precio;
    $producto->marca = $request->marca;
    $producto->presentacion_id = $request->presentacion_id;
    $producto->unidad_de_medida_id = $request->unidad_de_medida_id;
    $producto->provedor_id = $request->provedor_id;
    //Función para guardar la modificación y actualizar sus relaciones
    $producto->push();

    return view('productos.show')->with('producto', $producto);
  }

  //Función que para hacer un Softdelete a un producto en específica
  public function destroy($id){
    //Se obtiene y elimina el producto deseada
    $producto = Producto::find($id)->delete();
    //Se ejecuta la función index de ProductoController
    return redirect()->action('ProductoController@index');
  }

  //Función para reactivar un producto con Softdelete
  public function reactivate($id){
    //Se obtiene el producto con Softdelete
    $producto = Producto::withTrashed()->find($id);
    //Se asigna NULL a la propiedad delete_at del producto
    $producto->deleted_at = NULL;
    //Se guarda la modificación
    $producto->save();
    return view('productos.show')->with('producto', $producto);
  }
}
