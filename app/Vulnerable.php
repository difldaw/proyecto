<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vulnerable extends Model
{
  use SoftDeletes;
  protected $table = 'vulnerables';
  protected $primaryKey = 'id_vulnerable';
  protected $fillable = [
      'nombre',
      'programa_id',
      'created_at',
      'updated_at'
    ];
    protected $dates = ['deleted_at'];

    public function programas()
      {
          return $this->belongsToMany('App\Programa');
      }

}
