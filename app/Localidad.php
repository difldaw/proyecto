<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Localidad extends Model
{
    use SoftDeletes;

    //Campos llenables en la tabla Localidad
    protected $fillable =  ['nombre', 'latitud', 'longitud', 'altitud',
                            'clave_carta', 'clave_localidad', 'municipio_id',
                            'grado_de_marginacion_id'];

    //Varable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "localidades"
    protected $table = 'localidades';

    /* Función que devuelve el model de Municipio relacionada con la Localidad
    Función relación 1 (Localidad) a N (Municipios) */
    public function municipio()
    {
      return $this->belongsTo('App\Municipio');
    }

    /* Función que devuelve el model de Grados de Marginación relacionada con la Localidad
    Función relación 1 (Localidad) a N (Grados de Marginación) */
    public function grado_de_marginacion()
    {
      return $this->belongsTo('App\Grado_de_Marginacion');
    }

}
