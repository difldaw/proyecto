<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presentacion extends Model
{
    use SoftDeletes;

    //Campos llenables en tabla Presentacion
    protected $fillable = ['nombre'];

    //Varable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "presentaciones"
    protected $table = 'presentaciones';

    /* Función que devuelve el model de Producto relacionada con la Presentacion
    Función relación 1 (Producto) a N (Presentacion) */
    public function productos()
    {
        return $this->hasMany('App\Producto');
    }
}
