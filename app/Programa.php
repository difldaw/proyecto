<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programa extends Model
{
  use SoftDeletes;
  protected $table = 'programas';
  protected $primaryKey = 'id_programa';
  protected $fillable = [
      'tipo',
      'vulnerable_id',
      'created_at',
      'updated_at'
    ];
    protected $dates = ['deleted_at'];


    public function vulnerables()
      {
          return $this->belongsToMany('App\Vulnerable');
      }

    public function despensas()
    {
        return $this->hasMany('App\Despensa');
    }

}
