<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contrato extends Model
{
  use SoftDeletes;
  protected $table = 'contratos';
  protected $primaryKey = 'id_contrato';
  protected $fillable = [
      'nombre',
      'fechainicio',
      'fechafin',
      'montoasignado',
      'observacion',
      'provedor_id',
      'created_at',
      'updated_at'
    ];
    protected $dates = ['deleted_at'];

    public function provedor()
   {
       return $this->belongsTo('App\Provedor');
   }
}
