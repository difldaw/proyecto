<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Escuela extends Model
{
    use SoftDeletes;
    
    protected $table = 'escuelas';
    protected $primaryKey = 'id_escuela';
    protected $fillable =  ['nombre', 'clave','domicilio', 'colonia', 'codigoPostal','latitud','longitud','telefono',
                          'delegacion','localidad_id','municipio_id','turno_id','zona_id','sector_id','gradoEscolar_id',
                          'servicio_id','sostenimiento_id', 'director','directorCongrupo','alumnosNinas','alumnosNinos',
                          'alumnosPrimero','alumnosSegundo','alumnosTercero','alumnosCuarto','alumnosQuinto','alumnosSexto', 'alumnosLactantes','alumnosMaternal',
                          'gruposPrimero','gruposSegundo','gruposTercero','gruposCuarto','gruposQuinto','gruposSexto', 'gruposLactantes','gruposMaternal',
                          'docentesMujeres','docentesHombres','docentesPrimero','docentesSegundo','docentesTercero','docentesCuarto','docentesQuinto','docentesSexto', 'docentesLactantes','docentesMaternal',
                          'personal','aulas'
                        ];
    protected $dates = ['deleted_at'];

}
