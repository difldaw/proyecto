<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    use SoftDeletes;

    //Campos llenables en la tabla Municipio
    protected $fillable =  ['clave_municipio', 'nombre'];

    //Variable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "municipios"
    protected $table = 'municipios';

    /* Función que devuelve el model de localidad relacionada con el municipio
    Función relación 1 (Localidad) a N (Municipios) */
    public function localidades(){
      return $this->hasMany('App\Localidad');
    }

    public function users(){
      return $this->hasMany('App\User');
    }
}
