<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;

    //Campos llenables en tabla Producto
    protected $fillable = ['nombre', 'contenido_neto', 'masa_drenada', 'precio', 'marca',
                            'presentacion_id', 'unidad_de_medida_id', 'provedor_id'];
    //Varable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "productos"
    protected $table = 'productos';

    /* Función que devuelve el model de Presentacion relacionada con el Producto
    Función relación 1 (Presentación) a N (Producto) */
    public function presentacion()
    {
        return $this->belongsTo('App\Presentacion');
    }

    /* Función que devuelve el model de Unidad de Medida relacionada con el Producto
    Función relación 1 (Unidad de Medida) a N (Producto) */
    public function unidad_de_medida()
    {
        return $this->belongsTo('App\Unidad_de_Medida');
    }

    /* Función que devuelve el model de Unidad de Medida relacionada con el Producto
    Función relación 1 (Provedor) a N (Producto) */
    public function provedor()
    {
        return $this->belongsTo('App\Provider');
    }
    public function despensas()
    {
        return $this->belongsToMany('App\Despensa');

    }
}
