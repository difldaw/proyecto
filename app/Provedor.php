<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provedor extends Model
{
  use SoftDeletes;
  protected $table = 'provedores';
  protected $primaryKey = 'id_provedor';
  protected $fillable = [
      'nombre',
      'rfc',
      'razonsocial',
      'correo',
      'telefono',
      'domicilio',
      'estado_id',
      'created_at',
      'updated_at'
    ];
    protected $dates = ['deleted_at'];

    public function contratos()
   {
       return $this->hasMany('App\Contrato');
   }

   public function estado()
  {
      return $this->belongsTo('App\Estado');
  }

  public function productos()
  {
      return $this->hasMany('App\Producto');
  }

}
