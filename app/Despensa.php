<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Despensa extends Model
{
    use SoftDeletes;

    //Campos llenables en tabla
    protected $fillable = ['programa_id'];

    protected $primaryKey = 'id_despensa';
    //Varable para la utilización del SoftDelete

    protected $dates = ['deleted_at'];

    //Variable para el manejo de la tabla llamada "despensas"
    protected $table = 'despensas';

    /* Función que devuelve el model de FoodProgram relacionada con Despensa
    Función relación 1 (FoodProgram) a N (Despensa) */
    public function programa()
    {
        return $this->belongsTo('App\Programa');
    }
    public function productos()
    {
        return $this->belongsToMany('App\Producto')->withPivot('cantDiariaNino', 'diasEntrega');

    }

}
