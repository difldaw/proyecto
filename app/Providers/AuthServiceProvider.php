<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
         Escuela::class => PoliticasEscuela::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO

        $gate->define('crud-productos',function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('crud-usuarios', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('crearEscuela', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarEscuela', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarEscuela', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarEscuela', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });
        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearPrograma', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarPrograma', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarPrograma', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarPrograma', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });
        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearProvedor', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarProvedor', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarProvedor', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarProvedor', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearContrato', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarContrato', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarContrato', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarContrato', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearProducto', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarProducto', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarProducto', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarProducto', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearLocalidad', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarLocalidad', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarLocalidad', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarLocalidad', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        //Consultar->
        //registrar -> crear
        //modificar-> editar update
        //delete ->eliminar
        //recordar tipo de usuario 1 es ADMIN, 2 es CENTRAL y 3 FORANEO
        $gate->define('crearMunicipio', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('editarMunicipio', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1 || $user->tipo_usuario == 2) return true;

          return false;
        });

        $gate->define('eliminarMunicipio', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });

        $gate->define('reactivarMunicipio', function(){
          $user = Auth::user();
          if($user->tipo_usuario == 1) return true;

          return false;
        });


    }
}
