<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordendecompra extends Model
{
   //Campos llenables en tabla Orden de Compra
    protected $fillable = ['no_folio','mes', 'anio', 'no_beneficiarios',
                              'programa_id','municipio_id'];

    //Variable para el manejo de la tabla llamada "ordenes_de_compra"
    protected $table = 'ordenesdecompra';

    /* Función que devuelve el model de FoodProgram relacionada con la Orden de Compra
    Función relación 1 (FoodProgram) a N (Orden de Compra) */
    public function programa()
    {
        return $this->belongsTo('App\Programa');
    }

    /* Función que devuelve el model de Municiío relacionada con el Orden de Compra
    Función relación 1 (Municipio) a N (Orden de Compra) */
    public function municipio()
    {
        return $this->belongsTo('App\Municipio');
    }

    public function productos()
    {
        return $this->belongsToMany('App\Producto')->withPivot('precio');

    }
}
