<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estado extends Model
{
  use SoftDeletes;
  protected $table = 'estados';
  protected $primaryKey = 'id_estado';
  protected $fillable = [
      'nombre',
      'created_at',
      'updated_at'
    ];
    protected $dates = ['deleted_at'];

    public function provedores()
   {
       return $this->hasMany('App\provedor');
   }
}
