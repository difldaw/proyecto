<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grado_de_Marginacion extends Model
{
    use SoftDeletes;

    //Campos llenables en la tabla de Grados de Marginación
    protected $fillable =  ['nombre'];

    //Variable para la utilización del SoftDelete
    protected $dates = ['deleted_at'];
    //Variable para el manejo de la tabla llamada "grados_de_marginacion"
    protected $table = 'grados_de_marginacion';

    /* Función que devuelve el model de Localidad relacionada con la Grados de Marginación
    Función relación 1 (Localidad) a N (Grados de Marginación) */
    public function localidades(){
      return $this->hasMany('App\Localidad');
    }
}
