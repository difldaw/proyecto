<?php

use Illuminate\Http\RedirectResponse;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestLocalidades extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     public function test_Localidad()
     {
         $this->visit('/localidad')
              ->see("Clave de Localidad");
     }

     public function test_LocalidadCreate()
    {
        $this->visit('/localidad/create')
             ->see("Datos generales");
    }

    public function test_SaveModal_on_Create()
    {
        $this->visit('/localidad/create')
             ->type('text', 'nombre')
             ->type('52', 'clave_localidad')
             ->type('G19W89', 'clave_carta')
             ->select('Querétaro', 'municipio_id')
             ->select('Normal', 'grado_de_marginacion_id')
             ->type('20.614004', 'latitud')
             ->type('-100.365945', 'longitud')
             ->type('563', 'altitud')
             ->press('save')
             ->see("¿Deseas guardar el registro de la localidad?");
    }
}
