<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaOrdenDeCompra extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
     Schema::create('ordenesdecompra', function(Blueprint $table){
           $table->engine = 'InnoDB';
           //Llave primaria
           $table->increments('id');

           //Columnas de la tabla
           $table->string('no_folio');
           $table->string('mes');
           $table->string('anio');
           $table->integer('no_beneficiarios');

           //Llave foranea con tabla de presentacion

           $table->integer('programa_id')->unsigned();
           $table->foreign('programa_id')->references('id_programa')->on('programas')->onDelete('cascade');

           //Llave foranea con tabla unidad de medida
           $table->integer('municipio_id')->unsigned();
           $table->foreign('municipio_id')->references('id')->on('municipios')->onDelete('cascade');

           //Columnas para el manejo de fechas de creación o modificación
           $table->timestamps();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::drop('ordenesdecompra');
   }
}
