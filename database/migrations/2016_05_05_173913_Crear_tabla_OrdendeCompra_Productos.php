<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaOrdendeCompraProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ordendecompra_producto', function(Blueprint $table){
            $table->engine = 'InnoDB';

            //Llave primaria
            $table->increments('id');

            //Llaves foraneas
            $table->integer('ordendecompra_id')->unsigned();
            $table->foreign('ordendecompra_id')->references('id_despensa')->on('despensas')->onDelete('cascade');

            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');


            //Columnas necesarias
            $table->float('precio');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('ordendecompra_producto');
    }
}
