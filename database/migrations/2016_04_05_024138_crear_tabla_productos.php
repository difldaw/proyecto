<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function(Blueprint $table){
            $table->engine = 'InnoDB';
            //Llave primaria
            $table->increments('id');

            //Columnas de la tabla
            $table->string('nombre');
            $table->float('contenido_neto');
            $table->float('masa_drenada');
            $table->float('precio');
            $table->string('marca');

            //Llave foranea con tabla de presentacion
            $table->integer('presentacion_id')->unsigned();
            $table->foreign('presentacion_id')->references('id')->on('presentaciones')->onDelete('cascade');

            //Llave foranea con tabla unidad de medida
            $table->integer('unidad_de_medida_id')->unsigned();
            $table->foreign('unidad_de_medida_id')->references('id')->on('unidades_de_medida')->onDelete('cascade');

            //Llave foranea con tabla provedor

            $table->integer('provedor_id')->unsigned();
            $table->foreign('provedor_id')->references('id_provedor')->on('provedores')->onDelete('cascade');

            //Columna para el softDelete
            $table->softDeletes();
            //Columnas para el manejo de fechas de creación o modificación
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productos');
    }
}
