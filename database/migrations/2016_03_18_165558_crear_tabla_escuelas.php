<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEscuelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escuelas', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id_escuela');
          $table->mediumText('nombre');
          $table->string('clave',15);
          $table->mediumText('domicilio');
          $table->mediumText('colonia');
          $table->string('codigoPostal',10);
          $table->string('latitud',15);
          $table->string('longitud',15);
          $table->string('telefono',15);
          $table->string('delegacion',15)->nullable();
          $table->integer('localidad_id')->unsigned();
          $table->integer('municipio_id')->unsigned();
          $table->integer('turno_id')->unsigned();
          $table->integer('zona_id')->unsigned();
          $table->integer('sector_id')->unsigned();
          $table->integer('gradoEscolar_id')->unsigned();
          $table->integer('servicio_id')->unsigned();
          $table->integer('sostenimiento_id')->unsigned();
          $table->foreign('localidad_id')->references('id')->on('localidades')->onDelete('cascade');
          $table->foreign('municipio_id')->references('id')->on('municipios')->onDelete('cascade');
          $table->foreign('turno_id')->references('id_turno')->on('turnos')->onDelete('cascade');
          $table->foreign('zona_id')->references('id_zona')->on('zonas')->onDelete('cascade');
          $table->foreign('sector_id')->references('id_sector')->on('sectores')->onDelete('cascade');
          $table->foreign('gradoEscolar_id')->references('id_gradoEscolar')->on('gradosescolares')->onDelete('cascade');
          $table->foreign('servicio_id')->references('id_servicio')->on('servicios')->onDelete('cascade');
          $table->foreign('sostenimiento_id')->references('id_sostenimiento')->on('sostenimientos')->onDelete('cascade');
          $table->mediumText('director');
          $table->boolean('directorCongrupo');
          $table->integer('alumnosNinas')->nullable();
          $table->integer('alumnosNinos')->nullable();
          $table->integer('alumnosPrimero')->nullable();
          $table->integer('alumnosSegundo')->nullable();
          $table->integer('alumnosTercero')->nullable();
          $table->integer('alumnosCuarto')->nullable();
          $table->integer('alumnosQuinto')->nullable();
          $table->integer('alumnosSexto')->nullable();
          $table->integer('alumnosLactantes')->nullable();
          $table->integer('alumnosMaternal')->nullable();
          $table->integer('gruposPrimero')->nullable();
          $table->integer('gruposSegundo')->nullable();
          $table->integer('gruposTercero')->nullable();
          $table->integer('gruposCuarto')->nullable();
          $table->integer('gruposQuinto')->nullable();
          $table->integer('gruposSexto')->nullable();
          $table->integer('gruposLactantes')->nullable();
          $table->integer('gruposMaternal')->nullable();
          $table->integer('docentesMujeres')->nullable();
          $table->integer('docentesHombres')->nullable();
          $table->integer('docentesPrimero')->nullable();
          $table->integer('docentesSegundo')->nullable();
          $table->integer('docentesTercero')->nullable();
          $table->integer('docentesCuarto')->nullable();
          $table->integer('docentesQuinto')->nullable();
          $table->integer('docentesSexto')->nullable();
          $table->integer('docentesLactantes')->nullable();
          $table->integer('docentesMaternal')->nullable();
          $table->integer('personal')->nullable();
          $table->integer('aulas')->nullable();
          $table->softDeletes();
          $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('escuelas', function (Blueprint $table) {
            //
        });
    }
}
