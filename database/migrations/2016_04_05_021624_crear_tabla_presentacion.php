<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPresentacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presentaciones', function(Blueprint $table){
            $table->engine = 'InnoDB';
            //Llave primaria
            $table->increments('id');

            //Columnas de la tabla
            $table->string('nombre');

            //Columna para el softDelete
            $table->softDeletes();
            //Columnas para el manejo de fechas de creación o modificación
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('presentaciones');
    }
}
