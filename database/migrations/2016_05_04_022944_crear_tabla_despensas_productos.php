<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDespensasProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('despensa_producto', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->integer('despensa_id')->unsigned();
        $table->integer('producto_id')->unsigned();
        $table->integer('cantDiariaNino');
        $table->integer('diasEntrega');
        $table->foreign('despensa_id')->references('id_despensa')->on('despensas')->onDelete('cascade');
        $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
