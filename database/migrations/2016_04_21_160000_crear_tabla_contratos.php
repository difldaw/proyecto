<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaContratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('contratos',function(Blueprint $table){
        $table->increments('id_contrato');
        $table->string('nombre');
        $table->date('fechainicio');
        $table->date('fechafin');
        $table->string('montoasignado');
        $table->string('observacion');

        $table->integer('provedor_id')->unsigned();
        $table->foreign('provedor_id')->references('id_provedor')->on('provedores')->onDelete('cascade');

        $table->softDeletes();
        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}
