<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaProgramasVulnerables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('programa_vulnerable', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->integer('programa_id')->unsigned();
        $table->integer('vulnerable_id')->unsigned();
        $table->foreign('programa_id')->references('id_programa')->on('programas')->onDelete('cascade');
        $table->foreign('vulnerable_id')->references('id_vulnerable')->on('vulnerables')->onDelete('cascade');


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
