<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaVulnerables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('vulnerables',function(Blueprint $table){
           $table->engine = 'InnoDB';
           $table->increments('id_vulnerable');
           $table->string('nombre');

           $table->softDeletes();
           $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('vulnerables');
    }
}
