<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaDespensas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('despensas', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->increments('id_despensa');
        $table->integer('programa_id')->unsigned();
        $table->foreign('programa_id')->references('id_programa')->on('programas')->onDelete('cascade');
        $table->softDeletes();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
