<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaGradosescolares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gradosescolares', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id_gradoEscolar');
          $table->mediumText('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gradosescolares', function (Blueprint $table) {
            //
        });
    }
}
