<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaGradosDeMarginacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('grados_de_marginacion', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        //Llave primaria de la tabla
        $table->increments('id');
        //Columas de la tabla
        $table->mediumText('nombre');

        //Columnas para el SoftDelete
        $table->softDeletes();
        //Columnas para el manejo de fechas de creación
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grados_de_marginacion');
    }
}
