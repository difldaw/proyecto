<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLocalidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localidades', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          //Llave primaria
          $table->increments('id');

          //Columnas de la tabla
          $table->mediumText('nombre');
          $table->string('latitud',15);
          $table->string('longitud',15);
          $table->string('altitud',15);
          $table->string('clave_carta', 6);
          $table->string('clave_localidad');

          //LLave foranea con tabla de grados_de_marginacion
          $table->integer('grado_de_marginacion_id')->unsigned();
          $table->foreign('grado_de_marginacion_id')->references('id')->on('grados_de_marginacion')->onDelete('cascade');

          //Llave foranea con tabla de municipios
          $table->integer('municipio_id')->unsigned();
          $table->foreign('municipio_id')->references('id')->on('municipios')->onDelete('cascade');

          //Columnas para el SoftDelete
          $table->softDeletes();
          //Columnas para el manejo de fechas de creación
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('localidades');
    }
}
