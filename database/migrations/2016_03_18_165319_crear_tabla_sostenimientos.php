<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSostenimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sostenimientos', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          $table->increments('id_sostenimiento');
          $table->mediumText('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sostenimientos', function (Blueprint $table) {
            //
        });
    }
}
