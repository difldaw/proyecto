<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaProvedores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('provedores',function(Blueprint $table){

      $table->engine = 'InnoDB';
        $table->increments('id_provedor');
        $table->string('nombre');
        $table->string('rfc');
        $table->string('razonsocial');
        $table->string('correo');
        $table->string('telefono');
        $table->string('domicilio');

        $table->integer('estado_id')->unsigned();
        $table->foreign('estado_id')->references('id_estado')->on('estados')->onDelete('cascade');

        $table->softDeletes();
        $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('provedores');
    }
}
