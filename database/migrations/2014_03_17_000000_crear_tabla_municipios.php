<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaMunicipios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipios', function (Blueprint $table) {
          $table->engine = 'InnoDB';
          //Llave primaria
          $table->increments('id');

          //Columnas de la tabla
          $table->integer('clave_municipio');
          $table->mediumText('nombre');

          //Columnas para el SoftDelete
          $table->softDeletes();
          //Columnas para el manejo de fechas de creación
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('municipios');
    }
}
