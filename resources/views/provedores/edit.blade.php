@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class="  ui circular massive right floated teal save icon submit button"  type="submit" form="editar">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/provedor/{{$provedor->id_provedor}}">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop

  @section('contenido')


  <script type="text/javascript">


  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form();

      $('.ui.dropdown')
      .dropdown()
      ;
      $('.ui.checkbox').checkbox();
      $('.ui.form')
        .form({
          inline : true,
            fields: {
              nombre: {
                identifier: 'nombre',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }
                ]
              },
              rfc: {
                identifier: 'rfc',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'RFC necesario'
                  }
                ]
              },
              razonsocial: {
                identifier: 'razonsocial',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Razon social necesario'
                  }
                ]
              },
              correo: {
                identifier: 'correo',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Correo Electronico necesario'
                  }
                ]
              },
              telefono: {
                identifier: 'telefono',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Telefono necesario'
                  }
                ]
              },
              domicilio: {
                identifier: 'domicilio',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Domicilio necesario'
                  }
                ]
              },
              estado: {
                identifier: 'estado',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Estado necesario'
                  }
                ]
              }
            }
        });

  });


  </script>
    <form id="editar" class="ui form" action="{{route('provedor.update',$provedor->id_provedor)}}" method="POST">
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui grid container">

          <div class="row">
            <div class="column">

                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>

                <div class="fields">
                  <div class="fourteen wide required field">
                    <label class="prompt">Nombre</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->nombre}}" placeholder="Nombre de programa" name="nombre">
                    </div>
                  </div>


                  <div class="fourteen wide required field">
                    <label class="prompt">RFC</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->rfc}}" placeholder="RFC" name="rfc">
                    </div>
                  </div>


                  <div class="fourteen wide required field">
                    <label class="prompt">Razon social</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->razonsocial}}" placeholder="Razon social" name="razonsocial">
                    </div>
                  </div>

                  <div class="fourteen wide required field">
                    <label class="prompt">Correo Electronico</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->correo}}" placeholder="Correo Electronico" name="correo">
                    </div>
                  </div>

                  <div class="fourteen wide required field">
                    <label class="prompt">Telefono</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->telefono}}" placeholder="Telefono" name="telefono">
                    </div>
                  </div>

                  <div class="fourteen wide required field">
                    <label class="prompt">Domicilio</label>
                    <div class="ui left icon input">
                      <input type="text" value="{{$provedor->domicilio}}" placeholder="Domicilio" name="domicilio">
                    </div>
                  </div>

                  <div class="three wide required column">
                    <label class="prompt">Estado:</label>
                    <select class="ui fluid dropdown" name="municipio_id">
                      @foreach($estados as $estado)
                        @if($provedor->estado_id == $estado->id)
                          <option value="{{$estado->id_estado}}" selected="selected">{{$estado->nombre}}</option>
                        @else
                          <option value="{{$estado->id_estado}}">{{$estado->nombre}}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>


                </div>

                  </br>
          </div>
        </div>

      </div>
      {{method_field('PATCH')}}
    </form>
  @stop

  @section('titulo_seccion')
    Editar Provedor
  @stop
