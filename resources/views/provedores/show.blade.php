
@extends('layouts.master')

@section('botones')
    <div class="row">
      @if($provedor->deleted_at==NULL)
      <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/provedor/{{$provedor->id_provedor}}/edit">
      @else
      <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/provedor/{{$provedor->id_provedor}}/edit">
      @endif
      <i class="write icon"></i>
      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">


      <script>
      function activarModal(){
        $('.small.modal').modal('show');
      }
      </script>


      @if($provedor->deleted_at==NULL)
      <button class="  ui circular massive right floated teal trash outline icon button"  onclick="activarModal()" class="ui icon button" >
        <i class="trash outline icon"></i>
      @else
      <button class="  ui circular massive right floated teal undo icon button" onclick=window.location.href="/provedor/reac/{{$provedor->id_provedor}}" >
      <i class=" undo icon"></i>
      @endif
    </div>


    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">

      <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/provedor">
      <i class="chevron left icon"></i>
      </button>
    </div>
  </br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br></br>
    <div class="row">
      <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/contrato/create?prov={{$provedor->id_provedor}}">
        <i class="plus icon"></i>
      </button>
    </div>
  @stop


  @section('scripts')
  <script type="text/javascript">
    $( document ).ready(function() {
      $("#todas").hide();
      $("#provedores").show();
    });
  </script>
  <script>
    function eliminados(){
      if($('.ui.checkbox').checkbox('is checked')){
        $("#todas").show();
        $("#provedores").hide();
      }else{
        $("#provedores").show();
        $("#todas").hide();
      }
    }
  </script>

  <script type="text/javascript">

  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.accordion').accordion();
    $('.ui.form')
      .form();
  });

    $('.ui.modal')
    .modal('show')
    ;
  </script>
  @stop

  @section('contenido')


<form id="vista" action="{{route('provedor.destroy',$provedor->id_provedor)}}" method="POST" >
  <input type="hidden" name="_method" value="DELETE">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">


  <div class="ui small modal">
    <i class="close icon"></i>
    <div class="header">
      Borrar Provedor
    </div>
    <div class="image content">
      <div class="image">
        <i class="trash icon"></i>
      </div>
      <div class="description">
        <h3>Esta seguro que quiere borrar el provedor?</h3>
      </div>
    </div>
    <div class="actions">
      <div class="two fluid ui buttons">
        <div class="ui red cancel button" >
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui green submit button" type="submit" form="vista" value="delete">
          <i class="checkmark icon"></i>
          Si
        </button>
      </div>
    </div>
  </div>

  <div class="ui styled fluid accordion ">
      <div class="title active ">
        <i class="dropdown icon"></i>
        Datos generales
      </div>
      <div class="content active ui container grid">
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Nombre del provedor:</strong> {{$provedor->nombre}}
          </div>
        </div>
        <div class="row">
          <div class=" ui twelve wide column">
            <strong>RFC de provedor:</strong> {{$provedor->rfc}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Razon Social:</strong> {{$provedor->razonsocial}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Correo Electronico:</strong> {{$provedor->correo}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Telefono:</strong> {{$provedor->telefono}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Domicilio:</strong> {{$provedor->domicilio}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Estado:</strong> {{$provedor->estado->nombre}}
          </div>
        </div>
      </div>
  </div>



@if($provedor->deleted_at==NULL)
  <h2>Contratos</h2>

<table class="ui selectable sortable teal celled table" id="todas">
  <thead>
    <tr>
      <th class="four wide">Nombre</th>
      <th class="four wide">Fecha de Inicio</th>
      <th class="four wide">Fecha de Finalizacion</th>
      <th class="four wide">Monto Asignado</th>
      <th class="one wide">Ver</th>
    </tr>
  </thead>
  <tbody>
      @foreach($todas as $toda)
        @if($toda->deleted_at!=NULL)
          <tr class="negative">
        @else
          <tr class="positive">
        @endif
        <td>{{$toda->nombre}}</td>
        <td>{{$toda->fechainicio}}</td>
        <td>{{$toda->fechafin}}</td>
        <td>{{$toda->montoasignado}}</td>
          <td onclick=window.location.href="/contrato/{{$toda->id_contrato}}" class=" selectable center aligned">
            <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th colspan="6">
        <div class="ui right floated pagination menu">
          <a href="{!! $todas->previousPageUrl() !!}" class="icon item">
            <i class="left chevron icon "></i>
          </a>
          @for ($pages = 1; $pages <= $contratos->lastPage(); $pages++)
              <a href="{!! $todas->url($pages) !!}" class="item ">{{$pages}}</a>
          @endfor
          <a href="{!! $todas->nextPageUrl() !!}" class="icon item">
            <i class="right chevron icon "></i>
          </a>
        </div>
      </th>
    </tr>
  </tfoot>
</table>




  <!-- Tabla Contratos -->
  <table class="ui fixed sortable teal selectable celled table" id="contratos">
    <?php
    $contratos2 = App\Provedor::find($provedor->id_provedor)->contratos;
    ?>
    <thead>
      <tr>
        <th class="four wide">Nombre</th>
        <th class="four wide">Fecha de Inicio</th>
        <th class="four wide">Fecha de Finalizacion</th>
        <th class="four wide">Monto Asignado</th>
        <th class="one wide">Ver</th>
      </tr>
    </thead>
    <tbody>

        @foreach($contratos2 as $contrato)<tr>
          <td>{{$contrato->nombre}}</td>
          <td>{{$contrato->fechainicio}}</td>
          <td>{{$contrato->fechafin}}</td>
          <td>{{$contrato->montoasignado}}</td>
          <td onclick=window.location.href="/contrato/{{$contrato->id_contrato}}" class=" selectable center aligned">
            <i class="info circle big icon "></i>
          </td>
        </tr>@endforeach

    </tbody>
    <tfoot>
      <tr>
        <th colspan="6">
          <div class="ui right floated pagination menu">

            <a href="{!! $contratos->previousPageUrl() !!}" class="icon item">
              <i class="left chevron icon "></i>
            </a>
            @for ($pages = 1; $pages <= $contratos->lastPage(); $pages++)
                <a href="{!! $contratos->url($pages) !!}"class="item ">{{$pages}}</a>
            @endfor

            <a href="{!! $contratos->nextPageUrl() !!}"class="icon item">
              <i class="right chevron icon "></i>
            </a>
          </div>
        </th>
      </tr>
    </tfoot>
  </table>


@endif


{{method_field('DELETE')}}
</form>
    @stop

    @section('titulo_seccion')
      Mostrar Provedor
    @stop
