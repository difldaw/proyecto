@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class="  ui circular massive right floated teal save icon submit button"  type="submit" form="crear">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/provedor">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop


  @section('contenido')

  <script type="text/javascript">


  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.dropdown')
    .dropdown()
    ;
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form({
        inline : true,
          fields: {
            nombre: {
              identifier: 'nombre',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Nombre necesario'
                }
              ]
            },
            rfc: {
              identifier: 'rfc',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'RFC necesario'
                }
              ]
            },
            razonsocial: {
              identifier: 'razonsocial',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Razon social necesario'
                }
              ]
            },
            correo: {
              identifier: 'correo',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Correo Electronico necesario'
                }
              ]
            },
            telefono: {
              identifier: 'telefono',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Telefono necesario'
                }
              ]
            },
            domicilio: {
              identifier: 'domicilio',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Domicilio necesario'
                }
              ]
            },
            estado_id: {
              identifier: 'estado_id',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Estado necesario'
                }
              ]
            }
          }
      });


  });


  </script>

        <div class="ui grid container">

          <div class="row">
            <div class="column">

              <form id="crear" class="ui form" action="{{ url('/provedor') }}" method="POST" >
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>

                <div class="fields">
                  <div class="fourteen wide required field">
                    <label class="prompt">Nombre</label>
                    <div class="ui left icon input">
                      <input type="text" placeholder="Nombre de provedor" name="nombre">
                    </div>
                  </div>

                </br>


                <div class="fourteen wide required field">
                  <label class="prompt">RFC</label>
                  <div class="ui left icon input">
                    <input type="text" placeholder="RFC" name="rfc">
                  </div>
                </div>


                <div class="fourteen wide required field">
                  <label class="prompt">Razon Social</label>
                  <div class="ui left icon input">
                    <input type="text" placeholder="Razon Social" name="razonsocial">
                  </div>
                </div>

                </div>

                <div class="two fields">
                <div class="ten wide required field">
                  <label class="prompt">Correo Electronico</label>
                  <div class="ui left icon input">
                    <input type="text" placeholder="Correo Electronico" name="correo">
                  </div>
                </div>

                <div class="six wide required field">
                  <label class="prompt">Telefono</label>
                  <div class="ui left icon input">
                    <input type="text" placeholder="442-232-1234" name="telefono">
                  </div>
                </div>

                </div>
                <div class="two fields">
                <div class="twelve wide required field">
                  <label class="prompt">Domicilio</label>
                  <div class="ui left icon input">
                    <input type="text" placeholder="domicilio" name="domicilio">
                  </div>
                </div>

                <div class="four wide required field">
                  <label class="prompt">Estado</label>
                  <select class="ui fluid dropdown" name="estado_id">
                    <option value=""></option>
                    @foreach($estados as $estado)
                    <option value="{{$estado->id_estado}}">{{$estado->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>


                  </br>
                </div>
              </div>
              </form>
          </div>

  @stop

  @section('titulo_seccion')
    Crear Provedor
  @stop
