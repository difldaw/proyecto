@extends('layouts.master')
@section('botones')
    <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/provedor/create">
    <i class="plus icon"></i>
  </button>
  @stop

  @section('scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
        $("#todas").hide();
        $("#provedores").show();
      });
    </script>
    <script>
      function eliminados(){
        if($('.ui.checkbox').checkbox('is checked')){
          $("#todas").show();
          $("#provedores").hide();
        }else{
          $("#provedores").show();
          $("#todas").hide();
        }
      }
    </script>
  @stop

  @section('contenido')

  <div class="ui grid container">
    <div class="row">
      <div class="twelve wide column"></div>
      <div class="four wide column">
        <div class=" ui slider checkbox ">
          <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
          <label>Mostrar eliminados</label>
        </div>
      </div>
    </div>
  </div>


<!-- Tabla Todas -->

<table class="ui selectable sortable teal celled table" id="todas">
  <thead>
    <tr>
      <th class="four wide">Nombre</th>
      <th class="four wide">RFC</th>
      <th class="four wide">Razon Social</th>
      <th class="four wide">Correo Electronico</th>
      <th class="four wide">Telefono</th>
      <th class="four wide">Domicilio</th>
      <th class="four wide">Estado</th>
      <th class="one wide">Ver</th>
    </tr>
  </thead>
  <tbody>
      @foreach($todas as $toda)
        @if($toda->deleted_at!=NULL)
          <tr class="negative">
        @else
          <tr class="positive">
        @endif
          <td>{{$toda->nombre}}</td>
          <td>{{$toda->rfc}}</td>
          <td>{{$toda->razonsocial}}</td>
          <td>{{$toda->correo}}</td>
          <td>{{$toda->telefono}}</td>
          <td>{{$toda->domicilio}}</td>
          <td>{{$toda->estado->nombre}}</td>
          <td onclick=window.location.href="/provedor/{{$toda->id_provedor}}" class=" selectable center aligned">
            <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th colspan="6">
        <div class="ui right floated pagination menu">
          <a href="{!! $todas->previousPageUrl() !!}" class="icon item">
            <i class="left chevron icon "></i>
          </a>
          @for ($pages = 1; $pages <= $provedores->lastPage(); $pages++)
              <a href="{!! $todas->url($pages) !!}" class="item ">{{$pages}}</a>
          @endfor
          <a href="{!! $todas->nextPageUrl() !!}" class="icon item">
            <i class="right chevron icon "></i>
          </a>
        </div>
      </th>
    </tr>
  </tfoot>
</table>



<!-- Tabla Provedores -->
      <table class="ui fixed sortable teal selectable celled table" id="provedores">
        <thead>
          <tr>
            <th class="four wide">Nombre</th>
            <th class="four wide">RFC</th>
            <th class="four wide">Razon Social</th>
            <th class="four wide">Correo Electronico</th>
            <th class="four wide">Telefono</th>
            <th class="four wide">Domicilio</th>
            <th class="four wide">Estado</th>
            <th class="one wide">Ver</th>
          </tr>
        </thead>
        <tbody>

            @foreach($provedores as $provedor)<tr>
              <td>{{$provedor->nombre}}</td>
              <td>{{$provedor->rfc}}</td>
              <td>{{$provedor->razonsocial}}</td>
              <td>{{$provedor->correo}}</td>
              <td>{{$provedor->telefono}}</td>
              <td>{{$provedor->domicilio}}</td>
              <td>{{$provedor->estado->nombre}}</td>
              <td onclick=window.location.href="/provedor/{{$provedor->id_provedor}}" class=" selectable center aligned">
                <i class="info circle big icon "></i>
              </td>
            </tr>@endforeach

        </tbody>
        <tfoot>
          <tr>
            <th colspan="6">
              <div class="ui right floated pagination menu">

                <a href="{!! $provedores->previousPageUrl() !!}" class="icon item">
                  <i class="left chevron icon "></i>
                </a>
                @for ($pages = 1; $pages <= $provedores->lastPage(); $pages++)
                    <a href="{!! $provedores->url($pages) !!}"class="item ">{{$pages}}</a>
                @endfor

                <a href="{!! $provedores->nextPageUrl() !!}"class="icon item">
                  <i class="right chevron icon "></i>
                </a>
              </div>
            </th>
          </tr>
        </tfoot>
      </table>

  @stop

  @section('titulo_seccion')
    Provedores
    <!--<div class="fourteen wide field"></div>

    <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/foodprogram/create">
    <i class="plus icon"></i>
  </button>-->
  @stop
