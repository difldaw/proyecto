@extends('layouts.master')

@section('botones')
  <!-- Inicio de botón de guardado -->
  <div class="row">
    <button class="ui circular massive right floated teal save icon submit button" onclick="envio()" >
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final de botón de guardado -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regreso -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final de botón de regreso -->
@stop

@section('titulo_seccion')
  Despensa #{{$despensa->id_despensa}}
@stop

<?php $i=0;?>
@foreach($productostodos as $productostodo)
<?php $i++;
?>
@endforeach

@section('scripts')
   <script type="text/javascript">
      // Función para verificar si el form esta validado

      function envio(){
        //validacion de seleccion de al menos un producto
        var bandera = false;
        var php_countproductos = "<?php echo $i; ?>";
        for (var i = 1; i <= php_countproductos; i++) {
          var a = $( "input[name=cantDiariaNino"+i+"]").val();
          var b = $( "input[name=diasEntrega"+i+"]").val();
          //alert(i);
          //alert(a);
          //alert(b);
          if(a!="" && b!=""){
            bandera=true;
          }

        }

        //alert(bandera);
        if (bandera==true){
          $('#c').form('validate form');
          //Si es true, se mostrara el modal con el id "guardar"
          if($('#editar_form').form('is valid')){
            $('#guardar').modal('show');
          }
        }
        else{
          $('#mensajeProductos').show();
        }

      }
   </script>

   <script type="text/javascript">
      $(document).ready(function(){
        $('.segment').dimmer({
          on: 'hover'
        });
         //Inicialización de elementos de Semantic UI

         $('.ui.modal').modal();
         $('.menu .item').tab();
         $('select').dropdown();

         //Validaciones dentro de la forma con id "registro_form"

         $('#editar_form').form({
           on:'blur',
           inline: true,
           fields:{
             nombre:{
               identifier: 'programa_id',
               rules:[{
                 type: 'empty',
                 prompt: 'Seleccione un programa alimenticio'
               }]
             }
           }
         });

      });
   </script>
@stop

@section('contenido')
    <?php $i=0 ?>
    @foreach($despensa->productos as $producto)
    <?php $i++ ?>
    @endforeach
    <?php if ($i==0): ?>
      <div class="ui negative message"><i class="info circle icon"></i>La despensa no tiene productos asignados.</div>
    <?php else: ?>
      @if($despensa->deleted_at == NULL)
         <div class="ui message"><i class="info circle icon"></i>Información de la Despensa </div>
      @else
         <div class="ui negative message"><i class="info circle icon"></i>Despensa deshabilitada. Para editar deshacer eliminación. </div>
      @endif
    <?php endif; ?>

   <!--Inicio Modal de confirmacion de regreso a show-->
   <div class="ui small modal" id="regresar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Descartar registro
      </div>
      <div class=" content">
         <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
      </div>
      <div class="actions">
         <div class="ui negative cancel button">
            <i class="remove icon"></i>
            No
         </div>
         <div class="ui positive button" onclick=window.location.href="{{route('despensa.show', $despensa->id_despensa)}}">
            <i class="checkmark icon"></i>
            Si
         </div>
      </div>
   </div>
   <!--Final Modal de confirmacion de regreso a show-->

   <!--Inicio Modal de confirmacion de envio de modificaciones-->
   <div class="ui small modal" id="guardar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Guardar cambios
      </div>
      <div class="content">
         <p>¿Deseas guardar los cambios?</p>
      </div>
      <div class="actions">
         <div class="ui negative cancel button">
            <i class="remove icon"></i>
            No
         </div>
         <button class="ui positive submit button" type="submit" form="editar_form">
            <i class="checkmark icon"></i>
            Si
         </button>
      </div>
   </div>
   <!--Final Modal de confirmacion de envio de modificaciones-->


   <!--Inicio del Contenedor de forma de editar-->
   <!-- <div class="ui grid container"> -->
      <!--Inicio de la forma de editar-->
      <form id="editar_form" class="ui form" action="{{route('despensa.update', $despensa->id_despensa)}}" method="POST">
         <input type="hidden" name="_token" value="{{csrf_token()}}">
         <input type="hidden" name="_method" value="PATCH">



         <h4 class="ui horizontal divider header">
           <i class="food icon">
           </i>
           Detalles de despensa
         </h4>
         <div class="ui warning message hidden" id="mensajeProductos">
           <i class="close icon"></i>
           <div class="header">
             Debes agregar al menos un producto a la despensa!
           </div>
           Inserta cantidad por beneficiario y dias de entrega.
         </div>

            <div class="one fields">


                <!-- Select para el campo de "Presentacion" -->
                <div class="six wide required field">
                  <label class="prompt">Programa Alimenticio</label>
                  <select class="ui fluid dropdown" name="programa_id">

                    @foreach($programas as $programa)

                       @if($despensa->programa_id == $programa->id_programa)
                          <option value="{{$programa->id_programa}}" selected="selected">{{$programa->tipo}}</option>
                       @else
                          <option value="{{$programa->id_programa}}">{{$programa->tipo}}</option>
                       @endif
                    @endforeach

                  </select>
                </div>
              </div>

            <div class="row"></br></br></div>
            <!--Inicio del contenidor de cartas-->
            <div class="ui five special  cards">

               @foreach($productostodos as $productotodo)



                  <div class="ui card black blurring segment" >
                    <div class="ui inverted  dimmer">
                      <div class="content">
                        <div class="centered ">
                          <div class="ui grid">
                            <div class="two wide column ">
                            </div>
                            <div class="twelve wide column required field">
                              <label class="prompt">Cantidad diaria por niño</label>
                                <div class="ui mini right labeled input">
                                  <input type="text" placeholder="" name="cantDiariaNino{{$productotodo->id}}"
                                  @foreach($despensa->productos as $producto)
                                    @if($producto->pivot->producto_id == $productotodo->id)
                                    value="{{$producto->pivot->cantDiariaNino}}"
                                    @endif
                                  @endforeach
                                  >
                                  <div class="ui basic label">
                                    {{$productotodo->presentacion->nombre}}(s)
                                  </div>
                                </div>

                            </div>
                          </div>

                          <div class="ui grid">
                            <div class="two wide column ">
                            </div>
                            <div class="twelve wide column required field">
                              <label class="prompt">Dias de entrega</label>
                              <div class="ui  input">
                                <input type="number" placeholder="" name="diasEntrega{{$productotodo->id}}"
                                @foreach($despensa->productos as $producto)
                                  @if($producto->pivot->producto_id == $productotodo->id)
                                  value="{{$producto->pivot->diasEntrega}}"
                                  @endif
                                @endforeach
                                >
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                     <div class="content">

                        <div class="header">{{$productotodo->nombre}}
                          <div class="ui meta">
                            <span class="date">{{$productotodo->marca}}
                            </span>
                          </div>
                        </div>
                     </div>

                     <div class="content ">
                        <!-- Subtitulo para mostrar la marca del producto -->

                        <!-- Renglón para mostrar el Provedor del producto -->

                        <div class="content">
                           <span class="right floated">

                             xxxx
                           </span>

                           Proveedor
                         </div>
                         </br>
                         <div class="content">
                            <span class="right floated">

                              {{$productotodo->contenido_neto}}{{$productotodo->unidad_de_medida->nombre}}
                            </span>

                            Contenido Neto
                          </div>
                          </br>
                          <div class="content">
                             <span class="right floated">

                               {{$productotodo->masa_drenada}}{{$productotodo->unidad_de_medida->nombre}}
                             </span>

                             Masa Drenada
                         </div>



                     </div>
                     <div class="extra content">
                       <span class="right floated">
                         ${{$productotodo->precio}}
                       </span>
                       <span>
                         {{$productotodo->presentacion->nombre}}
                       </span>
                    </div>

                  </div>
                  <!-- Fin de la carta -->
               @endforeach
            </div>
            <!--Fin del contenidor de cartas-->

      </form>
      <!--Fin de la forma de editar-->
   <!-- </div> -->
   <!--Fin del Contenedor de forma de editar-->
@stop
