@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón crear -->
  <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="{{route('despensa.create')}}">
    <i class="plus icon"></i>
  </button>
  <!-- Fin del botón crear -->
@stop

@section('titulo_seccion')
  Despensas
@stop

@section('scripts')
  <script type="text/javascript">
    $( document ).ready(function() {
      //Ocultar tabla con despensas activas/inactivas
      $(".hidden").hide();
    });
  </script>
  <script>
  /*Función para mostrar la tabla de despensas activas y ocultar
    activas/inactivas y viceversa*/
    function eliminados(){
      if($('.ui.checkbox').checkbox('is checked')){
        $(".hidden").show();
      }else{
        $(".hidden").hide();
      }
    }
  </script>
@stop

@section('contenido')
<!--Inicio de container independiente de tablas-->
<div class="ui grid container">
 <div class="row">
   <div class="twelve wide column"></div>
   <div class="four wide column">
     <!-- Checkbox para mostrar las diferentes tablas -->
     <div class=" ui slider checkbox ">
       <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
       <label>Mostrar eliminadas</label>
     </div>
   </div>
 </div>
</div>

<!--Inicio del contenidor de cartas-->
   <div class="ui four cards">
      @foreach($despensas as $despensa)

         @if($despensa->deleted_at != NULL)
         <!-- Inicio de la carta -->
         <div class="ui card red hidden" onclick=window.location.href="{{route('despensa.show', $despensa->id_despensa)}}">
            <div class="content">
               <div class="ui red right ribbon label">
                  <i class="trash icon"></i> Eliminado
               </div>
         @else
         <div class="ui card green" onclick=window.location.href="{{route('despensa.show', $despensa->id_despensa)}}">
            <div class="content">
               <div class="ui green right ribbon label">
                  <i class="save icon"></i> Activa
               </div>
         @endif



            <div class="header"></br>{{$despensa->programa->tipo}}</div>
            </div>

            <div class="content">

               <!-- Subtitulo para mostrar la marca del producto -->

               <div class="meta">
                  Despensa #{{$despensa->id_despensa}}
                </div>
               <h4 class="ui sub header">Fecha de creación:</br></br><?php
               $date = date('d-m-Y, g:i a',strtotime($despensa->created_at));
               echo $date;
                ?></h4>
               <div class="ui small feed">
                  <!-- Renglón para mostrar el Provedor del producto -->
                  <div class="event"><div class="content"><div class="summary">
                  <div class="extra content">

                    <span>
                      <?php $i=0 ?>
                      @foreach($despensa->productos as $producto)
                      <?php $i++ ?>
                      @endforeach
                      <i class="food icon"></i>
                      <?php echo($i) ?> productos
                    </span>
                  </div>
                  </div></div></div>



               </div>
            </div>

         </div>
         <!-- Fin de la carta -->

      @endforeach
   </div>
   <!--Fin del contenidor de cartas-->
   <br>
   <div class="ui right floated pagination menu">
      <a href="{!! $despensas->previousPageUrl() !!}" class="icon item">
         <i class="left chevron icon "></i>
      </a>
      @for($pages = 1; $pages <= $despensas->lastPage(); $pages++)
      <a href="{!! $despensas->url($pages) !!}" class="item ">{{$pages}}</a>
      @endfor
      <a href="{!! $despensas->nextPageUrl() !!}" class="icon item">
         <i class="right chevron icon "></i>
      </a>
   </div>


@stop
