@extends('layouts.master')

@section('botones')
   <!-- Inicio botón para editar -->
   <div class="row">
   @if($despensa->deleted_at == NULL)
     <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="{{route('despensa.edit', $despensa->id_despensa)}}">
   @else
     <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="{{route('despensa.edit', $despensa->id_despensa)}}">
   @endif
       <i class="write icon"></i>
     </button>
   </div>
  <!-- Final botón para editar -->

  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de eliminar o rehabilitar, siendo el caso -->
  <div class="row">
  @if($despensa->deleted_at == NULL)
    <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
      <i class="trash outline icon"></i>
  @else
    <button class="  ui circular massive right floated teal undo icon button" onclick="$('#habilitar').modal('show');" >
      <i class=" undo icon"></i>
  @endif
    </button>
  </div>
  <!-- Fin del botón de eliminar o rehabilitar, siendo el caso -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regresar al index -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="{{route('despensa.index')}}">
      <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Fin de botón de regresar al index -->
@stop

@section('titulo_seccion')
  Despensa #{{$despensa->id_despensa}}
@stop

@section('scripts')
   <script>
      $( document ).ready(function() {
         // Inicialización de elementos de Semantic UI
         $('.ui.accordion').accordion();
         $('.ui.modal').modal();
      });
   </script>
@stop

@section('contenido')

   <!-- Inicio del Modal de confirmacion de regreso a index -->
   <div class="ui small modal" id="eliminar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Eliminar Despensa
      </div>
      <div class=" content">
        <p>¿Seguro que deseas eliminar la despensa #{{$despensa->id_despensa}} {{$despensa->programa->tipo}} ?</p>
      </div>
      <div class="actions">
           <div class="ui negative cancel button">
             <i class="remove icon"></i>
             No
           </div>
           <button class="ui positive submit button "  type="submit" form="vista" value="delete">
             <i class="checkmark icon"></i>
             Si
           </button>
      </div>
   </div>
   <!-- Final del Modal de confirmacion de regreso a index -->

   <!-- Inicio del Modal de confirmacion de habilitacion de producto -->
   <div class="ui small modal" id="habilitar">
       <i class="close icon"></i>
       <div class="ui icon header">
          <i class="archive icon"></i>
          Reactivar despensa
       </div>
       <div class=" content">
          <p>¿Seguro que deseas reactivar la despensa #{{$despensa->id_despensa}}  {{$despensa->programa->tipo}}?</p>
       </div>
       <div class="actions">
           <div class="ui negative cancel button">
              <i class="remove icon"></i>
              No
           </div>
           <button class="ui positive submit button " onclick=window.location.href="{{route('habilitaDespensa',$despensa->id_despensa)}}">
              <i class="checkmark icon"></i>
              Si
           </button>
       </div>
   </div>
   <!-- Final del Modal de confirmacion de habilitacion de producto -->

   <!--Inicio de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
   <form id="vista" class="ui form" action="{{route('despensa.destroy', $despensa->id_despensa)}}" method="POST" >
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="_method" value="DELETE">
      <h4 class="ui horizontal divider header">
        <i class="food icon">
        </i>
        Programa {{$despensa->programa->tipo}}
      </h4>
      <?php $i=0 ?>
      @foreach($despensa->productos as $producto)
      <?php $i++ ?>
      @endforeach
      <?php if ($i==0): ?>
        <div class="ui negative message"><i class="info circle icon"></i>La despensa no tiene productos asignados.</div>
      <?php else: ?>
        @if($despensa->deleted_at == NULL)
           <div class="ui message"><i class="info circle icon"></i>Información de la Despensa </div>
        @else
           <div class="ui negative message"><i class="info circle icon"></i>Despensa deshabilitada. Para editar deshacer eliminación. </div>
        @endif
      <?php endif; ?>




      <!--Inicio del contenidor de cartas-->
         <div class="ui five cards">

            @foreach($despensa->productos as $producto)
              <div class="ui card black" >

                 <div class="content">

                    <div class="header">{{$producto->nombre}}
                      <div class="ui meta">
                        <span class="date">{{$producto->marca}}
                        </span>
                      </div>
                    </div>
                 </div>

                 <div class="ui segments content">
                    <!-- Subtitulo para mostrar la marca del producto -->
                       <!-- Renglón para mostrar el Provedor del producto -->
                       <div class="ui segment">
                       <div class=" content">
                          <span class="right floated">
                            xxxx
                          </span>
                          Proveedor
                        </div>

                        <div class=" content">
                           <span class="right floated">
                             {{$producto->contenido_neto}}{{$producto->unidad_de_medida->nombre}}
                           </span>
                           Contenido Neto
                         </div>

                         <div class=" content">
                            <span class="right floated">
                              {{$producto->masa_drenada}}{{$producto->unidad_de_medida->nombre}}
                            </span>
                            Masa Drenada
                        </div>
                        </div>
                        <div class="ui segment">
                          <div class=" ">
                             <span class="right floated">

                               <strong>{{$producto->pivot->cantDiariaNino}}{{$producto->unidad_de_medida->nombre}}</strong>
                             </span>
                             <strong>Cantidad diaria </strong>
                           </div>
                           </br>
                           <div class="">
                              <span class="right floated">
                                <strong>
                                {{$producto->pivot->diasEntrega}}{{$producto->unidad_de_medida->nombre}}
                              </strong>
                              </span>
                            <strong>Dias de entrega</strong>
                          </div>
                        </div>
                 </div>
                 <div class="extra content">
                   <span class="right floated">
                     ${{$producto->precio}}
                   </span>
                   <span>
                     {{$producto->presentacion->nombre}}
                   </span>
                </div>

              </div>
              <!-- Fin de la carta -->

            @endforeach
         </div>
         <!--Fin del contenidor de cartas-->

   </form>
   <!--Final de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
@stop
