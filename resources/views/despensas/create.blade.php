@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón de guardado -->
  <div class="row">
    <button id="save" class="ui circular massive right floated teal save icon submit button" onclick="envio()">
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de regresar -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
@stop

@section('titulo_seccion')
  Crear Despensa
@stop


<?php $i=0;?>
@foreach($productos as $producto)
<?php $i++;
?>
@endforeach


@section('scripts')
  <script type="text/javascript">
    // Función para verificar si el form esta validado
    function envio(){
      //validacion de seleccion de al menos un producto
      var bandera = false;
      var php_countproductos = "<?php echo $i; ?>";
      for (var i = 1; i <= php_countproductos; i++) {
        var a = $( "input[name=cantDiariaNino"+i+"]").val();
        var b = $( "input[name=diasEntrega"+i+"]").val();

        if(a!="" && b!=""){
          bandera=true;
        }

      }
      //alert(bandera);
      if (bandera==true){
        $('#c').form('validate form');
        //Si es true, se mostrara el modal con el id "guardar"
        if($('#registro_form').form('is valid')){
          $('#guardar').modal('show');
        }
      }
      else{
        $('#mensajeProductos').show();
      }

    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.segment').dimmer({
        on: 'hover'
      });
      //Inicialización de elementos de Semantic UI
      $('.ui.modal').modal();
      $('.menu .item').tab();
      $('select').dropdown();

      //Validaciones dentro de la forma con id "registro_form"

      $('#registro_form').form({
        on:'blur',
        inline: true,
        fields:{
          nombre:{
            identifier: 'programa_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un programa alimenticio'
            }]
          }
        }
      });

    });
  </script>
@stop

@section('contenido')

   <!--Inicio del Modal de confirmacion de regresar a index-->
  <div class="ui small modal" id="regresar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Descartar registro
    </div>
    <div class=" content">
        <p>¿Estás seguro que deseas regresar?. Se perderan todos los datos sin guardar.</p>
    </div>
    <div class="actions">
      <div class="ui negative cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui positive button" onclick=window.location.href="{{route('despensa.index')}}">
        <i class="checkmark icon"></i>
        Si
      </div>
    </div>
  </div>
  <!--Final del Modal de confirmacion de regresar a index-->

  <!--Inicio del Modal de confirmacion de envio de formulario-->
  <div class="ui modal" id="guardar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Guardar registro nuevo
    </div>
    <div class="content">
        <p>¿Deseas guardar el registro de la localidad?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button" type="submit" form="registro_form">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!--Final del Modal de confirmacion de envio de formulario-->

  <!--Inicio del Contenedor de forma de registro-->
  <div class="ui grid container">
    <div class="row">
      <div class="column">
        <!--Inicio de la forma de registro-->
        <form id="registro_form" class="ui form" action="{{route('despensa.store')}}" method="POST">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="_method" value="POST">
          <h4 class="ui horizontal divider header">
            <i class="food icon">
            </i>
            Detalles de despensa
          </h4>
          <div class="ui warning message hidden" id="mensajeProductos">
            <i class="close icon"></i>
            <div class="header">
              Debes agregar al menos un producto a la despensa!
            </div>
            Inserta cantidad por beneficiario y dias de entrega.
          </div>

          <!-- Inicio de la Seccion de datos generales -->
            <!-- Inicio de la primera fila -->
            <div class="one fields">


              <!-- Select para el campo de "Presentacion" -->
              <div class="six wide required field">
                <label class="prompt">Programa Alimenticio</label>
                <select class="ui fluid dropdown" name="programa_id">
                  <option value=""></option>
                  @foreach($programas as $programa)
                  <option value="{{$programa->id_programa}}">{{$programa->tipo}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- Final de la primera fila -->

          <div class="row"></br></br></div>
          <!-- Final de la Seccion de datos generales -->
          <!--Inicio del contenidor de cartas-->
          <div class="ui five special  cards">
             @foreach($productos as $producto)



                <div class="ui card black blurring segment" >
                  <div class="ui inverted  dimmer">
                    <div class="content">
                      <div class="centered ">
                        <div class="ui grid">
                          <div class="two wide column ">
                          </div>
                          <div class="twelve wide column required field">
                            <label class="prompt">Cantidad diaria por niño</label>
                              <div class="ui mini right labeled input">
                                <input type="text" placeholder="" name="cantDiariaNino{{$producto->id}}">
                                <div class="ui basic label">
                                  {{$producto->unidad_de_medida->nombre}}
                                </div>
                              </div>

                          </div>
                        </div>

                        <div class="ui grid">
                          <div class="two wide column ">
                          </div>
                          <div class="twelve wide column required field">
                            <label class="prompt">Dias de entrega</label>
                            <div class="ui  input">
                              <input type="number" placeholder="" name="diasEntrega{{$producto->id}}">
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                   <div class="content">

                      <div class="header">{{$producto->nombre}}
                        <div class="ui meta">
                          <span class="date">{{$producto->marca}}
                          </span>
                        </div>
                      </div>
                   </div>

                   <div class="content ">
                      <!-- Subtitulo para mostrar la marca del producto -->

                      <!-- Renglón para mostrar el Provedor del producto -->

                      <div class="content">
                         <span class="right floated">

                           xxxx
                         </span>

                         Proveedor
                       </div>
                       </br>
                       <div class="content">
                          <span class="right floated">

                            {{$producto->contenido_neto}}{{$producto->unidad_de_medida->nombre}}
                          </span>

                          Contenido Neto
                        </div>
                        </br>
                        <div class="content">
                           <span class="right floated">

                             {{$producto->masa_drenada}}{{$producto->unidad_de_medida->nombre}}
                           </span>

                           Masa Drenada
                       </div>



                   </div>
                   <div class="extra content">
                     <span class="right floated">
                       ${{$producto->precio}}
                     </span>
                     <span>
                       {{$producto->presentacion->nombre}}
                     </span>
                  </div>

                </div>
                <!-- Fin de la carta -->
             @endforeach
          </div>
          <!--Fin del contenidor de cartas-->
        </form>
        <!--Final de la forma de registro-->
      </div>
    </div>
  </div>
  <!--Final del Contenedor de forma de registro-->
@stop
