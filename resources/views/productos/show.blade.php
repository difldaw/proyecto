@extends('layouts.master')

@section('botones')
   <!-- Inicio botón para editar -->
  <div class="row">
  @if($producto->deleted_at == NULL)
    <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/producto/{{$producto->id}}/edit">
  @else
    <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/producto/{{$producto->id}}/edit">
  @endif
      <i class="write icon"></i>
    </button>
  </div>
  <!-- Final botón para editar -->

  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de eliminar o rehabilitar, siendo el caso -->
  <div class="row">
  @if($producto->deleted_at == NULL)
    <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
      <i class="trash outline icon"></i>
  @else
    <button class="  ui circular massive right floated teal undo icon button" onclick="$('#habilitar').modal('show');" >
      <i class=" undo icon"></i>
  @endif
    </button>
  </div>
  <!-- Fin del botón de eliminar o rehabilitar, siendo el caso -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regresar al index -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/producto">
      <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Fin de botón de regresar al index -->
@stop

@section('titulo_seccion')
  {{$producto->nombre}} de {{$producto->marca}}
@stop

@section('scripts')
   <script>
      $( document ).ready(function() {
         // Inicialización de elementos de Semantic UI
         $('.ui.accordion').accordion();
         $('.ui.modal').modal();
      });
   </script>
@stop

@section('contenido')

   <!-- Inicio del Modal de confirmacion de regreso a index -->
   <div class="ui small modal" id="eliminar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Eliminar producto
      </div>
      <div class=" content">
         <p>¿Seguro que deseas eliminar el producto "{{$producto->nombre}}" con la marca "{{$producto->marca}}"?</p>
      </div>
      <div class="actions">
           <div class="ui negative cancel button">
             <i class="remove icon"></i>
             No
           </div>
           <button class="ui positive submit button "  type="submit" form="vista" value="delete">
             <i class="checkmark icon"></i>
             Si
           </button>
      </div>
   </div>
   <!-- Final del Modal de confirmacion de regreso a index -->

   <!-- Inicio del Modal de confirmacion de habilitacion de producto -->
   <div class="ui small modal" id="habilitar">
       <i class="close icon"></i>
       <div class="ui icon header">
          <i class="archive icon"></i>
          Reactivar producto
       </div>
       <div class=" content">
          <p>¿Seguro que deseas reactivar el producto "{{$producto->nombre}}" con la marca "{{$producto->marca}}"?</p>
       </div>
       <div class="actions">
           <div class="ui negative cancel button">
              <i class="remove icon"></i>
              No
           </div>
           <button class="ui positive submit button " onclick=window.location.href="/producto/reac/{{$producto->id}}">
              <i class="checkmark icon"></i>
              Si
           </button>
       </div>
   </div>
   <!-- Final del Modal de confirmacion de habilitacion de producto -->

   <!--Inicio de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
   <form id="vista" class="ui form" action="{{route('producto.destroy', $producto->id)}}" method="POST" >
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="_method" value="DELETE">
      <!--Inicio de validacion de producto no eliminada para seccion de informacion-->
      @if($producto->deleted_at == NULL)
         <div class="ui message"><i class="info circle icon"></i>Informacio del Producto </div>
      @else
         <div class="ui negative message"><i class="info circle icon"></i>Producto deshabilitado. Para editar deshacer eliminación. </div>
      @endif
      <!--Fin de validacion de producto no eliminada para seccion de informacion-->

      <!--Inicio de accordion-->
      <div class="ui styled fluid accordion">
         <!--Titulo primero-->
         <div class="title active">
           <i class="dropdown icon"></i>
           Datos generales
         </div>
         <!--Titulo primero-->

         <!--Inicio Primera seccion-->
         <div class="content active ui container grid">
            <!-- Inicio del Primer renglón -->
            <div class="row">
               <!-- Text field para el campo "Nombre del producto" -->
               <div class="ui five wide column">
                  <strong>Nombre del Producto:</strong> {{$producto->nombre}}
               </div>

               <!-- Text field para el campo "Marca" -->
               <div class="ui five wide column">
                  <strong>Marca:</strong> {{$producto->marca}}
               </div>

               <!-- Text field para el campo "Presentacion" -->
               <div class="ui five wide column">
                  <strong>Provedor:</strong> {{$producto->provedor->nombre}}
               </div>

            </div>
            <!-- Final del Primer renglón -->

            <!-- Inicio del Segundo renglón -->
            <div class="row">
               <!-- Text field para el campo "Nombre del producto" -->
               <div class="ui five wide column">
                  <strong>Contenido Neto:</strong> {{$producto->contenido_neto}}
               </div>

               <!-- Text field para el campo "Marca" -->
               <div class="ui five wide column">
                  <strong>Masa Drenada:</strong> {{$producto->masa_drenada}}
               </div>

               <!-- Text field para el campo "Presentacion" -->
               <div class="ui five wide column">
                  <strong>Unidad de Medida:</strong> {{$producto->unidad_de_medida->nombre}}
               </div>

            </div>
            <!-- Final del Segundo renglón -->

            <!-- Inicio del Tercer renglón -->
            <div class="row">
               <!-- Text field para el campo "Presentacion" -->
               <div class="ui five wide column">
                  <strong>Presentación:</strong> {{$producto->presentacion->nombre}}
               </div>

               <!-- Text field para el campo "Marca" -->
               <div class="ui five wide column">
                  <strong>Precio:</strong> {{$producto->precio}}
               </div>

            </div>
            <!-- Final del Segundo renglón -->

         </div>
         <!--Fin Primera seccion-->
      </div>
      <!--Final de accordion-->

   </form>
   <!--Final de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
@stop
