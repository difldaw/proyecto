@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón crear -->
  <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/producto/create">
    <i class="plus icon"></i>
  </button>
  <!-- Fin del botón crear -->
@stop

@section('titulo_seccion')
  Productos
@stop

@section('scripts')
  <script type="text/javascript">
    $( document ).ready(function() {
      //Ocultar tabla con productos activas/inactivas
      $(".hidden").hide();
    });
  </script>
  <script>
  /*Función para mostrar la tabla de productos activas y ocultar
    activas/inactivas y viceversa*/
    function eliminados(){
      if($('.ui.checkbox').checkbox('is checked')){
        $(".hidden").show();
      }else{
        $(".hidden").hide();
      }
    }
  </script>
@stop

@section('contenido')
  <!--Inicio de container independiente de tablas-->
  <div class="ui grid container">
    <div class="row">
      <div class="twelve wide column"></div>
      <div class="four wide column">
        <!-- Checkbox para mostrar las diferentes tablas -->
        <div class=" ui slider checkbox ">
          <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
          <label>Mostrar eliminadas</label>
        </div>
      </div>
    </div>
  </div>
  <!--fin de container independiente de tabla-->

   <!--Inicio del contenidor de cartas-->
   <div class="ui four cards">
      @foreach($productos as $producto)
         @if($producto->deleted_at != NULL)
         <!-- Inicio de la carta -->
         <div class="ui card red hidden" onclick=window.location.href="/producto/{{$producto->id}}">
            <div class="content">
               <div class="ui red right ribbon label">
                  <i class="trash icon"></i> Eliminado
               </div>
         @else
         <div class="ui card green" onclick=window.location.href="/producto/{{$producto->id}}">
            <div class="content">
               <div class="ui green right ribbon label">
                  <i class="save icon"></i> Activa
               </div>
         @endif
               <div class="header">{{$producto->nombre}}</div>
            </div>

            <div class="content">
               <!-- Subtitulo para mostrar la marca del producto -->
               <h4 class="ui sub header">{{$producto->marca}}</h4>
               <div class="ui small feed">
                  <!-- Renglón para mostrar el Provedor del producto -->
                  <div class="event"><div class="content"><div class="summary">
                     Provedor: {{$producto->provedor->nombre}}
                  </div></div></div>

                  <!-- Renglón para mostrar la Presentación del producto -->
                  <div class="event"><div class="content"><div class="summary">
                     Presentación: {{$producto->presentacion->nombre}}
                  </div></div></div>

                  <div class="event"><div class="content"><div class="summary">
                     Contenido Neto: {{$producto->contenido_neto}}
                  </div></div></div>

                  <!-- Renglón para mostrar la Masa_drenada del producto -->
                  <div class="event"><div class="content"><div class="summary">
                     Masa Drenada: {{$producto->masa_drenada}}
                  </div></div></div>

                  <!-- Renglón para mostrar la Unidad de Medida del producto -->
                  <div class="event"><div class="content"><div class="summary">
                     Unidad de Medida: {{$producto->unidad_de_medida->nombre}}
                  </div></div></div>

                  <!-- Renglón para mostrar el Precio del producto -->
                  <div class="event"><div class="content"><div class="summary">
                     Precio: {{$producto->precio}}
                  </div></div></div>

               </div>
            </div>
         </div>
         <!-- Fin de la carta -->
      @endforeach
   </div>
   <!--Fin del contenidor de cartas-->
   <br>
   <div class="ui right floated pagination menu">
      <a href="{!! $productos->previousPageUrl() !!}" class="icon item">
         <i class="left chevron icon "></i>
      </a>
      @for($pages = 1; $pages <= $productos->lastPage(); $pages++)
      <a href="{!! $productos->url($pages) !!}" class="item ">{{$pages}}</a>
      @endfor
      <a href="{!! $productos->nextPageUrl() !!}" class="icon item">
         <i class="right chevron icon "></i>
      </a>
   </div>

@stop
