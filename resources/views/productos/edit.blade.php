@extends('layouts.master')

@section('botones')
  <!-- Inicio de botón de guardado -->
  <div class="row">
    <button class="ui circular massive right floated teal save icon submit button" onclick="envio()" >
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final de botón de guardado -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regreso -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final de botón de regreso -->
@stop

@section('titulo_seccion')
  {{$producto->nombre}} de {{$producto->marca}}
@stop

@section('scripts')
   <script type="text/javascript">
      // Función para verificar si el form esta validado
      function envio(){
         $('#editar_form').form('validate form');
         //Si es true, se mostrara el modal con el id "guardar"
         if ($('#editar_form').form('is valid')){
            $('#guardar').modal('show');
         }
      }
   </script>

   <script type="text/javascript">
      $(document).ready(function(){
         //Inicialización de elementos de Semantic UI
         $('.ui.accordion').accordion();
         $('.ui.modal').modal();
         $('.menu .item').tab();
         $('select').dropdown();
         //Validaciones dentro de la forma con id "registro_form"
         $('.ui.form').form({
            inline: true,
            fields:{
               nombre:{
                  identifier: 'nombre',
                  rules:[{
                     type: 'empty',
                     prompt: 'Nombre necesario'
                  }]
               },
               marca:{
                  identifier: 'marca',
                  rules:[{
                     type: 'empty',
                     prompt: 'Marca necesaria'
                  }]
               },
               presentacion_id:{
                  identifier: 'presentacion_id',
                  rules:[{
                     type: 'empty',
                     prompt: 'Seleccione una presentacion'
                  }]
               },
               contenido_neto:{
                  identifier: 'contenido_neto',
                  rules:[{
                     type: 'empty',
                     prompt: 'Contenido neto necesario'
                  }]
               },
               masa_drenada:{
                  identifier: 'masa_drenada',
                  rules:[{
                     type   : 'empty',
                     prompt : 'Masa Drenada necesaria'
                  }]
               },
               unidad_de_medida_id:{
                  identifier: 'unidad_de_medida_id',
                  rules:[{
                     type: 'empty',
                     prompt: 'Seleccione una Unidad de medida'
                  }]
               },
               provedor_id:{
                  identifier: 'provedor_id',
                  rules:[{
                     type: 'empty',
                     prompt: 'Provedor necesario'
                  }]
               },
               precio:{
                  identifier: 'precio',
                  rules:[{
                     type: 'empty',
                     prompt: 'Precio necesario'
                  }]
               }
            }
         });
      });
   </script>
@stop

@section('contenido')
   <!--Inicio de Seccion de mensaje de informacion-->
   <div class="ui teal message"><i class="info circle icon"></i>Informacion del Producto </div>
   <!--Fin de Seccion de mensaje de informacion-->

   <!--Inicio Modal de confirmacion de regreso a show-->
   <div class="ui small modal" id="regresar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Descartar registro
      </div>
      <div class=" content">
         <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
      </div>
      <div class="actions">
         <div class="ui negative cancel button">
            <i class="remove icon"></i>
            No
         </div>
         <div class="ui positive button" onclick=window.location.href="/producto/{{$producto->id}}">
            <i class="checkmark icon"></i>
            Si
         </div>
      </div>
   </div>
   <!--Final Modal de confirmacion de regreso a show-->

   <!--Inicio Modal de confirmacion de envio de modificaciones-->
   <div class="ui small modal" id="guardar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Guardar cambios
      </div>
      <div class="content">
         <p>¿Deseas guardar los cambios?</p>
      </div>
      <div class="actions">
         <div class="ui negative cancel button">
            <i class="remove icon"></i>
            No
         </div>
         <button class="ui positive submit button" type="submit" form="editar_form">
            <i class="checkmark icon"></i>
            Si
         </button>
      </div>
   </div>
   <!--Final Modal de confirmacion de envio de modificaciones-->

   <!--Inicio del Contenedor de forma de editar-->
   <!-- <div class="ui grid container"> -->
      <!--Inicio de la forma de editar-->
      <form id="editar_form" class="ui form" action="{{route('producto.update', $producto->id)}}" method="POST">
         <input type="hidden" name="_token" value="{{csrf_token()}}">
         <input type="hidden" name="_method" value="PATCH">

         <!--Inicio de accordion-->
         <div class="ui styled fluid accordion">
            <!--Titulo primero-->
            <div class="title active">
              <i class="dropdown icon"></i>
              Datos generales
            </div>
            <!--Titulo primero-->

            <!--Inicio Primera seccion-->
            <div class="content active ui container grid">


               <!-- Inicio del Primer renglón -->
               <div class="row">
                  <!-- Input text para el campo de "Nombre" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Nombre</label>
                     <input type="text" placeholder="Ej. Cereal..." name="nombre" value="{{$producto->nombre}}">
                  </div>

                  <div class="ui one wide field"></div>

                  <!-- Input text para el campo de "Marca" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Marca</label>
                     <input type="text" placeholder="Ej. Zucaritas..." name="marca" value="{{$producto->marca}}">
                  </div>

                  <div class="ui one wide field"></div>

                  <!-- Select para el campo de "Provedor" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Provedor</label>
                     <select class="ui fluid dropdown" name="provedor_id">
                        @foreach($provedores as $provedor)
                           @if($producto->provedor_id == $provedor->id_provedor)
                              <option value="{{$provedor->id_provedor}}" selected="selected">{{$provedor->nombre}}</option>
                           @else
                              <option value="{{$provedor->id_provedor}}">{{$provedor->nombre}}</option>
                           @endif
                        @endforeach
                     </select>
                  </div>

               </div>
               <!-- Fin del Primer renglón -->

               <!-- Inicio del Segundo renglón -->
               <div class="row">
                  <!-- Input text para el campo de "Contenido Neto" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Contenido Neto</label>
                     <input type="text" placeholder="Ej. 44.5..." name="contenido_neto" value="{{$producto->contenido_neto}}">
                  </div>

                  <div class="ui one wide field"></div>

                  <!-- Input text para el campo de "Masa Drenada" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Masa Drenada</label>
                     <input type="text" placeholder="Ej. 60.5..." name="masa_drenada" value="{{$producto->masa_drenada}}">
                  </div>

                  <div class="ui one wide field"></div>

                  <!-- Select para el campo de "Unidad de Medida" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Unidad de Medida</label>
                     <select class="ui fluid dropdown" name="unidad_de_medida_id">
                        @foreach($unidades_de_medida as $unidad_de_medida)
                           @if($producto->unidad_de_medida_id == $unidad_de_medida->id)
                              <option value="{{$unidad_de_medida->id}}" selected="selected">{{$unidad_de_medida->nombre}}</option>
                           @else
                              <option value="{{$unidad_de_medida->id}}">{{$unidad_de_medida->nombre}}</option>
                           @endif
                        @endforeach
                     </select>
                  </div>

               </div>
               <!-- Fin del Segundo renglón -->

               <!-- Inicio del Tercer renglón -->
               <div class="row">
                  <!-- Select para el campo de "Presentacion" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Presentacion</label>
                     <select class="ui fluid dropdown" name="presentacion_id">
                        @foreach($presentaciones as $presentacion)
                           @if($producto->presentacion_id == $presentacion->id)
                              <option value="{{$presentacion->id}}" selected="selected">{{$presentacion->nombre}}</option>
                           @else
                              <option value="{{$presentacion->id}}">{{$presentacion->nombre}}</option>
                           @endif
                        @endforeach
                     </select>
                  </div>

                  <div class="ui one wide field"></div>

                  <!-- Input text para el campo de "Masa Drenada" -->
                  <div class="ui four wide required field">
                     <label class="prompt">Precio</label>
                     <input type="text" placeholder="Ej. 35.90..." name="precio" value="{{$producto->precio}}">
                  </div>

               </div>
               <!-- Fin del Tercer renglón -->


            </div>
            <!--Fin Primera seccion-->

         </div>
         <!--Fin de accordion-->
      </form>
      <!--Fin de la forma de editar-->
   <!-- </div> -->
   <!--Fin del Contenedor de forma de editar-->
@stop
