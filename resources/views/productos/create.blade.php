@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón de guardado -->
  <div class="row">
    <button id="save" class="ui circular massive right floated teal save icon submit button" onclick="envio()">
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de regresar -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
@stop

@section('titulo_seccion')
  Registrar Producto
@stop

@section('scripts')
  <script type="text/javascript">
    // Función para verificar si el form esta validado
    function envio(){
      $('#registro_form').form('validate form');
      //Si es true, se mostrara el modal con el id "guardar"
      if($('#registro_form').form('is valid')){
        $('#guardar').modal('show');
      }
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      //Inicialización de elementos de Semantic UI
      $('.ui.modal').modal();
      $('.menu .item').tab();
      $('select').dropdown();
      //Validaciones dentro de la forma con id "registro_form"
      $('#registro_form').form({
        inline: true,
        fields:{
          nombre:{
            identifier: 'nombre',
            rules:[{
              type: 'empty',
              prompt: 'Nombre necesario'
            }]
          },
          marca:{
            identifier: 'marca',
            rules:[{
              type: 'empty',
              prompt: 'Marca necesaria'
            }]
          },
          presentacion_id:{
            identifier: 'presentacion_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione una presentacion'
            }]
          },
          contenido_neto:{
            identifier: 'contenido_neto',
            rules:[{
              type: 'empty',
              prompt: 'Contenido neto necesario'
            }]
          },
          masa_drenada:{
            identifier: 'masa_drenada',
            rules:[{
              type   : 'empty',
              prompt : 'Masa Drenada necesaria'
            }]
          },
          unidad_de_medida_id:{
            identifier: 'unidad_de_medida_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione una Unidad de medida'
            }]
          },
          provedor_id:{
            identifier: 'provedor_id',
            rules:[{
              type: 'empty',
              prompt: 'Provedor necesario'
            }]
          },
          precio:{
            identifier: 'precio',
            rules:[{
              type: 'empty',
              prompt: 'Precio necesario'
            }]
          }
        }
      });
    });
  </script>
@stop

@section('contenido')
   <!--Inicio del Modal de confirmacion de regresar a index-->
  <div class="ui small modal" id="regresar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Descartar registro
    </div>
    <div class=" content">
        <p>¿Estás seguro que deseas regresar?. Se perderan todos los datos sin guardar.</p>
    </div>
    <div class="actions">
      <div class="ui negative cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui positive button" onclick=window.location.href="/producto">
        <i class="checkmark icon"></i>
        Si
      </div>
    </div>
  </div>
  <!--Final del Modal de confirmacion de regresar a index-->

  <!--Inicio del Modal de confirmacion de envio de formulario-->
  <div class="ui modal" id="guardar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Guardar registro nuevo
    </div>
    <div class="content">
        <p>¿Deseas guardar el registro de la localidad?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button" type="submit" form="registro_form">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!--Final del Modal de confirmacion de envio de formulario-->

  <!--Inicio del Contenedor de forma de registro-->
  <div class="ui grid container">
    <div class="row">
      <div class="column">
        <!--Inicio de la forma de registro-->
        <form id="registro_form" class="ui form" action="{{route('producto.store')}}" method="POST">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="_method" value="POST">
          <h4 class="ui horizontal divider header">
            <i class="tag icon">
            </i>
            Datos generales
          </h4>
          <!-- Inicio de la Seccion de datos generales -->
            <!-- Inicio de la primera fila -->
            <div class="three fields">
              <!-- Input text para el campo de "Nombre" -->
              <div class="seven wide required field">
                <label class="prompt">Nombre</label>
                <input type="text" placeholder="Ej. Cereal..." name="nombre">
              </div>

              <!-- Input text para el campo de "Marca" -->
              <div class="six wide required field">
                <label class="prompt">Marca</label>
                <input type="text" placeholder="Ej. Zucaritas..." name="marca">
              </div>

              <!-- Select para el campo de "Presentacion" -->
              <div class="three wide required field">
                <label class="prompt">Presentacion</label>
                <select class="ui fluid dropdown" name="presentacion_id">
                  <option value=""></option>
                  @foreach($presentaciones as $presentacion)
                  <option value="{{$presentacion->id}}">{{$presentacion->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- Final de la primera fila -->

            <!-- Inicio de la segunda fila -->
              <div class="five fields">

                 <!-- Input text para el campo de "Provedor" -->
                 <div class="three wide required field">
                    <label class="prompt">Provedor</label>
                    <select class="ui fluid dropdown" name="provedor_id">
                      <option value=""></option>
                      @foreach($provedores as $provedor)
                      <option value="{{$provedor->id_provedor}}">{{$provedor->nombre}}</option>
                      @endforeach
                    </select>
                 </div>

                 <!-- Input text para el campo de "Contenido Neto" -->
                 <div class="four wide required field">
                    <label class="prompt">Contenido Neto</label>
                    <input type="text" placeholder="Ej. 4.56.." name="contenido_neto">
                 </div>

                 <!-- Input text para el campo de "Masa Drenada" -->
                 <div class="three wide required field">
                   <label class="prompt">Masa Drenada</label>
                   <input type="text" placeholder="Ej. 60.5..." name="masa_drenada">
                 </div>

                 <!-- Select para el campo de "Unidad de Medida" -->
                 <div class="three wide required field">
                   <label class="prompt">Unidad de Medida</label>
                   <select class="ui fluid dropdown" name="unidad_de_medida_id">
                     <option value=""></option>
                     @foreach($unidades_de_medida as $unidad_de_medida)
                     <option value="{{$unidad_de_medida->id}}">{{$unidad_de_medida->nombre}}</option>
                     @endforeach
                   </select>
                 </div>

                 <!-- Input text para el campo de "Masa Drenada" -->
                 <div class="three wide required field">
                   <label class="prompt">Precio</label>
                   <input type="text" placeholder="Ej. 35.90..." name="precio">
                 </div>
              </div>
            <!-- Final de la segunda fila -->
          <!-- Final de la Seccion de datos generales -->
        </form>
        <!--Final de la forma de registro-->
      </div>
    </div>
  </div>
  <!--Final del Contenedor de forma de registro-->
@stop
