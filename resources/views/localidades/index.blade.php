@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón crear -->
  <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/localidad/create">
    <i class="plus icon"></i>
  </button>
  <!-- Fin del botón crear -->
@stop

@section('titulo_seccion')
  Localidades
@stop

@section('scripts')
  <script type="text/javascript">
    $( document ).ready(function() {
      //Ocultar tabla con localidades activas/inactivas
      $("#todas").hide();
      //Mostrar tabla con localidades activas
      $("#localidades").show();
    });
  </script>
  <script>
  /*Función para mostrar la tabla de localidades activas y ocultar
    activas/inactivas y viceversa*/
    function eliminados(){
      if($('.ui.checkbox').checkbox('is checked')){
        $("#todas").show();
        $("#localidades").hide();
      }else{
        $("#localidades").show();
        $("#todas").hide();
      }
    }
  </script>
@stop

@section('contenido')
  <!--Inicio de container independiente de tablas-->
  <div class="ui grid container">
    <div class="row">
      <div class="twelve wide column"></div>
      <div class="four wide column">
        <!-- Checkbox para mostrar las diferentes tablas -->
        <div class=" ui slider checkbox ">
          <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
          <label>Mostrar eliminadas</label>
        </div>
      </div>
    </div>
  </div>
  <!--fin de container independiente de tabla-->

  <!--Inicio de la tabla sin filtrado de eliminados-->
  <table class="ui selectable sortable teal celled table" id="todas">
    <thead>
      <tr>
        <th class="three wide center aligned">Clave de Localidad</th>
        <th class="three wide center aligned">Nombre</th>
        <th class="three wide center aligned">Municipio</th>
        <th class="two wide center aligned">Latitud</th>
        <th class="two wide center aligned">Longitud</th>
        <th class="two wide center aligned">Altitud</th>
        <th class="two wide center aligned">Clave Carta</th>
        <th class="three wide center aligned">Grado de Marginación</th>
        <th class="one wide center aligned">Ver</th>
      </tr>
    </thead>
    <tbody>
      @foreach($todas as $localidad)
        @if($localidad->deleted_at != NULL)
          <tr class="negative">
        @else
          <tr class="positive">
        @endif
          <td class="center aligned">{{$localidad->clave_localidad}}</td>
          <td class="center aligned">{{$localidad->nombre}}</td>
          <td class="center aligned">{{$localidad->municipio->nombre}}</td>
          <td class="center aligned">{{$localidad->latitud}}</td>
          <td class="center aligned">{{$localidad->longitud}}</td>
          <td class="center aligned">{{$localidad->altitud}}</td>
          <td class="center aligned">{{$localidad->clave_carta}}</td>
          <td class="center aligned">{{$localidad->grado_de_marginacion->nombre}}</td>
          <td onclick=window.location.href="/localidad/{{$localidad->id}}" class=" selectable center aligned">
             <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th colspan="9">
          <div class="ui right floated pagination menu">
            <a href="{!! $todas->previousPageUrl() !!}" class="icon item">
              <i class="left chevron icon "></i>
            </a>
            @for($pages = 1; $pages <= $todas->lastPage(); $pages++)
                <a href="{!! $todas->url($pages) !!}" class="item ">{{$pages}}</a>
            @endfor
            <a href="{!! $todas->nextPageUrl() !!}" class="icon item">
              <i class="right chevron icon "></i>
            </a>
          </div>
        </th>
      </tr>
    </tfoot>
  </table>
  <!--Final de la tabla sin filtrado de eliminados-->

  <!--Inicio de la tabla con filtrado de eliminados-->
  <table class="ui selectable sortable teal celled table" id="localidades">
    <thead>
      <tr>
        <th class="three wide center aligned">Clave de Localidad</th>
        <th class="three wide center aligned">Nombre</th>
        <th class="three wide center aligned">Municipio</th>
        <th class="two wide center aligned">Latitud</th>
        <th class="two wide center aligned">Longitud</th>
        <th class="two wide center aligned">Altitud</th>
        <th class="two wide center aligned">Clave Carta</th>
        <th class="three wide center aligned">Grado de Marginación</th>
        <th class="one wide center aligned">Ver</th>
      </tr>
    </thead>
    <tbody>
      @foreach($localidades as $localidad)
        <tr>
          <td class="center aligned">{{$localidad->clave_localidad}}</td>
          <td class="center aligned">{{$localidad->nombre}}</td>
          <td class="center aligned">{{$localidad->municipio->nombre}}</td>
          <td class="center aligned">{{$localidad->latitud}}</td>
          <td class="center aligned">{{$localidad->longitud}}</td>
          <td class="center aligned">{{$localidad->altitud}}</td>
          <td class="center aligned">{{$localidad->clave_carta}}</td>
          <td class="center aligned">{{$localidad->grado_de_marginacion->nombre}}</td>
          <td onclick=window.location.href="/localidad/{{$localidad->id}}" class=" selectable center aligned">
             <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th colspan="9">
          <div class="ui right floated pagination menu">
            <a href="{!! $localidades->previousPageUrl() !!}" class="icon item">
              <i class="left chevron icon "></i>
            </a>
            @for($pages = 1; $pages <= $localidades->lastPage(); $pages++)
                <a href="{!! $localidades->url($pages) !!}" class="item ">{{$pages}}</a>
            @endfor
            <a href="{!! $localidades->nextPageUrl() !!}" class="icon item">
              <i class="right chevron icon "></i>
            </a>
          </div>
        </th>
      </tr>
    </tfoot>
  </table>
  <!--Final de la tabla con filtrado de eliminados-->
@stop
