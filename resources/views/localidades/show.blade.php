@extends('layouts.master')

@section('botones')
  <div class="row">
  @if($localidad->deleted_at == NULL)
    <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/localidad/{{$localidad->id}}/edit">
  @else
    <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/localidad/{{$localidad->id}}/edit">
  @endif
      <i class="write icon"></i>
    </button>
  </div>

  <div style="visibility:hidden">
    <br />..
  </div>

  <div class="row">
  @if($localidad->deleted_at == NULL)
    <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
      <i class="trash outline icon"></i>
  @else
    <button class="  ui circular massive right floated teal undo icon button" onclick="$('#habilitar').modal('show');" >
      <i class=" undo icon"></i>
  @endif
    </button>
  </div>

  <div style="visibility:hidden">
    <br />..
  </div>

  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/localidad">
      <i class="chevron left icon"></i>
    </button>
  </div>
@stop

@section('titulo_seccion')
  {{$localidad->nombre}}
@stop

@section('scripts')
  <script>
    $( document ).ready(function() {
      // Inicialización de elementos de Semantic UI
      $('.ui.accordion').accordion();
    });
  </script>

  <script>
      // Función para inicializar el mapa de google
      function init_map(){
        var myOptions = {
        zoom:17,
        center:new google.maps.LatLng({{$localidad->latitud}},{{$localidad->longitud}}),
        mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({
          map: map,position: new google.maps.LatLng({{$localidad->latitud}},{{$localidad->longitud}})});
          infowindow = new google.maps.InfoWindow({
            content:'<strong>{{$localidad->nombre}}</strong><br/>Delegación {{$localidad->municipio->nombre}}'});
            google.maps.event.addListener(marker, 'click', function(){
              infowindow.open(map,marker);});infowindow.open(map,marker);
      }google.maps.event.addDomListener(window, 'load', init_map);
  </script>
@stop

@section('contenido')
  <!-- Inicio del Modal de confirmacion de regreso a index -->
  <div class="ui small modal" id="eliminar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Eliminar Localidad
    </div>
    <div class=" content">
      <p>¿Seguro que deseas eliminar la localidad {{$localidad->nombre}}?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button "  type="submit" form="vista" value="delete">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!-- Final del Modal de confirmacion de regreso a index -->

  <!-- Inicio del Modal de confirmacion de habilitacion de localidad -->
  <div class="ui small modal" id="habilitar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Reactivar Localidad
    </div>
    <div class=" content">
      <p>¿Seguro que deseas reactivar la localidad {{$localidad->nombre}}?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button " onclick=window.location.href="/localidad/reac/{{$localidad->id}}">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!-- Final del Modal de confirmacion de habilitacion de localidad -->

  <!--Inicio de forma de visualizacion que utiliza el metodo destroy para eliminar localidad-->
  <form id="vista" action="{{route('localidad.destroy', $localidad->id)}}" method="POST" >
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="_method" value="DELETE">
    <!--Inicio de validacion de localidad no eliminada para seccion de informacion-->
    @if($localidad->deleted_at == NULL)
      <div class="ui message"><i class="info circle icon"></i>Informacio de la Localidad </div>
    @else
      <div class="ui negative message"><i class="info circle icon"></i>Localidad deshabilitada. Para editar deshacer eliminación. </div>
    @endif
    <!--Final de validacion de localidad no eliminada para seccion de informacion-->

    <!--Inicio de Seccion de mapa-->
    <div  class="row">
      <div id='gmap_canvas' style='height:300px;width:100%;'>
      </div>
    </div>
    <!--Fin de Seccion de mapa-->
    <br>
    <!--Inicio de accordion-->
    <div class="ui styled fluid accordion">
      <!--Titulo primero-->
      <div class="title active">
        <i class="dropdown icon"></i>
        Datos generales
      </div>
      <!--Titulo primero-->

      <!--Primera seccion-->
      <div class="content active ui container grid">
        <!-- Inicio del Primer renglón -->
        <div class="row">
          <!-- Text field para el campo "Nombre de la Localidad" -->
          <div class="ui eight wide column">
            <strong>Nombre de la Localidad:</strong> {{$localidad->nombre}}
          </div>
        </div>
        <!-- Final del Primer renglón -->

        <!-- Inicio del Segundo renglón -->
        <div class="row">
          <!-- Text field para el campo "Clave de Localidad" -->
          <div class="ui three wide column">
            <strong>Clave de Localidad:</strong> {{$localidad->clave_localidad}}
          </div>
          <!-- Text field para el campo "Nombre Carta" -->
          <div class="ui three wide column">
            <strong>Clave Carta:</strong> {{$localidad->clave_carta}}
          </div>
        </div>
        <!-- Final del Segundo renglón -->

        <!-- Inicio del Tercer renglón -->
        <div class="row">
          <!-- Text field para el campo "Municipio" -->
          <div class="ui three wide column">
            <strong>Delegación:</strong> {{$localidad->municipio->nombre}}
          </div>
          <!-- Text field para el campo "Grado de Marginación" -->
          <div class="ui four wide column">
            <strong>Grado de Marginación:</strong> {{$localidad->grado_de_marginacion->nombre}}
          </div>
        </div>
        <!-- Final del Tercer renglón -->

        <!-- Inicio del Cuarto renglón -->
        <div class="row">
          <!-- Text field para el campo "Latitud" -->
          <div class="ui two wide column">
            <strong>Latidud:</strong> {{$localidad->latitud}}
          </div>
          <!-- Text field para el campo "Longitud" -->
          <div class="ui two wide column">
            <strong>Longitud:</strong> {{$localidad->longitud}}
          </div>
          <!-- Text field para el campo "Altitud" -->
          <div class="ui two wide column">
            <strong>Altitud:</strong> {{$localidad->altitud}}
          </div>
        </div>
        <!-- Final del Cuarto renglón -->
      </div>
      <!--Primera seccion-->
    </div>
    <!--Fin de accordion-->
  </form>
  <!--Final de forma de visualizacion que utiliza el metodo destroy para eliminar Localidad-->

@stop
