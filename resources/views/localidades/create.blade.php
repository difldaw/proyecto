@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón de guardado -->
  <div class="row">
    <button id="save" class="ui circular massive right floated teal save icon submit button" onclick="envio()">
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de regresar -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
@stop

@section('titulo_seccion')
  Registrar Localidad
@stop

@section('scripts')
  <script type="text/javascript">
    // Función para verificar si el form esta validado
    function envio(){
      $('#registro_form').form('validate form');
      //Si es true, se mostrara el modal con el id "guardar"
      if($('#registro_form').form('is valid')){
        $('#guardar').modal('show');
      }
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      //Inicialización de elementos de Semantic UI
      $('.ui.modal').modal();
      $('.menu .item').tab();
      $('select').dropdown();
      //Validaciones dentro de la forma con id "registro_form"
      $('#registro_form').form({
        inline: true,
        fields:{
          nombre:{
            identifier: 'nombre',
            rules:[{
              type: 'empty',
              prompt: 'Nombre necesario'
            }]
          },
          clave_carta:{
            identifier: 'clave_carta',
            rules:[{
              type: 'exactLength[6]',
              prompt: '6 carácteres'
            }]
          },
          clave_localidad:{
            identifier: 'clave_localidad',
            rules:[{
              type: 'empty',
              prompt: 'Clave de Loclidad necesaria'
            }]
          },
          latitud:{
            identifier: 'latitud',
            rules:[{
              type: 'regExp[/^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$/]',
              prompt: 'Longitud incorrecta'
            }]
          },
          longitud:{
            identifier: 'longitud',
            rules:[{
              type   : 'regExp[/^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$/]',
              prompt : 'Longitud incorrecta'
            }]
          },
          altitud:{
            identifier: 'altitud',
            rules:[{
              type: 'empty',
              prompt: 'Altitud necesario'
            }]
          },
          municipio_id:{
            identifier: 'municipio_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un Municipio'
            }]
          },
          grado_de_marginacion_id:{
            identifier: 'grado_de_marginacion_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un Grado de Marginación'
            }]
          }
        }
      });
    });
  </script>
@stop

@section('contenido')
  <!--Inicio del Modal de confirmacion de regresar a index-->
  <div class="ui small modal" id="regresar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Descartar registro
    </div>
    <div class=" content">
        <p>¿Estás seguro que deseas regresar?. Se perderan todos los datos sin guardar.</p>
    </div>
    <div class="actions">
      <div class="ui negative cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui positive button" onclick=window.location.href="/localidad">
        <i class="checkmark icon"></i>
        Si
      </div>
    </div>
  </div>
  <!--Final del Modal de confirmacion de regresar a index-->

  <!--Inicio del Modal de confirmacion de envio de formulario-->
  <div class="ui modal" id="guardar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Guardar registro nuevo
    </div>
    <div class="content">
        <p>¿Deseas guardar el registro de la localidad?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button" type="submit" form="registro_form">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!--Final del Modal de confirmacion de envio de formulario-->

  <!--Inicio del Contenedor de forma de registro-->
  <div class="ui grid container">
    <div class="row">
      <div class="column">
        <!--Inicio de la forma de registro-->
        <form id="registro_form" class="ui form" action="{{route('localidad.store')}}" method="POST">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="_method" value="POST">
          <h4 class="ui horizontal divider header">
            <i class="tag icon">
            </i>
            Datos generales
          </h4>
          <!-- Inicio de la Seccion de datos generales -->
            <!-- Inicio de la primera fila -->
            <div class="three fields">
              <!-- Input text para el campo de "Nombre" -->
              <div class="twelve wide required field">
                <label class="prompt">Nombre</label>
                <input type="text" placeholder="Ej. El Chapulín..." name="nombre">
              </div>

              <!-- Input text para el campo de "Clave de la Localidad" -->
              <div class="two wide required field">
                <label class="prompt">Clave de Localidad</label>
                <input type="text" placeholder="Ej. 76..." name="clave_localidad">
              </div>

              <!-- Input text para el campo de "Clave Carta" -->
              <div class="two wide required field">
                <label class="prompt">Clave Carta</label>
                <input type="text" placeholder="Ej. F36C98..." name="clave_carta">
              </div>
            </div>
            <!-- Final de la primera fila -->

            <!-- Inicio de la segunda fila -->
              <div class="five fields">
                <!-- Select para el campo de "Municipio" -->
                <div class="five wide required field">
                  <label class="prompt">Municipio</label>
                  <select class="ui fluid dropdown" name="municipio_id">
                    <option value=""></option>
                    @foreach($municipios as $municipio)
                    <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                    @endforeach
                  </select>
                </div>

                <!-- Select para el campo de "Grado de Marginación" -->
                <div class="five wide required field">
                  <label class="prompt">Grado de Marginación</label>
                  <select class="ui fluid dropdown" name="grado_de_marginacion_id">
                    <option value=""></option>
                    @foreach($grados_de_marginacion as $grado_de_marginacion)
                    <option value="{{$grado_de_marginacion->id}}">{{$grado_de_marginacion->nombre}}</option>
                    @endforeach
                  </select>
                </div>

                <!-- Input text para el campo de "Latitud" -->
                <div class="two wide required field">
                  <label class="prompt">Latitud</label>
                  <input type="text" name="latitud" placeholder="Ej. 87...">
                </div>

                <!-- Input text para el campo de "Longitud" -->
                <div class="two wide required field">
                  <label class="prompt">Longitud</label>
                  <input type="text" name="longitud" placeholder="Ej. 20...">
                </div>

                <!-- Input text para el campo de "Altitud" -->
                <div class="two wide required field">
                  <label class="prompt">Altitud</label>
                  <input type="text" name="altitud" placeholder="Ej. 70...">
                </div>
              </div>
            <!-- Final de la segunda fila -->
          <!-- Final de la Seccion de datos generales -->
        </form>
        <!--Final de la forma de registro-->
      </div>
    </div>
  </div>
  <!--Final del Contenedor de forma de registro-->
@stop
