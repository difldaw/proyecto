@extends('layouts.master')

@section('botones')
  <!-- Inicio de botón de guardado -->
  <div class="row">
    <button class="ui circular massive right floated teal save icon submit button" onclick="envio()" >
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final de botón de guardado -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regreso -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final de botón de regreso -->
@stop

@section('titulo_seccion')
  {{$localidad->nombre}}
@stop

@section('scripts')
  <script type="text/javascript">
    // Función para verificar si el form esta validado
    function envio(){
      $('.ui.form').form('validate form');
      //Si es true, se mostrara el modal con el id "guardar"
      if ($('.ui.form').form('is valid')){
        $('#guardar').modal('show');
      }
    }

    //Función para inicializar el mapa de google
    function init_map(){
      var myOptions = {
      zoom:17,
      center:new google.maps.LatLng({{$localidad->latitud}},{{$localidad->longitud}}),
      mapTypeId: google.maps.MapTypeId.ROADMAP};
      map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
      marker = new google.maps.Marker({
        map: map,position: new google.maps.LatLng({{$localidad->latitud}},{{$localidad->longitud}})});
        infowindow = new google.maps.InfoWindow({
          content:'<strong>{{$localidad->nombre}}</strong><br/> Delegación: {{$localidad->municipio->nombre}}'});
          google.maps.event.addListener(marker, 'click', function(){
            infowindow.open(map,marker);});infowindow.open(map,marker);

    }google.maps.event.addDomListener(window, 'load', init_map);
    </script>
  <script type="text/javascript">
    $( document ).ready(function(){

        //Inicialización de elementos de Semantic UI
        $('.ui.modal').modal();
        $('.ui.accordion').accordion();
        $('.menu .item').tab();
        $('select').dropdown();

        //Validaciones dentro de la forma con id "registro_form"
        $('.ui.form').form({
          inline: true,
          fields:{
            nombre:{
              identifier: 'nombre',
              rules:[{
                type: 'empty',
                prompt: 'Nombre necesario'
              }]
            },
            clave_carta:{
              identifier: 'clave_carta',
              rules:[{
                type: 'exactLength[6]',
                prompt: '6 carácteres'
              }]
            },
            clave_localidad:{
              identifier: 'clave_localidad',
              rules:[{
                type: 'empty',
                prompt: 'Clave de Loclidad necesaria'
              }]
            },
            latitud:{
              identifier: 'latitud',
              rules:[{
                type: 'regExp[/^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$/]',
                prompt: 'Longitud incorrecta'
              }]
            },
            longitud:{
              identifier: 'longitud',
              rules:[{
                type   : 'regExp[/^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$/]',
                prompt : 'Longitud incorrecta'
              }]
            },
            altitud:{
              identifier: 'altitud',
              rules:[{
                type: 'empty',
                prompt: 'Altitud necesario'
              }]
            },
            municipio_id:{
              identifier: 'municipio_id',
              rules:[{
                type: 'empty',
                prompt: 'Seleccione un Municipio'
              }]
            },
            grado_de_marginacion_id:{
              identifier: 'grado_de_marginacion_id',
              rules:[{
                type: 'empty',
                prompt: 'Seleccione un Grado de Marginación'
              }]
            }
          }
        });
      });
  </script>
@stop

@section('contenido')
<!--Inicio de Seccion de mensaje de informacion-->
<div class="ui teal message"><i class="info circle icon"></i>Informacion de la localidad </div>
<!--Fin de Seccion de mensaje de informacion-->

<!--Seccion de mapa-->
<div  class="row">
  <div id='gmap_canvas' style='height:300px;width:100%;'>
  </div>
</div>
<!--Seccion de mapa-->
<br>
<!--Inicio Modal de confirmacion de regreso a show-->
<div class="ui small modal" id="regresar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Descartar registro
  </div>
  <div class=" content">
      <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
  </div>
  <div class="actions">

    <div class="ui negative cancel button">
      <i class="remove icon"></i>
      No
    </div>
    <div class="ui positive button" onclick=window.location.href="/localidad/{{$localidad->id}}">
      <i class="checkmark icon"></i>
      Si
    </div>
  </div>
</div>
<!--Final Modal de confirmacion de regreso a show-->

<!--Inicio Modal de confirmacion de envio de modificaciones-->
<div class="ui small modal" id="guardar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Guardar cambios
  </div>
  <div class="content">
      <p>¿Deseas guardar los cambios?</p>
  </div>
  <div class="actions">
      <div class="ui negative cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <button class="ui positive submit button" type="submit" form="editar">
        <i class="checkmark icon"></i>
        Si
      </button>
  </div>
</div>
<!--Final Modal de confirmacion de envio de modificaciones-->

<div class="ui grid container">
  <!--Inicio Forma de carga de datos y edicion-->
  <form id="editar" class="ui form" action="{{route('localidad.update', $localidad->id)}}" method="POST" novalidate>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="_method" value="PATCH">
    <!--Inicio de accordion-->
    <div class="ui styled fluid accordion">
      <!--Titulo primero-->
      <div class="title active">
        <i class="dropdown icon"></i>
        Datos generales
      </div>
      <!--Titulo primero-->

      <!--Primera seccion-->
      <div  class="ui grid container content active">
        <!-- Inicio del Primer renglón -->
        <div class="row">
          <!-- Input text para el campo "Nombre de la Localidad" -->
          <div class="six wide required column">
            <label class="prompt"><strong>Nombre de la Localidad:</strong></label>
            <input type="text" placeholder="Ej. El Chapulín..." name="nombre" value="{{$localidad->nombre}}">
          </div>
        </div>
        <!-- Final del Primer renglón -->

        <!-- Inicio del Segundo renglón -->
        <div class="row">
          <!-- Input text para el campo "Clave de Localidad" -->
          <div class="three wide required column">
            <label class="prompt"><strong>Clave de Localidad:</strong></label>
            <input type="text" placeholder="Ej. 076..." name="clave_localidad" value="{{$localidad->clave_localidad}}">
          </div>
          <!-- Input text para el campo "Clave Carta" -->
          <div class="three wide required column">
            <label class="prompt"><strong>Clave Carta:</strong></label>
            <input type="text" placeholder="Ej. F36C98..." name="clave_carta" value="{{$localidad->clave_carta}}">
          </div>
        </div>
        <!-- Final del Segundo renglón -->

        <!-- Inicio del Tercer renglón -->
        <div class="row">
          <!-- Select para el campo "Municipio" -->
          <div class="three wide required column">
            <label class="prompt">Municipio:</label>
            <select class="ui fluid dropdown" name="municipio_id">
              @foreach($municipios as $municipio)
                @if($localidad->municipio_id == $municipio->id)
                  <option value="{{$municipio->id}}" selected="selected">{{$municipio->nombre}}</option>
                @else
                  <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                @endif
              @endforeach
            </select>
          </div>

          <!-- Select para el campo "Grado de Marginación" -->
          <div class="four wide required column">
            <label class="prompt"><strong>Grado de Marginación:</strong></label>
            <select class="ui fluid dropdown" name="grado_de_marginacion_id">
              @foreach($grados_de_marginacion as $grado_de_marginacion)
                @if($localidad->grado_de_marginacion_id == $grado_de_marginacion->id)
                  <option value="{{$grado_de_marginacion->id}}">{{$grado_de_marginacion->nombre}}</option>
                @else
                  <option value="{{$grado_de_marginacion->id}}">{{$grado_de_marginacion->nombre}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <!-- Final del Tercer renglón -->

        <!-- Inicio del Cuarto renglón -->
        <div class="row">
          <!-- Input text para el campo "Latitud" -->
          <div class="two wide required column">
            <label class="prompt"><strong>Latitud:</strong></label>
            <input type="text" name="latitud" placeholder="Ej. 87..." value="{{$localidad->latitud}}">
          </div>

          <!-- Input text para el campo "Longitud" -->
          <div class="two wide required column">
            <label class="prompt"><strong>Longitud</strong></label>
            <input type="text" name="longitud" placeholder="Ej. 20..." value="{{$localidad->longitud}}">
          </div>

          <!-- Input text para el campo "Altitud" -->
          <div class="two wide required column">
            <label class="prompt"><strong>Altitud</strong></label>
            <input type="text" name="altitud" placeholder="Ej. 70..." value="{{$localidad->altitud}}">
          </div>
        </div>
        <!-- Final del Cuarto renglón -->
      </div>
      <!--Primera seccion-->
    </div>
    <!--Fin de accordion-->
  </form>
  <!--Final Forma de carga de datos y edicion-->
</div>
@stop
