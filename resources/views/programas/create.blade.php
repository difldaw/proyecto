@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class="  ui circular massive right floated teal save icon submit button"  type="submit" form="crear">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/programa/">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop


  @section('contenido')

  <script type="text/javascript">


  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.dropdown')
    .dropdown()
    ;
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form({
        inline : true,
          fields: {
            tipo: {
              identifier: 'tipo',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Tipo necesario'
                }
              ]
            }
          }
      });


  });


  </script>

        <div class="ui grid container">

          <div class="row">
            <div class="column">

              <form id="crear" class="ui form" action="{{ url('/programa') }}" method="POST" >
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>

                <div class="two fields">
                  <div class="eight wide required field">
                    <label class="prompt">Tipo de Programa</label>
                    <div class="ui left icon input">
                      <input type="text" placeholder="Tipo de programa" name="tipo">
                    </div>
                  </div>

                </br>



                    <div class="eight wide required field">
                      <label class="prompt">Grupos Vulnerables</label>
                      <select multiple="multiple" class="ui fluid dropdown" name="id_vulnerable[]">
                        <option value=""></option>
                        @foreach($vulnerables as $vulnerable)
                        <option value="{{$vulnerable->id_vulnerable}}">{{$vulnerable->nombre}}</option>
                        @endforeach
                      </select>
                    </div>

                    <!--    <input type="text" placeholder="Introducir informacion aqui" name="vulnerable">    -->
                  </div>


                </div>
              </div>
                  </br>
              </form>
          </div>

  @stop

  @section('titulo_seccion')
    Crear Programa Alimenticio
  @stop
