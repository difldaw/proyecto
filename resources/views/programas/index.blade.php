@extends('layouts.master')
@section('botones')
    <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/programa/create">
    <i class="plus icon"></i>
  </button>
  @stop

  @section('scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
        $("#todas").hide();
        $("#programas").show();
      });
    </script>
    <script>
      function eliminados(){
        if($('.ui.checkbox').checkbox('is checked')){
          $("#todas").show();
          $("#programas").hide();
        }else{
          $("#programas").show();
          $("#todas").hide();
        }
      }
    </script>
  @stop


  @section('contenido')

  <!--<div class="ui grid container">
    <div class="row">
      <div class="twelve wide column"></div>
      <div class="four wide column">
        <div class=" ui slider checkbox ">
          <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
          <label>Mostrar eliminados</label>
        </div>
      </div>
    </div>
  </div>
-->
<!-- Tabla todas -->

<table class="ui selectable sortable teal celled table" id="todas">
  <thead>
    <tr>
      <th class="four wide">Tipo de programa</th>
      <th class="one wide">Ver</th>
    </tr>
  </thead>
  <tbody>
      @foreach($todas as $toda)
        @if($toda->deleted_at!=NULL)
          <tr class="negative">
        @else
          <tr class="positive">
        @endif
          <td>{{$toda->tipo}}</td>
          <td onclick=window.location.href="/programa/{{$toda->id_programa}}" class=" selectable center aligned">
            <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th colspan="6">
        <div class="ui right floated pagination menu">
          <a href="{!! $todas->previousPageUrl() !!}" class="icon item">
            <i class="left chevron icon "></i>
          </a>
          @for ($pages = 1; $pages <= $programas->lastPage(); $pages++)
              <a href="{!! $todas->url($pages) !!}" class="item ">{{$pages}}</a>
          @endfor
          <a href="{!! $todas->nextPageUrl() !!}" class="icon item">
            <i class="right chevron icon "></i>
          </a>
        </div>
      </th>
    </tr>
  </tfoot>
</table>

<!-- Tabla Programas-->
      <table class="ui fixed sortable teal selectable celled table" id="programas">
        <thead>
          <tr>
            <th class="four wide">Tipo de programa</th>
            <th class="one wide">Ver</th>
          </tr>
        </thead>
        <tbody>
            @foreach($programas as $programa)
            <tr>
              <td>{{$programa->tipo}}</td>
              <td onclick=window.location.href="/programa/{{$programa->id_programa}}" class=" selectable center aligned">
                <i class="info circle big icon "></i>
              </td>
            </tr>
            @endforeach

        </tbody>
        <tfoot>
          <tr>
            <th colspan="6">
              <div class="ui right floated pagination menu">

                <a href="{!! $programas->previousPageUrl() !!}" class="icon item">
                  <i class="left chevron icon "></i>
                </a>
                @for ($pages = 1; $pages <= $programas->lastPage(); $pages++)
                    <a href="{!! $programas->url($pages) !!}"class="item ">{{$pages}}</a>
                @endfor

                <a href="{!! $programas->nextPageUrl() !!}"class="icon item">
                  <i class="right chevron icon "></i>
                </a>
              </div>
            </th>
          </tr>
        </tfoot>
      </table>

  @stop

  @section('titulo_seccion')
    Programas Alimenticios
    <!--<div class="fourteen wide field"></div>

    <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/programa/create">
    <i class="plus icon"></i>
  </button>-->
  @stop
