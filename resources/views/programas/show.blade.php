@extends('layouts.master')
@section('botones')
    <div class="row">
      <!--
      @if($programa->deleted_at==NULL)

      <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/programa/{{$programa->id_programa}}/edit">
      @else
      <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/programa/{{$programa->id_programa}}/edit">
      @endif
      <i class="write icon"></i>
      </button>
    -->
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">



      <script>
      function activarModal(){
        $('.small.modal').modal('show');
      }
      </script>


      </button>
      <!--
      @if($programa->deleted_at==NULL)
      <button class="  ui circular massive right floated teal trash outline icon button"  onclick="activarModal()" class="ui icon button" >
        <i class="trash outline icon"></i>
      @else
      <button class="  ui circular massive right floated teal undo icon button" onclick=window.location.href="/programa/reac/{{$programa->id_programa}}" >
      <i class=" undo icon"></i>
      @endif
    -->
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">

      <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/programa">
      <i class="chevron left icon"></i>
      </button>
    </div>
  @stop




  @section('contenido')


<form id="vista" action="{{route('programa.destroy',$programa->id_programa)}}" method="POST" >
  <input type="hidden" name="_method" value="DELETE">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <script type="text/javascript">

  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.accordion').accordion();
    $('.ui.form')
      .form();
  });

    $('.ui.modal')
    .modal('show')
    ;

  </script>

  <div class="ui small modal">
    <i class="close icon"></i>
    <div class="header">
      Borrar Programa Alimenticio
    </div>
    <div class="image content">
      <div class="image">
        <i class="trash icon"></i>
      </div>
      <div class="description">
        <h3>Esta seguro que quiere borrar el programa alimenticio?</h3>
      </div>
    </div>
    <div class="actions">
      <div class="two fluid ui buttons">
        <div class="ui red cancel button" >
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui green submit button" type="submit" form="vista" value="delete">
          <i class="checkmark icon"></i>
          Si
        </button>
      </div>
    </div>
  </div>

  <div class="ui styled fluid accordion ">
      <div class="title active ">
        <i class="dropdown icon"></i>
        Datos generales
      </div>
        <div class="row">
          <div class=" ui twelve wide column">
            <strong>Tipo de programa:</strong> {{$programa->tipo}}
          </div>
        </div>
      </div>


<!-- Tabla Vulnerables -->
<table class="ui fixed sortable teal selectable celled table" id="contratos">
  <thead>
    <tr>
      <th class="four wide">Grupo Vulnerable</th>
    </tr>
  </thead>
  <tbody>

      @foreach($programa->vulnerables as $vulnerable)<tr>
        <td>{{$vulnerable->nombre}}</td>
      </tr>@endforeach

  </tbody>
  <tfoot>

  </tfoot>
</table>

{{method_field('DELETE')}}
</form>
    @stop

    @section('titulo_seccion')
      Mostrar Programa Alimenticio
    @stop
