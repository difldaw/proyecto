@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class="  ui circular massive right floated teal save icon submit button"  type="submit" form="editar">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/programa/{{$programa->id_programa}}">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop

  @section('contenido')


  <script type="text/javascript">


  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form();

      $('.ui.dropdown')
      .dropdown()
      ;
      $('.ui.checkbox').checkbox();
      $('.ui.form')
        .form({
          inline : true,
            fields: {
              tipo: {
                identifier: 'tipo',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Tipo necesario'
                  }
                ]
              }
            }
        });
  });


  </script>
    <form id="editar" class="ui form" action="{{route('programa.update',$programa->id_programa)}}" method="POST">
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui grid container">

          <div class="row">
            <div class="column">

                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>

                </br>

                  <div class="fourteen wide required field">
                    <label class="prompt">Tipo de Programa</label>

                    <div class="ui selection dropdown">
                      <input type="hidden" value="{{$programa->tipo}}" placeholder="Tipo de programa" name="tipo">
                      <i class="dropdown icon"></i>
                      <div class="default text">Tipo de programa</div>
                      <div class="menu">
                        <div class="item" value="Despensa Unica (PAAFAMDES)">Despensa Unica (PAAFAMDES)</div>
                        <div class="item" value="Despensa Complementaria (Familias en desamparo)">Despensa Complementaria (Familias en desamparo)</div>
                        <div class="item" value="Desayuno Frio (PRODES)">Desayuno Frio (PRODES)</div>
                        <div class="item" value="Desayuno Caliente (PRODES)">Desayuno Caliente (PRODES)</div>
                        <div class="item" value="Prog. de Apoyo Integral a Ninos con Desnutricion">Prog. de Apoyo Integral a Ninos con Desnutricion</div>
                        <div class="item" value="Desayuno Caliente (Escuelas de Tiempo Completo)">Desayuno Caliente (Escuelas de Tiempo Completo)</div>
                        <div class="item" value="Otros">Otros</div>
                      </div>
                    </div>

                    <!--<input type="text" placeholder="Introducir informacion aqui" name="tipo">-->
                  </div>

                  <div class="fourteen wide required field">
                    <label class="prompt">Grupo Vulnerable</label>

                    <div class="ui selection dropdown">
                      <input type="hidden" value="{{$programa->vulnerable}}" placeholder="Grupo vulnerable" name="vulnerable">
                      <i class="dropdown icon"></i>
                      <div class="default text">Grupo Vulnerable</div>
                      <div class="menu">
                        <div class="item" value="Ninos 6 a 11 meses">Ninos 6 a 11 meses</div>
                        <div class="item" value="Ninos 1 a 4 anios">Ninos 1 a 4 anios</div>
                        <div class="item" value="Ninos 5 a 14 anios">Ninos 5 a 14 anios</div>
                        <div class="item" value="Mujeres Embarazadas">Mujeres Embarazadas</div>
                        <div class="item" value="Mujeres en P/ de Lactancia">Mujeres en P/ de Lactancia</div>
                        <div class="item" value="Ancianos">Ancianos</div>
                        <div class="item" value="Discapacitados">Discapacitados</div>
                        <div class="item" value="Adultos Mayores">Adultos Mayores</div>
                        <div class="item" value="Enfermos">Enfermos</div>
                        <div class="item" value="Ninos con Desnutricion">Ninos con Desnutricion</div>
                        <div class="item" value="Mujeres Emb y en P/ de Lactancia">Mujeres Emb y en P/ de Lactancia</div>
                      </div>
                    </div>

                    <!--    <input type="text" placeholder="Introducir informacion aqui" name="vulnerable">    -->
                  </div>


                </div>

                  </br>
          </div>
        </div>

      </div>
      {{method_field('PATCH')}}
    </form>
  @stop

  @section('titulo_seccion')
    Editar Programa Alimenticio
  @stop
