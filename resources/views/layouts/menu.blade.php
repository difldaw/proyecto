<?php $path = Route::getCurrentRoute()->getPath(); $usr = Auth::user();?>


<div class="three wide  column">
    <div class="ui vertical inverted pointing  left  fluid   menu">

        <div class="ui card fluid" >
          <div class=" image" >
            <img src="{{ asset('/img/logo.png')}}">
          </div>
          <div class="content">
            <a href="{{route('perfil',Auth::user()->id)}}"class="header">{{Auth::user()->nombre}}<br />  {{Auth::user()->apellido_p}} {{Auth::user()->apellido_m}}</a>
            <div class="meta">
              <span class="date">
                @if(Auth::user()->tipo_usuario==1)
                  Administrador
                @elseif(Auth::user()->tipo_usuario==2)
                  Usuario Central
                @elseif(Auth::user()->tipo_usuario==3)
                  Encargado(a) de {{Auth::user()->municipio->nombre}}
                @endif
              </span>
            </div>
          </div>
        </div>



      @if($path == "/")
        <a href="{{ url('/')}}" class="item active">
      @else
        <a href="{{ url('/')}}" class="item ">
      @endif
        <i class="home icon"></i> Inicio
      </a>

      @can('crud-usuarios')
        @if(str_contains($path,'usuario'))
          <a href="{{ url('usuario')}}" class="item active">
        @else
          <a href="{{ url('usuario')}}" class="item ">
        @endif
          <i class="user icon"></i> Usuarios
        </a>
      @endcan


      @if(str_contains($path,'beneficiario'))
        <a href="{{ url('beneficiario')}}" class="item active">
      @else
        <a href="{{ url('beneficiario')}}" class="item ">
      @endif
        <i class="child icon"></i> Beneficiarios
      </a>


      @if(str_contains($path,'localidad'))
        <a href="{{ url('localidad')}}" class="item active">
      @else
        <a href="{{ url('localidad')}}" class="item ">
      @endif
        <i class="marker icon"></i> Localidades
      </a>


      @if(str_contains($path,'provedor'))
        <a href="{{ url('provedor')}}" class="item active">
      @else
        <a href="{{ url('provedor')}}" class="item ">
      @endif
        <i class="shipping icon"></i> Provedores
      </a>


      @if(str_contains($path,'escuela'))
        <a href="{{ url('escuela')}}" class="item active">
      @else
        <a href="{{ url('escuela')}}" class="item ">
      @endif
        <i class="university icon"></i> Escuelas
      </a>



      @can('crud-productos')
        @if(str_contains($path,'producto'))
          <a href="{{ url('producto')}}" class="item active">
        @else
          <a href="{{ url('producto')}}" class="item ">
        @endif
          <i class="book icon"></i>  Inventario Productos
        </a>
      @endcan

      @if(str_contains($path,'programa'))
        <a href="{{ url('programa')}}" class="item active">
      @else
        <a href="{{ url('programa')}}" class="item ">
      @endif
        <i class="coffee icon"></i> Programas Alimenticios
      </a>



      @if(str_contains($path,'reporte'))
        <a href="{{ url('reporte')}}" class="item active">
      @else
        <a href="{{ url('reporte')}}" class="item ">
      @endif
        <i class="line chart icon"></i> Reportes
      </a>

      <a href="{{url('logout')}}" class="item">
        <i class="sign out icon"></i> Salir
      </a>




    </div>
  </div>
