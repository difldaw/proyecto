<!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SEDIF - Padrón Beneficiarios</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

      <link href="{{ asset('/css/semantic.min.css') }}" rel="stylesheet" />
      <link href="{{ asset('/components/icon.min.css') }}" rel="stylesheet" />
      <link href="{{ asset('/components/form.min.css') }}" rel="stylesheet" />
      <link href="{{ asset('/components/table.min.css') }}" rel="stylesheet" />

      <script src="{{ asset('/js/semantic.min.js') }}"></script>
      <script src="{{ asset('/components/form.js') }}"></script>
      <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    </style>
    @yield('scripts')
    @yield('css')
    </head>

    <body>
      <div class="ui grid">

          @include('layouts.menu')

        <div class="eleven wide streched  column" style="padding-top:2em;">
          
              <h1>@yield('titulo_seccion')</h1>

            @yield('contenido')

        </div>


        <div class="column" style="padding-top:5em;">
          @yield('botones')

        </div>

      </div>
    </body>

  </html>
