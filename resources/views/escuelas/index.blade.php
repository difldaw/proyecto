@extends('layouts.master')
  @section('botones')

    @can('crearEscuela')
      <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/escuela/create">
      <i class="plus icon"></i>
      </button>
    @endcan
  @stop

  @section('titulo_seccion')
    Escuelas
  @stop

  @section('scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
        $("#todas").hide();
        $("#escuelas").show();
      });
    </script>
    <script>
      function eliminados(){
        if($('.ui.checkbox').checkbox('is checked')){
          $("#todas").show();
          $("#escuelas").hide();
        }else{
          $("#escuelas").show();
          $("#todas").hide();
        }
      }
    </script>
  @stop

  @section('contenido')
    <!--Inicio de container independiente de tablas-->
    <div class="ui grid container">
      <div class="row">
        <div class="twelve wide column"></div>
        <div class="four wide column">
          <div class=" ui slider checkbox ">
            <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
            <label>Mostrar eliminadas</label>
          </div>
        </div>
      </div>
    </div>
    <!--fin de container independiente de tabla-->
    <!--Inicio de tabla sin filtrado de eliminados-->
    <table class="ui selectable sortable teal celled table" id="todas">
      <thead>
        <tr>
          <th class="four wide">Nombre</th>
          <th class="two wide">Clave</th>
          <th class="two wide">Teléfono</th>
          <th class="two wide">Municipio</th>
          <th class="two wide">Localidad</th>
          <th class="one wide">Ver</th>

        </tr>
      </thead>
      <tbody>
          @foreach($todas as $toda)
            @if($toda->deleted_at!=NULL)
              <tr class="negative">
            @else
              <tr class="positive">
            @endif
              <td>
                {{$toda->nombre}}
              </td>
              <td>{{$toda->clave}}</td>
              <td>{{$toda->telefono}}</td>
              <?php
              //Obtiene resultado de funcion en controller de municipio
               $municipios= app('App\Http\Controllers\EscuelaController')->getMunicipio($toda->municipio_id);
               $localidades= app('App\Http\Controllers\EscuelaController')->getLocalidad($toda->localidad_id);
               ?>
               @foreach ($municipios as $municipio)
                <td>{{$municipio->nombre}}</td>
               @endforeach
               @foreach ($localidades as $localidad)
                <td>{{$localidad->nombre}}</td>
               @endforeach
               <td onclick=window.location.href="/escuela/{{$toda->id_escuela}}" class=" selectable center aligned">
                  <i class="info circle big icon "></i>
               </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th colspan="6">
            <div class="ui right floated pagination menu">
              <a href="{!! $todas->previousPageUrl() !!}" class="icon item">
                <i class="left chevron icon "></i>
              </a>
              @for ($pages = 1; $pages <= $escuelas->lastPage(); $pages++)
                  <a href="{!! $todas->url($pages) !!}" class="item ">{{$pages}}</a>
              @endfor
              <a href="{!! $todas->nextPageUrl() !!}" class="icon item">
                <i class="right chevron icon "></i>
              </a>
            </div>
          </th>
        </tr>
      </tfoot>
    </table>
    <!--Inicio de tabla con filtrado de eliminados-->
    <table class="ui fixed selectable sortable teal celled table" id="escuelas">
      <thead>
        <tr>
          <th class="four wide">Nombre</th>
          <th class="two wide">Clave</th>
          <th class="two wide">Teléfono</th>
          <th class="two wide">Municipio</th>
          <th class="two wide">Localidad</th>
          <th class="one wide">Ver</th>
        </tr>
      </thead>
      <tbody>
          @foreach($escuelas as $escuela)
            <tr>
              <td>
                {{$escuela->nombre}}
              </td>
              <td>{{$escuela->clave}}</td>
              <td>{{$escuela->telefono}}</td>
              <?php
              //Obtiene resultado de funcion en controller de municipio
               $municipios= app('App\Http\Controllers\EscuelaController')->getMunicipio($escuela->municipio_id);
               $localidades= app('App\Http\Controllers\EscuelaController')->getLocalidad($escuela->localidad_id);
              ?>
               @foreach ($municipios as $municipio)
                <td>{{$municipio->nombre}}</td>
               @endforeach
               @foreach ($localidades as $localidad)
                <td>{{$localidad->nombre}}</td>
               @endforeach
              <td onclick=window.location.href="/escuela/{{$escuela->id_escuela}}" class=" selectable center aligned">
                  <i class="info circle big icon "></i>
              </td>
            </tr>
          @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th colspan="6">
            <div class="ui right floated pagination menu">
              <a href="{!! $escuelas->previousPageUrl() !!}" class="icon item">
                <i class="left chevron icon "></i>
              </a>
              @for ($pages = 1; $pages <= $escuelas->lastPage(); $pages++)
                  <a href="{!! $escuelas->url($pages) !!}" class="item ">{{$pages}}</a>
              @endfor
              <a href="{!! $escuelas->nextPageUrl() !!}" class="icon item">
                <i class="right chevron icon "></i>
              </a>
            </div>
          </th>
        </tr>
      </tfoot>
    </table>
  @stop
