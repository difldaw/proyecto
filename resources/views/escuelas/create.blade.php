@extends('layouts.master')
  @section('botones')
    <div class="row">
      <button class="  ui circular massive right floated teal save icon submit button" onclick="envio()">
      <i class="save icon"></i>
      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">
      <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
      <i class="chevron left icon"></i>
      </button>
    </div>
  @stop

  @section('titulo_seccion')
    Escuelas
  @stop

  @section('scripts')
    <script type="text/javascript">
      function adaptarGrados(sel) {
        if(sel.value==3){
          deshabilitaGrupos();

          $("#a1").removeClass("disabled");
          $("#a2").removeClass("disabled");
          $("#a3").removeClass("disabled");
          $("#g1").removeClass("disabled");
          $("#g2").removeClass("disabled");
          $("#g3").removeClass("disabled");
          $("#d1").removeClass("disabled");
          $("#d2").removeClass("disabled");
          $("#d3").removeClass("disabled");
          $("#a4").removeClass("disabled");
          $("#a5").removeClass("disabled");
          $("#a6").removeClass("disabled");
          $("#g4").removeClass("disabled");
          $("#g5").removeClass("disabled");
          $("#g6").removeClass("disabled");
          $("#d4").removeClass("disabled");
          $("#d5").removeClass("disabled");
          $("#d6").removeClass("disabled");
          deshabilitacendi();
        }else if (sel.value==2 || sel.value==4){
          deshabilitaGrupos();

          $("#a1").removeClass("disabled");
          $("#a2").removeClass("disabled");
          $("#a3").removeClass("disabled");
          $("#g1").removeClass("disabled");
          $("#g2").removeClass("disabled");
          $("#g3").removeClass("disabled");
          $("#d1").removeClass("disabled");
          $("#d2").removeClass("disabled");
          $("#d3").removeClass("disabled");
          deshabilitacendi();
        }else if (sel.value==1){
          deshabilitaGrupos();
          $("#ac1").removeClass("disabled");
          $("#ac2").removeClass("disabled");
          $("#gc1").removeClass("disabled");
          $("#gc2").removeClass("disabled");
          $("#dc1").removeClass("disabled");
          $("#dc2").removeClass("disabled");
        }
      }
      function envio(){
        $('.ui.form').form('validate form');
        if ($('.ui.form').form('is valid')){
          $('#guardar').modal('show');
        }

      }
      function deshabilitaGrupos(){
        $("#a1").addClass("disabled");
        $("#a2").addClass("disabled");
        $("#a3").addClass("disabled");
        $("#g1").addClass("disabled");
        $("#g2").addClass("disabled");
        $("#g3").addClass("disabled");
        $("#d1").addClass("disabled");
        $("#d2").addClass("disabled");
        $("#d3").addClass("disabled");
        $("#a4").addClass("disabled");
        $("#a5").addClass("disabled");
        $("#a6").addClass("disabled");
        $("#g4").addClass("disabled");
        $("#g5").addClass("disabled");
        $("#g6").addClass("disabled");
        $("#d4").addClass("disabled");
        $("#d5").addClass("disabled");
        $("#d6").addClass("disabled");

      }
      function deshabilitacendi(){
        $("#ac1").addClass("disabled");
        $("#ac2").addClass("disabled");
        $("#gc1").addClass("disabled");
        $("#gc2").addClass("disabled");
        $("#dc1").addClass("disabled");
        $("#dc2").addClass("disabled");
      }
    </script>
    <script type="text/javascript">

      $( document ).ready(function() {
        deshabilitaGrupos();
        deshabilitacendi();
        $('.ui.modal').modal();
        $('.menu .item').tab();
        $('.ui.checkbox').checkbox();
        $('select').dropdown();

        $('.ui.form')
          .form({
            inline : true,
            fields: {
              nombre: {
                identifier: 'nombre',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }
                ]
              },
              clave: {
                identifier: 'clave',
                rules: [
                  {
                    type   : 'exactLength[10]',
                    prompt : '10 carácteres'
                  }
                ]
              },
              domicilio: {
                identifier: 'domicilio',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Domicilio necesaria'
                  }
                ]
              },
              codigoPostal: {
                identifier: 'codigoPostal',
                rules: [
                  {
                    type   : 'exactLength[5]',
                    prompt : '5 carácteres'
                  }
                ]
              },
              colonia: {
                identifier: 'colonia',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Colonia necesaria'
                  }
                ]
              },
              latitud: {
                identifier: 'latitud',
                rules: [
                  {
                    type   : 'regExp[/^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$/]',
                    prompt : 'Longitud incorrecta'
                  }
                ]
              },
              longitud: {
                identifier: 'longitud',
                rules: [
                  {
                    type   : 'regExp[/^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$/]',
                    prompt : 'Longitud incorrecta'
                  }
                ]
              },
              telefono: {
                identifier: 'telefono',
                rules: [
                  {
                    type   : 'exactLength[10]',
                    prompt : 'Longitud incorrecta'
                  }
                ]
              },

              localidad_id: {
                identifier: 'localidad_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Localidad'
                  }
                ]
              },

              municipio_id: {
                identifier: 'municipio_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Municipio'
                  }
                ]
              },
              turno_id: {
                identifier: 'turno_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Turno'
                  }
                ]
              },
              zona_id: {
                identifier: 'zona_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Zona'
                  }
                ]
              },
              sector_id: {
                identifier: 'sector_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Sector'
                  }
                ]
              },
              gradoEscolar_id: {
                identifier: 'gradoEscolar_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Grado Escolar'
                  }
                ]
              },
              director: {
                identifier: 'director',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Director necesario'
                  }
                ]
              },
              sostenimiento_id: {
                identifier: 'sostenimiento_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona sostenimiento'
                  }
                ]
              },
              servicio_id: {
                identifier: 'servicio_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona servicio'
                  }
                ]
              },


            }

          });
      });

    </script>
  @stop

  @section('contenido')
    <!--Modal de confirmacion de regresar a index-->
    <div class="ui small modal" id="regresar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Descartar registro
      </div>
      <div class=" content">
          <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
      </div>
      <div class="actions">
        <div class="ui negative   button">
          <i class="remove icon"></i>
          No
        </div>
        <div class="ui positive  button" onclick=window.location.href="/escuela">
          <i class="checkmark icon"></i>
          Yes
        </div>
      </div>
    </div>
    <!--Modal de confirmacion de envio de formulario-->
    <div class="ui small modal" id="guardar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Guardar cambios
      </div>
      <div class="content">
          <p>¿Deseas guardar el registro de escuela?</p>
      </div>
      <div class="actions">
          <div class="ui red cancel button">
            <i class="remove icon"></i>
            No
          </div>
          <button class="ui green ok submit  button"  type="submit" form="registro">
            <i class="checkmark icon"></i>
            Si
          </button>
      </div>
    </div>

    <!--Contenedor de forma de registro-->
    <div class="ui grid container">

      <!--Row unico-->
      <div class="row">
        <!--Columna unica-->
        <div class="column">
          <!--Inicio de forma de registro-->
          <form id="registro" class="ui form" action="{{route('escuela.store')}}" method="POST" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h4 class="ui horizontal divider header">
              <i class="tag icon">
              </i>
              Datos generales
            </h4>
            <!--Seccion de datos generales-->
            <div class="two fields">
              <div class="fourteen wide required field">
                <label class="prompt" class="prompt">Nombre</label>
                <div class="ui left icon input">
                  <input type="text" placeholder="Esc. Prim..." name="nombre">
                  <i class="university icon"></i>
                </div>
              </div>
              <div class="two wide required field">
                <label class="prompt">Clave</label>
                <input type="text" placeholder="Esc. Prim..." name="clave">
              </div>
            </div>
            <div class="fields">
                <div class="six wide required field">
                  <label class="prompt">Domicilio</label>
                  <input type="text" name="domicilio" placeholder="Domicilio">
                </div>
                <div class="four wide required field">
                  <label class="prompt">Colonia</label>
                  <input type="text" name="colonia" placeholder="Colonia">
                </div>
                <div class="two wide required field">
                  <label class="prompt">Codigo Postal</label>
                  <input type="text" name="codigoPostal" placeholder="Codigo Postal">
                </div>
                <div class="two wide right floated required field">
                  <label class="prompt">Lat.</label>
                  <input type="text" name="latitud" placeholder="xxx">
                </div>
                <div class="two wide right floated required field">
                  <label class="prompt">Lo.</label>
                  <input type="text" placeholder="xxx" name="longitud">
                </div>
            </div>
            <div class=" three fields ">
              <div class="four wide required field">
                <label class="prompt">Teléfono</label>
                <div class="ui left icon input">
                  <input type="text" placeholder="Teléfono" name="telefono">
                  <i class="phone icon"></i>
                </div>
              </div>
              <div class="six wide required field">
                <label class="prompt">Delegación</label>
                <input type="text" placeholder="Delegación" name="delegacion">
              </div>
              <div class="six wide required field">
                <?php $municipios= app('App\Http\Controllers\EscuelaController')->getMunicipios(); ?>
                <label class="prompt">Municipio</label>
                <select class="ui fluid dropdown" name="municipio_id">
                  <option value=""></option>
                  @foreach ($municipios as $municipio)
                    <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="equal width five fields ">
              <div class=" required field">
                <label class="prompt">Localidad</label>
                <?php $localidades= app('App\Http\Controllers\EscuelaController')->getLocalidades(); ?>
                <select class="ui dropdown" name="localidad_id">
                  <option value=""></option>
                  @foreach ($localidades as $localidad)
                    <option value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="  required field">
                <label class="prompt">Turno</label>
                <?php $turnos= app('App\Http\Controllers\EscuelaController')->getTurnos(); ?>
                <select class="ui fluid dropdown" name="turno_id">
                  <option value=""></option>
                  @foreach ($turnos as $turno)
                    <option value="{{$turno->id_turno}}">{{$turno->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class=" required field">
                <label class="prompt">Zona</label>
                <?php $zonas= app('App\Http\Controllers\EscuelaController')->getZonas(); ?>
                <select class="ui fluid dropdown" name="zona_id">
                  <option value=""></option>
                  @foreach ($zonas as $zona)
                    <option value="{{$zona->id_zona}}">{{$zona->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="  required field">
                <label class="prompt">Sector</label>
                <?php $sectores= app('App\Http\Controllers\EscuelaController')->getSectores(); ?>
                <select class="ui fluid dropdown" name="sector_id">
                  <option value=""></option>
                  @foreach ($sectores as $sector)
                    <option value="{{$sector->id_sector}}">{{$sector->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="  required field" >
                <label class="prompt">Grado Escolar</label>
                <?php $grados= app('App\Http\Controllers\EscuelaController')->getGrados(); ?>
                <select class="ui fluid dropdown" name="gradoEscolar_id" onchange="adaptarGrados(this)">
                  <option value=""></option>
                  @foreach ($grados as $grado)
                    <option value="{{$grado->id_gradoEscolar}}">{{$grado->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class=" three fields ">
              <div class="eight wide required field">
                <label class="prompt">Director</label>
                <div class="ui left icon input">
                  <input type="text" placeholder="Director" name="director">
                  <i class="user icon"></i>
                </div>
              </div>
              <div class="four wide required field">
                <label class="prompt">Sostenimiento</label>
                <?php $sostenimientos= app('App\Http\Controllers\EscuelaController')->getSostenimientos(); ?>
                <select class="ui fluid dropdown" name="sostenimiento_id">
                  <option value=""></option>
                  @foreach ($sostenimientos as $sostenimiento)
                    <option value="{{$sostenimiento->id_sostenimiento}}">{{$sostenimiento->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="four wide required field">
                <label class="prompt">Servicio</label>
                <?php $servicios= app('App\Http\Controllers\EscuelaController')->getServicios(); ?>
                <select class="ui fluid dropdown" name="servicio_id">
                  <option value=""></option>
                  @foreach ($servicios as $servicio)
                    <option value="{{$servicio->id_servicio}}">{{$servicio->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!--Seccion de datos mas especificos-->
            <h4 class="ui horizontal divider header">
              <i class="bar chart icon"></i>
              Especificaciones
            </h4>
            <!--Menu de segmentos-->
            <div class="ui top secondary pointing four item menu">
              <a class="item active" data-tab="first">ALUMNOS</a>
              <a class="item" data-tab="second">GRUPOS</a>
              <a class="item" data-tab="third">DOCENTES</a>
              <a class="item" data-tab="fourth">DIRECTOR, PERSONAL, AULAS</a>
            </div>
            <!--Inicia manejo de segmentos-->
            <!--Inicio de Primer segmento-->
            <div class="ui bottom attached tab segment active "data-tab="first">
              </br>

              <div class="ui vertical segments" >
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Sexo
                  </h3>
                  <div class="fields">
                    <div class="  six wide field">
                    </div>
                    <div class="two wide required field">
                      <label class="prompt">Niñas</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="alumnosNinas">
                        <i class="female icon"></i>
                      </div>
                    </div>
                    <div class="two wide required field">
                      <label class="prompt">Niños</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="alumnosNinos">
                        <i class="male icon"></i>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">Grado</h3>
                  <div class="fields">
                    <div class="  two wide required field">
                    </div>
                    <div class="  two wide required field" id="a1">
                      <label class="prompt">Primero</label>
                      <input type="number"  min="1" name="alumnosPrimero">
                    </div>
                    <div class=" two  wide required field" id="a2">
                      <label class="prompt">Segundo</label>
                      <input type="number"  min="1" name="alumnosSegundo">
                    </div>
                    <div class=" two  wide required field" id="a3">
                      <label class="prompt">Tercero</label>
                      <input type="number"  min="1" name="alumnosTercero">
                    </div>
                    <div class=" two  wide required field " id="a4">
                      <label class="prompt">Cuarto</label>
                      <input type="number"  min="1" name="alumnosCuarto">
                    </div>
                    <div class=" two  wide required field " id="a5">
                      <label class="prompt">Quinto</label>
                      <input type="number"  min="1" name="alumnosQuinto">
                    </div>
                    <div class=" two  wide required field " id="a6">
                      <label class="prompt">Sexto</label>
                      <input type="number"  min="1" name="alumnosSexto">
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Cendi
                  </h3>
                  <div class="centered fields" >
                    <div class="  six wide required field">
                    </div>
                    <div class="centered two wide required field" id="ac1">
                      <label class="prompt">Lactantes</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="alumnosLactantes">
                        <i class=""></i>
                      </div>
                    </div>
                    <div class=" two wide required field" id="ac2">
                      <label class="prompt">Maternal</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="alumnosMaternal">
                        <i class=""></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </br>
            </div>
            <!--Fin de primer segmento-->
            <!--Inicio de segundo segmento-->
            <div class="ui bottom attached tab segment" data-tab="second">
              </br>
              <div class="ui vertical segments">
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">Grado</h3>
                  <div class="fields">
                    <div class="  two wide required field">
                    </div>
                    <div class="two wide required field" id="g1">
                      <label class="prompt">Primero</label>
                      <input type="number"  min="1" name="gruposPrimero">
                    </div>
                    <div class=" two  wide required field" id="g2">
                      <label class="prompt">Segundo</label>
                      <input type="number"  min="1" name="gruposSegundo">
                    </div>
                    <div class=" two  wide required field" id="g3">
                      <label class="prompt">Tercero</label>
                      <input type="number"  min="1" name="gruposTercero">
                    </div>
                    <div class=" two  wide required field" id="g4">
                      <label class="prompt">Cuarto</label>
                      <input type="number"  min="1" name="gruposCuarto" >
                    </div>
                    <div class=" two  wide required field" id="g5">
                      <label class="prompt">Quinto</label>
                      <input type="number"  min="1" name="gruposQuinto">
                    </div>
                    <div class=" two  wide required field" id="g6">
                      <label class="prompt">Sexto</label>
                      <input type="number"  min="1" name="gruposSexto">
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Cendi
                  </h3>
                  <div class="centered fields" >
                    <div class="  six wide required field">
                    </div>
                    <div class="centered two wide required field" id="gc1">
                      <label class="prompt">Lactantes</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="gruposLactantes">
                        <i class=""></i>
                      </div>
                    </div>
                    <div class=" two wide required field" id="gc2">
                      <label class="prompt">Maternal</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="gruposMaternal">
                        <i class=""></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </br>
            </div>
            <!--Fin de segundo segmento-->
            <!--Inicio de tercer segmento-->
            <div class="ui bottom attached tab segment" data-tab="third">
              </br>
              <div class="ui vertical segments">
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Sexo
                  </h3>
                  <div class="fields">
                    <div class="  six wide required field">
                    </div>
                    <div class="two wide required field">
                      <label class="prompt">Mujeres</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="docentesMujeres">
                        <i class="female icon"></i>
                      </div>
                    </div>
                    <div class="two wide required field">
                      <label class="prompt">Hombres</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="docentesHombres" >
                        <i class="male icon"></i>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">Grado</h3>
                  <div class="fields">
                    <div class="  two wide required field">
                    </div>
                    <div class="  two wide required field" id="d1">
                      <label class="prompt">Primero</label>
                      <input type="number"  min="1" name="docentesPrimero" >
                    </div>
                    <div class=" two  wide required field" id="d2">
                      <label class="prompt">Segundo</label>
                      <input type="number"  min="1"  name="docentesSegundo" >
                    </div>
                    <div class=" two  wide required field" id="d3">
                      <label class="prompt">Tercero</label>
                      <input type="number"  min="1" name="docentesTercero" >
                    </div>
                    <div class=" two  wide required field" id="d4">
                      <label class="prompt">Cuarto</label>
                      <input type="number"  min="1" name="docentesCuarto" >
                    </div>
                    <div class=" two  wide required field" id="d5">
                      <label class="prompt">Quinto</label>
                      <input type="number"  min="1" name="docentesQuinto" >
                    </div>
                    <div class=" two  wide required field" id="d6">
                      <label class="prompt">Sexto</label>
                      <input type="number"  min="1" name="docentesSexto" >
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Cendi
                  </h3>
                  <div class="centered fields" >
                    <div class="  six wide required field">
                    </div>
                    <div class="centered two wide required field" id="dc1">
                      <label class="prompt">Lac</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="docentesLactantes">
                        <i class=""></i>
                      </div>
                    </div>
                    <div class=" two wide required field" id="dc2">
                      <label class="prompt">Mat</label>
                      <div class="ui left icon input">
                        <input type="number" min="1" name="docentesMaternal" >
                        <i class=""></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </br>
            </div>
            <!--Fin de tercer segmento-->
            <!--Inicio de cuarto segmento-->
            <div class="ui bottom attached tab segment" data-tab="fourth">
              </br>
              <div class="ui vertical segments">
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Director
                  </h3>
                  <br />
                  <div class="fields">
                    <div class="  seven wide required field">
                    </div>
                    <div class="two wide required inline field">
                      <div class="ui toggle checkbox">
                        <input type="checkbox" tabindex="0" class="hidden" value="1" name="directorCongrupo">
                        <label >con grupo</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">Personal</h3>
                  <div class="fields">
                    <div class="  seven wide required field">
                    </div>
                    <div class="  two wide required field">
                      <label class="prompt">Total</label>
                      <input type="number"  min="1" name="personal">
                    </div>
                  </div>
                </div>
                <div class="ui center aligned segment" style="background-color:#F0F0F0;">
                  <h3 class="ui header">
                    Aulas
                  </h3>
                  <div class="centered fields">
                    <div class="  seven wide field">
                    </div>
                    <div class=" two wide required field">
                      <label class="prompt">Total</label>
                      <input type="number" min="1" name="aulas">
                      <i class=""></i>
                    </div>

                  </div>
                </div>
              </div>
              </br>
            </div>
            <!--Fin de cuarto segmento-->
          </form>
          <!--Fin de forma-->
        </div>
      </div>
    </div>
  @stop
