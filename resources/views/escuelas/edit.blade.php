@extends('layouts.master')

  @section('botones')
    <div class="row">
      <button class="ui circular massive right floated teal save icon submit button" onclick="envio()" >
      <i class="save icon"></i>
      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">
      <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
      <i class="chevron left icon"></i>
      </button>
    </div>
  @stop

  @section('titulo_seccion')
    Escuela {{$escuela->nombre}}
  @stop

  @section('scripts')
    <script type="text/javascript">
      //funcion de validacion de forma y activacion de modal
      function envio(){
        $('.ui.form').form('validate form');
        if ($('.ui.form').form('is valid')){
          $('#guardar').modal('show');
        }

      }
      //funcion de carga de mapa
      function init_map(){
        var myOptions = {
        zoom:17,
        center:new google.maps.LatLng({{$escuela->latitud}},{{$escuela->longitud}}),
        mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({
          map: map,position: new google.maps.LatLng({{$escuela->latitud}},{{$escuela->longitud}})});
          infowindow = new google.maps.InfoWindow({
            content:'<strong>{{$escuela->nombre}}</strong><br/>{{$escuela->domicilio}} {{$escuela->codigoPostal}}'});
            google.maps.event.addListener(marker, 'click', function(){
              infowindow.open(map,marker);});infowindow.open(map,marker);

      }google.maps.event.addDomListener(window, 'load', init_map);

      function adaptarGrados(sel) {
        if(sel.value==3){
          deshabilitaGrupos();
          $("#a1").removeClass("disabled");
          $("#a2").removeClass("disabled");
          $("#a3").removeClass("disabled");
          $("#g1").removeClass("disabled");
          $("#g2").removeClass("disabled");
          $("#g3").removeClass("disabled");
          $("#d1").removeClass("disabled");
          $("#d2").removeClass("disabled");
          $("#d3").removeClass("disabled");
          $("#a4").removeClass("disabled");
          $("#a5").removeClass("disabled");
          $("#a6").removeClass("disabled");
          $("#g4").removeClass("disabled");
          $("#g5").removeClass("disabled");
          $("#g6").removeClass("disabled");
          $("#d4").removeClass("disabled");
          $("#d5").removeClass("disabled");
          $("#d6").removeClass("disabled");
          deshabilitacendi();
        }else if (sel.value==2 || sel.value==4){
          deshabilitaGrupos();
          $("#a1").removeClass("disabled");
          $("#a2").removeClass("disabled");
          $("#a3").removeClass("disabled");
          $("#g1").removeClass("disabled");
          $("#g2").removeClass("disabled");
          $("#g3").removeClass("disabled");
          $("#d1").removeClass("disabled");
          $("#d2").removeClass("disabled");
          $("#d3").removeClass("disabled");
          deshabilitacendi();
        }else if (sel.value==1){
          deshabilitaGrupos();
          $("#ac1").removeClass("disabled");
          $("#ac2").removeClass("disabled");
          $("#gc1").removeClass("disabled");
          $("#gc2").removeClass("disabled");
          $("#dc1").removeClass("disabled");
          $("#dc2").removeClass("disabled");
        }
      }
      function deshabilitaGrupos(){
        $("#a1").addClass("disabled");
        $("#a2").addClass("disabled");
        $("#a3").addClass("disabled");
        $("#g1").addClass("disabled");
        $("#g2").addClass("disabled");
        $("#g3").addClass("disabled");
        $("#d1").addClass("disabled");
        $("#d2").addClass("disabled");
        $("#d3").addClass("disabled");
        $("#a4").addClass("disabled");
        $("#a5").addClass("disabled");
        $("#a6").addClass("disabled");
        $("#g4").addClass("disabled");
        $("#g5").addClass("disabled");
        $("#g6").addClass("disabled");
        $("#d4").addClass("disabled");
        $("#d5").addClass("disabled");
        $("#d6").addClass("disabled");

      }
      function deshabilitacendi(){
        $("#ac1").addClass("disabled");
        $("#ac2").addClass("disabled");
        $("#gc1").addClass("disabled");
        $("#gc2").addClass("disabled");
        $("#dc1").addClass("disabled");
        $("#dc2").addClass("disabled");
      }
    </script>
    <script type="text/javascript">
      $( document ).ready(function() {
        $('.ui.modal').modal();
        $('.ui.accordion').accordion();
        $('.menu .item').tab();
        $('.ui.checkbox').checkbox();
        $('select').dropdown();
        $('.ui.form')
          .form({
            inline : true,
            fields: {
              nombre: {
                identifier: 'nombre',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }
                ]
              },
              clave: {
                identifier: 'clave',
                rules: [
                  {
                    type   : 'exactLength[10]',
                    prompt : '10 carácteres'
                  }
                ]
              },
              domicilio: {
                identifier: 'domicilio',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Domicilio necesaria'
                  }
                ]
              },
              codigoPostal: {
                identifier: 'codigoPostal',
                rules: [
                  {
                    type   : 'exactLength[5]',
                    prompt : '5 carácteres'
                  }
                ]
              },
              colonia: {
                identifier: 'colonia',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Colonia necesaria'
                  }
                ]
              },
              latitud: {
                identifier: 'latitud',
                rules: [
                  {
                    type   : 'regExp[/^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$/]',

                    prompt : 'Latitud incorrecta'
                  }
                ]
              },
              longitud: {
                identifier: 'longitud',
                rules: [
                  {
                    type   : 'regExp[/^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$/]',

                    prompt : 'Longitud incorrecta'
                  }
                ]
              },
              telefono: {
                identifier: 'telefono',
                rules: [
                  {
                    type   : 'exactLength[10]',
                    prompt : 'Longitud incorrecta'
                  }
                ]
              },

              localidad_id: {
                identifier: 'localidad_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Localidad'
                  }
                ]
              },

              municipio_id: {
                identifier: 'municipio_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Municipio'
                  }
                ]
              },
              turno_id: {
                identifier: 'turno_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Turno'
                  }
                ]
              },
              zona_id: {
                identifier: 'zona_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Zona'
                  }
                ]
              },
              sector_id: {
                identifier: 'sector_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Sector'
                  }
                ]
              },
              gradoEscolar_id: {
                identifier: 'gradoEscolar_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona Grado Escolar'
                  }
                ]
              },
              director: {
                identifier: 'director',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Director necesario'
                  }
                ]
              },
              sostenimiento_id: {
                identifier: 'sostenimiento_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona sostenimiento'
                  }
                ]
              },
              servicio_id: {
                identifier: 'servicio_id',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Selecciona servicio'
                  }
                ]
              },


            }

          });
      });
    </script>
  @stop

  @section('contenido')
    <!--Seccion de mensaje de informacion-->
    <div class="ui teal message"><i class="info circle icon"></i>Informacion de la escuela </div>
    <!--Seccion de mapa-->
    <div  class="row">
      <div id='gmap_canvas' style='height:300px;width:100%;'>
      </div>
    </div>
    <!--Modal de confirmacion de regreso a show-->
    <div class="ui small modal" id="regresar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Descartar registro
      </div>
      <div class=" content">
          <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
      </div>
      <div class="actions">

        <div class="ui red cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <div class="ui green ok button" onclick=window.location.href="/escuela/{{$escuela->id_escuela}}">
          <i class="checkmark icon"></i>
          Yes
        </div>
      </div>
    </div>
    <!--Modal de confirmacion de envio de modificaciones-->
    <div class="ui small modal" id="guardar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Guardar cambios
      </div>
      <div class="content">
          <p>¿Deseas guardar los cambios en escuela {{$escuela->nombre}}?</p>
      </div>
      <div class="actions">
          <div class="ui red cancel button">
            <i class="remove icon"></i>
            No
          </div>
          <button class="ui green ok submit button" type="submit" form="editar">
            <i class="checkmark icon"></i>
            Si
          </button>
      </div>
    </div>
    <!--Forma de carga dedatos y edicion-->
    <form id="editar" class="ui form" action="{{route('escuela.update',$escuela->id_escuela)}}" method="POST" novalidate>
      <!--Inicio de accordion-->
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="ui styled fluid accordion ">
        <!--Titulo primera seccion-->
        <div class="title active ">
          <i class="dropdown icon"></i>
          Datos generales
        </div>
        <!--Inicio de primera seccion-->
        <div class="ui grid container content active ">
          <div class=" two fields">
            <div class="fourteen wide required field">
              <label class="prompt" class="prompt">Nombre</label>
              <input type="text" placeholder="Esc. Prim..." name="nombre" value="{{$escuela->nombre}}">
            </div>
            <div class="two wide required field">
              <label class="prompt">Clave</label>
              <input type="text" placeholder="Esc. Prim..." name="clave" value="{{$escuela->clave}}">
            </div>
          </div>
          <div class="fields">
              <div class="six wide required field">
                <label class="prompt">Domicilio</label>
                <input type="text" name="domicilio" placeholder="Domicilio" value="{{$escuela->domicilio}}">
              </div>
              <div class="four wide required field">
                <label class="prompt">Colonia</label>
                <input type="text" name="colonia" placeholder="Colonia" value="{{$escuela->colonia}}">
              </div>
              <div class="two wide required field">
                <label class="prompt">Codigo Postal</label>
                <input type="text" name="codigoPostal" placeholder="Codigo Postal" value="{{$escuela->codigoPostal}}">
              </div>
              <div class="two wide right floated required field">
                <label class="prompt">Lat.</label>
                <input type="text" name="latitud" placeholder="xxx" value="{{$escuela->latitud}}">
              </div>
              <div class="two wide right floated required field">
                <label class="prompt">Lo.</label>
                <input type="text" placeholder="xxx" name="longitud" value="{{$escuela->longitud}}">
              </div>
          </div>
          <div class=" three fields ">
            <div class="six wide required field">
              <label class="prompt">Delegación</label>
              <input type="text" placeholder="Delegación" name="delegacion" value="{{$escuela->delegacion}}">
            </div>
            <div class="six wide required field">
              <?php $municipios= app('App\Http\Controllers\EscuelaController')->getMunicipios(); ?>
              <label class="prompt">Municipio</label>
              <select class="ui fluid dropdown" name="municipio_id">
                @foreach ($municipios as $municipio)
                  @if ($municipio->id==$escuela->municipio_id)
                  <option selected="selected" value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                  @else
                  <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class=" required field">
              <label class="prompt">Localidad</label>
              <?php $localidades= app('App\Http\Controllers\EscuelaController')->getLocalidades(); ?>
              <select class="ui dropdown" name="localidad_id">
                <option value=""></option>
                @foreach ($localidades as $localidad)
                  @if ($localidad->id==$escuela->localidad_id)
                  <option selected="selected "value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                  @else
                  <option value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="equal width five fields ">
            <div class="four wide required field">
              <label class="prompt">Teléfono</label>
              <div class="ui left icon input">
                <input type="text" placeholder="Teléfono" name="telefono" value="{{$escuela->telefono}}">
                <i class="phone icon"></i>
              </div>
            </div>
            <div class="  required field">
              <label class="prompt">Turno</label>
              <?php $turnos= app('App\Http\Controllers\EscuelaController')->getTurnos(); ?>
              <select class="ui fluid dropdown" name="turno_id">
                <option value=""></option>
                @foreach ($turnos as $turno)
                  @if ($turno->id_turno==$escuela->turno_id)
                  <option selected="selected" value="{{$turno->id_turno}}">{{$turno->nombre}}</option>
                  @else
                  <option value="{{$turno->id_turno}}">{{$turno->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class=" required field">
              <label class="prompt">Zona</label>
              <?php $zonas= app('App\Http\Controllers\EscuelaController')->getZonas(); ?>
              <select class="ui fluid dropdown" name="zona_id">
                <option value=""></option>
                @foreach ($zonas as $zona)
                  @if ($zona->id_zona==$escuela->zona_id)
                  <option selected="selected" value="{{$zona->id_zona}}">{{$zona->nombre}}</option>
                  @else
                  <option value="{{$zona->id_zona}}">{{$zona->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="  required field">
              <label class="prompt">Sector</label>
              <?php $sectores= app('App\Http\Controllers\EscuelaController')->getSectores(); ?>
              <select class="ui fluid dropdown" name="sector_id">
                <option value=""></option>
                @foreach ($sectores as $sector)
                  @if ($sector->id_sector==$escuela->sector_id)
                  <option selected="selected" value="{{$sector->id_sector}}">{{$sector->nombre}}</option>
                  @else
                  <option value="{{$sector->id_sector}}">{{$sector->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="  required field" >
              <label class="prompt">Grado Escolar</label>
              <?php $grados= app('App\Http\Controllers\EscuelaController')->getGrados(); ?>
              <select class="ui fluid dropdown" name="gradoEscolar_id" onchange="adaptarGrados(this)">
                <option value=""></option>
                @foreach ($grados as $grado)
                  @if ($grado->id_gradoEscolar==$escuela->gradoEscolar_id)
                  <option selected="selected" value="{{$grado->id_gradoEscolar}}">{{$grado->nombre}}</option>
                  @else
                  <option value="{{$grado->id_gradoEscolar}}">{{$grado->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class=" three fields ">
            <div class="eight wide required field">
              <label class="prompt">Director</label>
              <div class="ui left icon input">
                <input type="text" placeholder="Director" name="director" value="{{$escuela->director}}">
                <i class="user icon"></i>
              </div>
            </div>
            <div class="four wide required field">
              <label class="prompt">Servicio</label>
              <?php $servicios= app('App\Http\Controllers\EscuelaController')->getServicios(); ?>
              <select class="ui fluid dropdown" name="servicio_id">
                <option value=""></option>
                @foreach ($servicios as $servicio)
                  @if ($servicio->id_servicio==$escuela->servicio_id)
                  <option selected="selected" value="{{$servicio->id_servicio}}">{{$servicio->nombre}}</option>
                  @else
                  <option value="{{$servicio->id_servicio}}">{{$servicio->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="four wide required field">
              <label class="prompt">Sostenimiento</label>
              <?php $sostenimientos= app('App\Http\Controllers\EscuelaController')->getSostenimientos(); ?>
              <select class="ui fluid dropdown" name="sostenimiento_id">
                <option value=""></option>
                @foreach ($sostenimientos as $sostenimiento)
                  @if ($sostenimiento->id_sostenimiento==$escuela->sostenimiento_id)
                  <option selected="selected" value="{{$sostenimiento->id_sostenimiento}}">{{$sostenimiento->nombre}}</option>
                  @else
                  <option value="{{$sostenimiento->id_sostenimiento}}">{{$sostenimiento->nombre}}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <!--Titulo segunda seccion-->
        <div class="title">
          <i class="dropdown icon"></i>
          Alumnos
        </div>
        <!--Inicio de segunda seccion-->
        <div class="content ui container grid">
          <div class="fields">
            <div class="  six wide field">
            </div>
            <div class="two wide required field">
              <label class="prompt">Niñas</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="alumnosNinas" value="{{$escuela->alumnosNinas}}">
                <i class="female icon"></i>
              </div>
            </div>
            <div class="two wide required field">
              <label class="prompt">Niños</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="alumnosNinos" value="{{$escuela->alumnosNinos}}">
                <i class="male icon"></i>
              </div>
            </div>
          </div>
          <div class="fields">
            <div class="  two wide required field">
            </div>
            <div class="  two wide required field" id="a1">
              <label class="prompt">Primero</label>
              <input type="number"  min="1" name="alumnosPrimero" value="{{$escuela->alumnosPrimero}}">
            </div>
            <div class=" two  wide required field" id="a2">
              <label class="prompt">Segundo</label>
              <input type="number"  min="1" name="alumnosSegundo"value="{{$escuela->alumnosSegundo}}">
            </div>
            <div class=" two  wide required field" id="a3">
              <label class="prompt">Tercero</label>
              <input type="number"  min="1" name="alumnosTercero" value="{{$escuela->alumnosTercero}}">
            </div>
            <div class=" two  wide required field " id="a4">
              <label class="prompt">Cuarto</label>
              <input type="number"  min="1" name="alumnosCuarto" value="{{$escuela->alumnosCuarto}}">
            </div>
            <div class=" two  wide required field " id="a5">
              <label class="prompt">Quinto</label>
              <input type="number"  min="1" name="alumnosQuinto" value="{{$escuela->alumnosQuinto}}">
            </div>
            <div class=" two  wide required field " id="a6">
              <label class="prompt">Sexto</label>
              <input type="number"  min="1" name="alumnosSexto" value="{{$escuela->alumnosSexto}}">
            </div>
          </div>
          <div class="centered fields" >
            <div class="  six wide required field">
            </div>
            <div class="centered two wide required field" id="ac1">
              <label class="prompt">Lactantes</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="alumnosLactantes" value="{{$escuela->alumnosLactantes}}">
                <i class=""></i>
              </div>
            </div>
            <div class=" two wide required field" id="ac2">
              <label class="prompt">Maternal</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="alumnosMaternal" value="{{$escuela->alumnosMaternal}}">
                <i class=""></i>
              </div>
            </div>
          </div>
        </div>
        <!--Titulo tercera seccion-->
        <div class="title">
          <i class="dropdown icon"></i>
          Grupos
        </div>
        <!--Inicio de tercera seccion-->
        <div class="content ui container grid">
          <div class="fields">
            <div class="  two wide required field">
            </div>
            <div class="two wide required field" id="g1">
              <label class="prompt">Primero</label>
              <input type="number"  min="1" name="gruposPrimero" value="{{$escuela->gruposPrimero}}">
            </div>
            <div class=" two  wide required field" id="g2">
              <label class="prompt">Segundo</label>
              <input type="number"  min="1" name="gruposSegundo" value="{{$escuela->gruposSegundo}}">
            </div>
            <div class=" two  wide required field" id="g3">
              <label class="prompt">Tercero</label>
              <input type="number"  min="1" name="gruposTercero" value="{{$escuela->gruposTercero}}">
            </div>
            <div class=" two  wide required field" id="g4">
              <label class="prompt">Cuarto</label>
              <input type="number"  min="1" name="gruposCuarto" value="{{$escuela->gruposCuarto}}" >
            </div>
            <div class=" two  wide required field" id="g5">
              <label class="prompt">Quinto</label>
              <input type="number"  min="1" name="gruposQuinto" value="{{$escuela->gruposQuinto}}">
            </div>
            <div class=" two  wide required field" id="g6">
              <label class="prompt">Sexto</label>
              <input type="number"  min="1" name="gruposSexto" value="{{$escuela->gruposSexto}}">
            </div>
          </div>
          <div class="centered fields" >
            <div class="  six wide required field">
            </div>
            <div class="centered two wide required field" id="gc1">
              <label class="prompt">Lactantes</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="gruposLactantes" value="{{$escuela->gruposLactantes}}">
                <i class=""></i>
              </div>
            </div>
            <div class=" two wide required field" id="gc2">
              <label class="prompt">Maternal</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="gruposMaternal" value="{{$escuela->gruposMaternal}}">
                <i class=""></i>
              </div>
            </div>
          </div>
        </div>
        <!--Titulo cuarta seccion-->
        <div class="title">
          <i class="dropdown icon"></i>
          Docentes
        </div>
        <!--Inicio de cuarta seccion-->
        <div class="content ui container grid">
          <div class="fields">
            <div class="  six wide required field">
            </div>
            <div class="two wide required field">
              <label class="prompt">Mujeres</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="docentesMujeres" value="{{$escuela->docentesMujeres}}">
                <i class="female icon"></i>
              </div>
            </div>
            <div class="two wide required field">
              <label class="prompt">Hombres</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="docentesHombres" value="{{$escuela->docentesHombres}}">
                <i class="male icon"></i>
              </div>
            </div>
          </div>
          <div class="fields">
            <div class="  two wide required field">
            </div>
            <div class="  two wide required field" id="d1">
              <label class="prompt">Primero</label>
              <input type="number"  min="1" name="docentesPrimero" value="{{$escuela->docentesPrimero}}">
            </div>
            <div class=" two  wide required field" id="d2">
              <label class="prompt">Segundo</label>
              <input type="number"  min="1"  name="docentesSegundo" value="{{$escuela->docentesSegundo}}" >
            </div>
            <div class=" two  wide required field" id="d3">
              <label class="prompt">Tercero</label>
              <input type="number"  min="1" name="docentesTercero" value="{{$escuela->docentesTercero}}" >
            </div>
            <div class=" two  wide required field" id="d4">
              <label class="prompt">Cuarto</label>
              <input type="number"  min="1" name="docentesCuarto" value="{{$escuela->docentesCuarto}}">
            </div>
            <div class=" two  wide required field" id="d5">
              <label class="prompt">Quinto</label>
              <input type="number"  min="1" name="docentesQuinto" value="{{$escuela->docentesQuinto}}" >
            </div>
            <div class=" two  wide required field" id="d6">
              <label class="prompt">Sexto</label>
              <input type="number"  min="1" name="docentesSexto" value="{{$escuela->docentesSexto}}" >
            </div>
          </div>
          <div class="centered fields" >
            <div class="  six wide required field">
            </div>
            <div class="centered two wide required field" id="dc1">
              <label class="prompt">Lac</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="docentesLactantes" value="{{$escuela->docentesLactantes}}">
                <i class=""></i>
              </div>
            </div>
            <div class=" two wide required field" id="dc2">
              <label class="prompt">Mat</label>
              <div class="ui left icon input">
                <input type="number" min="1" name="docentesMaternal" value="{{$escuela->docentesMaternal}}" >
                <i class=""></i>
              </div>
            </div>
          </div>
        </div>
        <!--Titulo quinta seccion-->
        <div class="title">
          <i class="dropdown icon"></i>
          Director, Aulas y Personal
        </div>
        <!--Inicio de quinta seccion-->
        <div class="content ui container grid">
          <div class="fields">
            <div class="  seven wide required field">
            </div>
            <div class="two wide required inline field">
              <div class="ui toggle checkbox">
                @if ($escuela->directorCongrupo==1)
                <input checked="checked" type="checkbox" tabindex="0" class="hidden" value="1" name="directorCongrupo">
                @else
                <input type="checkbox" tabindex="0" class="hidden" value="1" name="directorCongrupo">
                @endif
                <label class="">con grupo</label>
              </div>
            </div>
          </div>
          <div class="fields">
            <div class="  seven wide required field">
            </div>
            <div class="  two wide required field">
              <label class="prompt">Total</label>
              <input type="number"  min="1" name="personal" value="{{$escuela->personal}}">
            </div>
          </div>
          <div class="centered fields">
            <div class="  seven wide  field">
            </div>
            <div class=" two wide required field">
              <label class="prompt">Total</label>
                <input type="number" min="1" name="aulas" value="{{$escuela->aulas}}">
                <i class=""></i>
            </div>
          </div>
        </div>
      </div>
      <!--Fin de accordion-->
      {{method_field('PATCH')}}
    </form>
    <!--Fin de forma -->
  @stop
