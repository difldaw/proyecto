@extends('layouts.master')

  @section('botones')
    <div class="row">
      @can('editarEscuela')
        @if($escuela->deleted_at==NULL)
        <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/escuela/{{$escuela->id_escuela}}/edit">
        @else
        <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/escuela/{{$escuela->id_escuela}}/edit">
        @endif
        <i class="write icon"></i>
        </button>
      @endcan
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">

      @if($escuela->deleted_at==NULL)
        @can('eliminarEscuela')
          <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
          <i class="trash outline icon"></i>
        @endcan
      @else
        @can('reactivarEscuela')
          <button class="  ui circular massive right floated teal undo icon button" onclick="$('#habilitar').modal('show');" >
          <i class=" undo icon"></i>
        @endcan
      @endif
      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">

      <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/escuela">
      <i class="chevron left icon"></i>
      </button>
    </div>
  @stop

  @section('titulo_seccion')
    Escuela {{$escuela->nombre}}
  @stop

  @section('scripts')
    <script>
      $( document ).ready(function() {
        $('.ui.accordion').accordion();
      });
    </script>
    <script>

        function init_map(){
          var myOptions = {
          zoom:17,
          center:new google.maps.LatLng({{$escuela->latitud}},{{$escuela->longitud}}),
          mapTypeId: google.maps.MapTypeId.ROADMAP};
          map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
          marker = new google.maps.Marker({
            map: map,position: new google.maps.LatLng({{$escuela->latitud}},{{$escuela->longitud}})});
            infowindow = new google.maps.InfoWindow({
              content:'<strong>{{$escuela->nombre}}</strong><br/>{{$escuela->domicilio}} {{$escuela->codigoPostal}}'});
              google.maps.event.addListener(marker, 'click', function(){
                infowindow.open(map,marker);});infowindow.open(map,marker);

        }google.maps.event.addDomListener(window, 'load', init_map);
    </script>
  @stop

  @section('contenido')
    <!--Modal de confirmacion de regreso a index-->
    <div class="ui small modal" id="eliminar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Eliminar Escuela
      </div>
      <div class=" content">
        <p>¿Seguro que deseas eliminar la escuela {{$escuela->nombre}}?</p>
      </div>
      <div class="actions">
          <div class="ui red  cancel button">
            <i class="remove icon"></i>
            No
          </div>
          <button class="ui green ok submit button "  type="submit" form="vista" value="delete">
            <i class="checkmark icon"></i>
            Si
          </button>
      </div>
    </div>
    <!--Modal de confirmacion de habilitacion de escuela-->
    <div class="ui small modal" id="habilitar">
      <i class="close icon"></i>
      <div class="ui icon header">
        <i class="archive icon"></i>
        Reactivar Escuela
      </div>
      <div class=" content">
        <p>¿Seguro que deseas reactivar la escuela {{$escuela->nombre}}?</p>
      </div>
      <div class="actions">
          <div class="ui red  cancel button">
            <i class="remove icon"></i>
            No
          </div>
          <button class="ui green ok submit button " onclick=window.location.href="/escuela/reac/{{$escuela->id_escuela}}">
            <i class="checkmark icon"></i>
            Si
          </button>
      </div>
    </div>
    <!--Inicio de forma de visualizacion que utiliza el metodo destroy para eliminar escuelas-->
    <form id="vista" action="{{route('escuela.destroy',$escuela->id_escuela)}}" method="POST" >
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <!--validacion de escuela no eliminada para seccion de informacion-->
      @if($escuela->deleted_at==NULL)
        <div class="ui message"><i class="info circle icon"></i>Informacion de la escuela </div>
      @else
        <div class="ui negative message"><i class="info circle icon"></i>Escuela deshabilitada. Para editar deshacer eliminación. </div>
      @endif
      <!--Seccion de mapa-->
      <div  class="row">
        <div id='gmap_canvas' style='height:300px;width:100%;'>
        </div>
      </div>
      <!--Inici de accordion-->
      <div class="ui styled fluid accordion ">
        <!--Titulo primero-->
        <div class="title active ">
          <i class="dropdown icon"></i>
          Datos generales
        </div>
        <!--Primera seccion-->
        <div class="content active ui container grid">
          <div class="row">
            <div class="ui twelve wide column">
              <strong>Nombre de la Escuela:</strong> {{$escuela->nombre}}
            </div>
            <div class=" ui four wide column">
              <strong>Clave:</strong> {{$escuela->clave}}
            </div>
          </div>
          <div class="row">
            <div class="ui six wide column">
              <strong>Domicilio:</strong> {{$escuela->domicilio}}
            </div>
            <div class=" ui four wide column">
              <strong>Col.</strong> {{$escuela->colonia}}
            </div>
            <div class=" ui two wide column">
              <strong>CP.</strong> {{$escuela->codigoPostal}}
            </div>
            @if ($escuela->delegacion !=null)
            <div class=" ui four wide column">
              <strong>Delegacion:</strong> {{$escuela->delegacion}}
            </div>
            @endif
          </div>
          <div class="row">
            <?php $localidades= app('App\Http\Controllers\EscuelaController')->getLocalidad($escuela->localidad_id); ?>
            <div class=" ui six wide column">
              @foreach ($localidades as $localidad)
              <strong>Localidad:</strong> {{$localidad->nombre}}
              @endforeach
            </div>
            <?php $municipios= app('App\Http\Controllers\EscuelaController')->getMunicipio($escuela->municipio_id); ?>
            <div class=" ui six wide column">
              @foreach ($municipios as $municipio)
              <strong>Municipio:</strong> {{$municipio->nombre}}
              @endforeach
            </div>
            <div class=" ui four wide column">
              <strong>Telefono:</strong> {{$escuela->telefono}}
            </div>
          </div>

          <div class="row">
            <?php $zonas= app('App\Http\Controllers\EscuelaController')->getZona($escuela->zona_id); ?>
            <div class=" ui four wide column">
              @foreach ($zonas as $zona)
              <strong>Zona:</strong> {{$zona->nombre}}
              @endforeach
            </div>
            <?php $turnos= app('App\Http\Controllers\EscuelaController')->getTurno($escuela->turno_id); ?>
            <div class=" ui four wide column">
              @foreach ($turnos as $turno)
              <strong>Turno:</strong> {{$turno->nombre}}
              @endforeach
            </div>
            <?php $sectores= app('App\Http\Controllers\EscuelaController')->getSector($escuela->sector_id); ?>
            <div class=" ui four wide column">
              @foreach ($sectores as $sector)
              <strong>Sector:</strong> {{$sector->nombre}}
              @endforeach
            </div>
            <?php $grados= app('App\Http\Controllers\EscuelaController')->getGrado($escuela->gradoEscolar_id); ?>
            <div class=" ui four wide column">
              @foreach ($grados as $grado)
              <strong>Grado:</strong> {{$grado->nombre}}
              @endforeach
            </div>
          </div>
          <div class="row">
            <div class=" ui eight wide column">
              <strong>Director:</strong> {{$escuela->director}}

            </div>
            <?php $servicios= app('App\Http\Controllers\EscuelaController')->getTurno($escuela->servicio_id); ?>
            <div class=" ui four wide column">
              @foreach ($servicios as $servicio)
              <strong>Servicio:</strong> {{$servicio->nombre}}
              @endforeach
            </div>
            <?php $sostenimientos= app('App\Http\Controllers\EscuelaController')->getSector($escuela->sostenimiento_id); ?>
            <div class=" ui four wide column">
              @foreach ($sostenimientos as $sostenimiento)
              <strong>Sostenimiento:</strong> {{$sostenimiento->nombre}}
              @endforeach
            </div>

          </div>


        </div>
        <!--Titulo degundo-->
        <div class="title">
          <i class="dropdown icon"></i>
          Alumnos
        </div>
        <!--Segunda seccion-->
        <div class="content ui container grid">
          <div class="centered row">
            <div class="four wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Niñas</th>
                    <th>Niños</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->alumnosNinas}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosNinas}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="six wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>1°</th>
                    <th>2°</th>
                    <th>3°</th>
                    @if ($escuela->gradoEscolar_id==3)
                    <th>4°</th>
                    <th>5°</th>
                    <th>6°</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->alumnosPrimero}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosSegundo}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosTercero}}
                    </td>
                    @if ($escuela->gradoEscolar_id==3)
                    <td class="center aligned">
                      {{$escuela->alumnosCuarto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosQuinto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosSexto}}
                    </td>
                    @endif
                  </tr>
                </tbody>
              </table>
            </div>
            @if ($escuela->gradoEscolar_id!=1)
            <div class="four wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Lactantes</th>
                    <th>Maternal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->alumnosLactantes}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->alumnosMaternal}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            @endif
          </div>
        </div>
        <!--Titulo tercero-->
        <div class="title">
          <i class="dropdown icon"></i>
          Grupos
        </div>
        <!--Tercera seccion-->
        <div class="content ui container grid">
          <div class=" centered row">
            <div class="six wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>1°</th>
                    <th>2°</th>
                    <th>3°</th>
                    @if ($escuela->gradoEscolar_id==3)
                    <th>4°</th>
                    <th>5°</th>
                    <th>6°</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->gruposPrimero}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->gruposSegundo}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->gruposTercero}}
                    </td>
                    @if ($escuela->gradoEscolar_id==3)
                    <td class="center aligned">
                      {{$escuela->gruposCuarto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->gruposQuinto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->gruposSexto}}
                    </td>
                    @endif
                  </tr>
                </tbody>
              </table>
            </div>
            @if ($escuela->gradoEscolar_id!=1)
            <div class="four wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Lactantes</th>
                    <th>Maternal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->gruposLactantes}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->gruposMaternal}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            @endif
          </div>
        </div>
        <!--Titulo cuarto-->
        <div class="title">
          <i class="dropdown icon"></i>
          Docentes
        </div>
        <!--Cuarta seccion-->
        <div class="content ui container grid">
          <div class="centered row">
            <div class="four wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Mujeres</th>
                    <th>Hombres</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->docentesMujeres}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesHombres}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="six wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>1°</th>
                    <th>2°</th>
                    <th>3°</th>
                    @if ($escuela->gradoEscolar_id==3)
                    <th>4°</th>
                    <th>5°</th>
                    <th>6°</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->docentesPrimero}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesSegundo}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesTercero}}
                    </td>
                    @if ($escuela->gradoEscolar_id==3)
                    <td class="center aligned">
                      {{$escuela->docentesCuarto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesQuinto}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesSexto}}
                    </td>
                    @endif
                  </tr>
                </tbody>
              </table>
            </div>
            @if ($escuela->gradoEscolar_id!=1)
            <div class="four wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Lactantes</th>
                    <th>Maternal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      {{$escuela->docentesLactantes}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->docentesMaternal}}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            @endif
          </div>
        </div>
        <!--Titulo quinto-->
        <div class="title">
          <i class="dropdown icon"></i>
          Director, Aulas y Personal
        </div>
        <!--Quinta seccion-->
        <div class="content ui container grid">
          <div class="centered row">
            <div class="six wide column">
              <table class="ui very basic collapsing celled table">
                <thead>
                  <tr>
                    <th>Director con grupo </th>
                    <th>Personal</th>
                    <th>Aulas</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center aligned">
                      @if ($escuela->directorCongrupo==1)
                      si
                      @else ($escuela->directorCongrupo==0)
                      no
                      @endif
                    </td>
                    <td class="center aligned">
                      {{$escuela->personal}}
                    </td>
                    <td class="center aligned">
                      {{$escuela->aulas}}
                    </td>

                  </tr>
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
      {{method_field('DELETE')}}
    </form>
  @stop
