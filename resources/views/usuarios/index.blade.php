@extends('layouts.master')

  @section('scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
        $("#todos").hide();
        $("#usuarios").show();
      });
    </script>
    <script>
      function eliminados(){
        if($('.ui.checkbox').checkbox('is checked')){
          $("#todos").show();
          $("#usuarios").hide();
        }else{
          $("#usuarios").show();
          $("#todos").hide();
        }
      }
    </script>
  @stop

  @section('titulo_seccion')
    Usuarios
  @stop


  @section('botones')
    @can('crearEscuela')
      <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="{{route('usuario.create')}}">
      <i class="plus icon"></i>
      </button>
    @endcan
  @stop

  @section('contenido')
    <!--Inicio de container independiente de tablas-->
    <div class="ui grid container">
      <div class="row">
        <div class="twelve wide column"></div>
        <div class="four wide column">
          <div class=" ui slider checkbox ">
            <input type="checkbox" name="eliminados" value="1" onchange="eliminados()">
            <label>Mostrar eliminadas</label>
          </div>
        </div>
      </div>
    </div>



    <table class="ui selectable sortable teal celled table" id="todos">
      <thead>
        <tr>
          <th class="four wide">Nombre</th>
          <th class="two wide">Apellido Paterno</th>
          <th class="two wide">Apellido Materno</th>
          <th class="two wide">Email</th>
          <th class="two wide">Tipo Usuario</th>
          <th class="two wide">Municipio</th>
          <th class="one wide">Ver</th>
        </tr>
      </thead>
      <tbody>
        @foreach($todos as $usuario)
          @if($usuario->deleted_at!=NULL)
            <tr class="negative">
          @else
            <tr class="positive">
          @endif
            <td>
              {{$usuario->nombre}}
            </td>
            <td>{{$usuario->apellido_p}}</td>
            <td>{{$usuario->apellido_m}}</td>
            <td>{{$usuario->email}}   </td>
            <td> @if($usuario->tipo_usuario==1)
                    Admin
                  @elseif($usuario->tipo_usuario==2)
                    Central
                  @else
                    Foráneo
                  @endif
            </td>
            <td> {{$usuario->municipio->nombre}}</td>
             <td onclick=window.location.href="{{route('usuario.show', $usuario->id)}}" class=" selectable center aligned">
                <i class="info circle big icon "></i>
             </td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th colspan="7">
            <div class="ui right floated pagination menu">
              <a href="{!! $todos->previousPageUrl() !!}" class="icon item">
                <i class="left chevron icon "></i>
              </a>
              @for ($pages = 1; $pages <= $todos->lastPage(); $pages++)
                  <a href="{!! $todos->url($pages) !!}" class="item ">{{$pages}}</a>
              @endfor
              <a href="{!! $todos->nextPageUrl() !!}" class="icon item">
                <i class="right chevron icon "></i>
              </a>
            </div>
          </th>
        </tr>
      </tfoot>
    </table>

    <table class="ui selectable sortable teal celled table" id="usuarios">
      <thead>
        <tr>
          <th class="four wide">Nombre</th>
          <th class="two wide">Apellido Paterno</th>
          <th class="two wide">Apellido Materno</th>
          <th class="two wide">Email</th>
          <th class="two wide">Tipo Usuario</th>
          <th class="two wide">Municipio</th>
          <th class="one wide">Ver</th>
        </tr>
      </thead>
      <tbody>
        @foreach($usuarios as $usuario)
            <tr>
            <td>
              {{$usuario->nombre}}
            </td>
            <td>{{$usuario->apellido_p}}</td>
            <td>{{$usuario->apellido_m}}</td>
            <td>{{$usuario->email}}   </td>
            <td> @if($usuario->tipo_usuario==1)
                    Admin
                  @elseif($usuario->tipo_usuario==2)
                    Central
                  @else
                    Foráneo
                  @endif
            </td>
            <td>{{$usuario->municipio->nombre}}</td>
             <td onclick=window.location.href="{{route('usuario.show', $usuario->id)}}" class=" selectable center aligned">
                <i class="info circle big icon "></i>
             </td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
          <th colspan="7">
            <div class="ui right floated pagination menu">
              <a href="{!! $todos->previousPageUrl() !!}" class="icon item">
                <i class="left chevron icon "></i>
              </a>
              @for ($pages = 1; $pages <= $todos->lastPage(); $pages++)
                  <a href="{!! $todos->url($pages) !!}" class="item ">{{$pages}}</a>
              @endfor
              <a href="{!! $todos->nextPageUrl() !!}" class="icon item">
                <i class="right chevron icon "></i>
              </a>
            </div>
          </th>
        </tr>
      </tfoot>
    </table>
  @stop
