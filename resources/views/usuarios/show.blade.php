@extends('layouts.master')


@section('titulo_seccion')
  Usuario
@endsection




@section('botones')
  <div class="row">
    @can('editarEscuela')
      @if($usuario->deleted_at==NULL)
      <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/usuario/{{$usuario->id}}/edit">
      @else
      <button class="  ui circular massive right floated teal write icon submit disabled button"  onclick=window.location.href="/usuario/{{$usuario->id}}/edit">
      @endif
      <i class="write icon"></i>
      </button>
    @endcan
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    @if($usuario->deleted_at==NULL)
        <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
        <i class="trash outline icon"></i>
    @else
        <button class="  ui circular massive right floated teal undo icon button" onclick="$('#habilitar').modal('show');" >
        <i class=" undo icon"></i>
    @endif
    </button>
  </div>
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="{{route('usuario.index')}}">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop







@section('contenido')
<!--Modal de confirmacion de regreso a index-->
<div class="ui small modal" id="eliminar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Eliminar Usuario
  </div>
  <div class=" content">
    <p>¿Seguro que deseas eliminar el usuario {{$usuario->nombre}}?</p>
  </div>
  <div class="actions">
      <div class="ui red  cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <button class="ui green ok submit button "  type="submit" form="vista" value="delete">
        <i class="checkmark icon"></i>
        Si
      </button>
  </div>
</div>


<!--Modal de confirmacion de habilitacion de escuela-->
<div class="ui small modal" id="habilitar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Reactivar Escuela
  </div>
  <div class=" content">
    <p>¿Seguro que deseas reactivar la escuela {{$usuario->nombre}} con email : {{$usuario->email}}?</p>
  </div>
  <div class="actions">
      <div class="ui red  cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <button class="ui green ok submit button " onclick=window.location.href="{{route('habilitaUsuario',$usuario->id)}}">
        <i class="checkmark icon"></i>
        Si
      </button>
  </div>
</div>



<form id="vista" method="post" action="{{route('usuario.destroy',$usuario->id)}}">


    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    {{method_field('DELETE')}}
    <div class="ui card centered">
      <div class="image">
        <img src="{{asset('img/avatar.jpg')}}"/>
      </div>
      <div class="content">
        <div class="header">
          {{$usuario->nombre}}   {{$usuario->apellido_p}}  {{$usuario->apellido_m}}
        </div>
        <div class="meta">
          {{$usuario->email}}
        </div>
        <div class="description">

          @if($usuario->tipo_usuario==1)
            Ubicado en : {{$usuario->municipio->nombre}}
          @elseif($usuario->tipo_usuario==2)
            Ubicado en : {{$usuario->municipio->nombre}}
          @else
            Responsable de  : {{$usuario->municipio->nombre}}
          @endif
           <br />

          Rol:
          @if($usuario->tipo_usuario==1)
            Admin
          @elseif($usuario->tipo_usuario==2)
            Central
          @else
            Foráneo
          @endif

        </div>

      </div>
    </div>
  </form>
@endsection
