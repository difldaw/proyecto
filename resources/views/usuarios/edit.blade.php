@extends('layouts.master')

@section('titulo_seccion')
  Editando usuario : {{$usuario->nombre}}  {{$usuario->apellido_p}} {{$usuario->apellido_m}}
@endsection

@section('scripts')

  <script type="text/javascript">
  function envio(){
    $('.ui.form').form('validate form');
    if ($('.ui.form').form('is valid')){
      $('#guardar').modal('show');
    }

  }

  </script>

  <script type="text/javascript">

  $(function() {
    $('.ui.form').form({
      on:'blur',
      inline:true,
      fields: {
        nombre: {
          identifier  : 'nombre',
          rules: [
            {
              type   : 'empty',
              prompt : 'El nombre es necesario'
            }
          ]
        },
        apellido_p: {
         identifier  : 'apellido_p',
         rules: [
           {
             type   : 'empty',
             prompt : 'El apellido paterno es necesario'
           }
         ]
        },
        apellido_m: {
          identifier  : 'apellido_m',
          rules: [
            {
              type   : 'empty',
              prompt : 'El apellido materno es necesario'
            }
          ]
        },
        email: {
         identifier  : 'email',
         rules: [
           {
             type   : 'empty',
             prompt : 'El email es necesario'
           },
           {
             type   : 'email',
             prompt : 'El email {value} no es válido'
           }
         ]
        },
        municipio_id: {
          identifier  : 'municipio_id',
          rules: [
            {
              type   : 'empty',
              prompt : 'Por favor selecciona un municipio'
            }
          ]
        },
        tipo_usuario: {
          identifier  : 'tipo_usuario',
          rules: [
            {
              type   : 'empty',
              prompt : 'Por favor selecciona un rol'
            }
          ]
        },
        password: {
          identifier  : 'password',
          optional   : true,
          rules: [
            {
              type   : 'minLength[6]',
              prompt : 'La contraseña debe tener al menos 6 caracteres'
            }
          ]
        },
        confirm_pass: {
          identifier  : 'confirm_pass',
          rules: [
            {
              type   : 'match[password]',
              prompt : 'Las contraseñas no coinciden'
            }
          ]
        },
      }
    });
  });
  </script>
@endsection


@section('botones')
  <div class="row">
    <button class="ui circular massive right floated teal save icon submit button" onclick="envio()" >
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
@endsection

@section('contenido')


<!--Modal de confirmacion de regreso a show-->
<div class="ui small modal" id="regresar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Descartar registro
  </div>
  <div class=" content">
      <p>¿Estás seguro que deseas regresar?. Se perderan todos los campos.</p>
  </div>
  <div class="actions">

    <div class="ui red cancel button">
      <i class="remove icon"></i>
      No
    </div>
    <div class="ui green ok button" onclick=window.location.href="{{route('usuario.show', $usuario->id)}}">
      <i class="checkmark icon"></i>
      Yes
    </div>
  </div>
</div>
<!--Modal de confirmacion de envio de modificaciones-->
<div class="ui small modal" id="guardar">
  <i class="close icon"></i>
  <div class="ui icon header">
    <i class="archive icon"></i>
    Guardar cambios
  </div>
  <div class="content">
      <p>¿Deseas guardar los cambios en escuela {{$usuario->nombre}}?</p>
  </div>
  <div class="actions">
      <div class="ui red cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <button class="ui green ok submit button" type="submit" form="editar">
        <i class="checkmark icon"></i>
        Si
      </button>
  </div>
</div>


  <form id="editar" action="{{route('usuario.update',$usuario->id)}}" class="ui form" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    {{method_field('PATCH')}}

    <div class="field">
      <label class="prompt">Nombre Completo</label>
      <div class="three fields">
        <div class="field">
          <input type="text" name="nombre" placeholder="Juan Pablo" value="{{$usuario->nombre}}">
        </div>
        <div class="field">
          <input type="text" name="apellido_p" placeholder="López" value="{{$usuario->apellido_p}}">
        </div>
        <div class="field">
          <input type="text" name="apellido_m" placeholder="Martínez" value="{{$usuario->apellido_m}}">
        </div>
      </div>
    </div>
    <div class="field">
      <div class="three fields">
        <div class="field">
          <label>Correo Electónico</label>
          <input type="text" name="email" placeholder="juan.lopez@queretaro.gob.mx" value="{{$usuario->email}}">
        </div>
        <div class="field">
          <label>Responsable de Municipio</label>
          <select class="ui fluid dropdown" name="municipio_id">
            <option></option>
            @foreach($municipios as $municipio)
              @if($usuario->municipio_id == $municipio->id)
                <option selected value="{{$municipio->id}}">{{$municipio->nombre}}</option>
              @else
                <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
              @endif
            @endforeach
          </select>
        </div>
        <div class="field">
          <label>Rol de Usuario</label>
          <select class="ui fluid dropdown" name="tipo_usuario">
            <option></option>
            @if($usuario->tipo_usuario == 1)
              <option selected value="1">Administrador</option>
              <option value="2">Central - DIF Estatal</option>
              <option value="3">Foráneo - DIF Municipal</option>
            @elseif($usuario->tipo_usuario == 2)
              <option value="1">Administrador</option>
              <option value="2">Central - DIF Estatal</option>
              <option selected value="3">Foráneo - DIF Municipal</option>
            @elseif($usuario->tipo_usuario == 3)
              <option value="1">Administrador</option>
              <option value="2">Central - DIF Estatal</option>
              <option selected value="3">Foráneo - DIF Municipal</option>
            @endif

          </select>
        </div>
      </div>
    </div>
    <div class="field">
      <div class="two fields">
        <div class="field">
          <label>Nueva Contraseña</label>
          <input type="password" name="password" />
        </div>
        <div class="field">
          <label>Confirmar Contraseña</label>
          <input type="password" name="confirm_pass" />
        </div>
      </div>
    </div>
  </form>
@endsection
