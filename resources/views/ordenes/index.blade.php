@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón crear -->
  <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="{{route('ordenes.create')}}">
    <i class="plus icon"></i>
  </button>
  <!-- Fin del botón crear -->
@stop

@section('titulo_seccion')
  Ordenes de Compra
@stop

@section('scripts')

@stop

@section('contenido')

  <!--Inicio de la tabla sin filtrado de eliminados-->
  <table class="ui selectable sortable teal celled table">
    <thead>
      <tr>
        <th class="two wide center aligned">No. Folio</th>
        <th class="six wide center aligned">Programa Alimenticio</th>
        <th class="three wide center aligned">Municipio</th>
        <th class="one wide center aligned">Mes</th>
        <th class="one wide center aligned">Año</th>
        <th class="one wide center aligned">Ver</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ordenes_de_compra as $orden_de_compra)
        <tr>
          <td class="center aligned">{{$orden_de_compra->no_folio}}</td>
          <td class="center aligned">{{$orden_de_compra->programa->tipo}}</td>
          <td class="center aligned">{{$orden_de_compra->municipio->nombre}}</td>
          <td class="center aligned">{{$orden_de_compra->mes}}</td>
          <td class="center aligned">{{$orden_de_compra->anio}}</td>
          <td onclick=window.location.href="{{route('ordenes.show', $orden_de_compra->id)}}" class=" selectable center aligned">
             <i class="info circle big icon "></i>
          </td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th colspan="9">
          <div class="ui right floated pagination menu">
            <a href="{!! $ordenes_de_compra->previousPageUrl() !!}" class="icon item">
              <i class="left chevron icon "></i>
            </a>
            @for($pages = 1; $pages <= $ordenes_de_compra->lastPage(); $pages++)
                <a href="{!! $ordenes_de_compra->url($pages) !!}" class="item ">{{$pages}}</a>
            @endfor
            <a href="{!! $ordenes_de_compra->nextPageUrl() !!}" class="icon item">
              <i class="right chevron icon "></i>
            </a>
          </div>
        </th>
      </tr>
    </tfoot>
  </table>
  <!--Final de la tabla sin filtrado de eliminados-->

@stop
