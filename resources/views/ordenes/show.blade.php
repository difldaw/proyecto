@extends('layouts.master')

@section('botones')

  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de eliminar o rehabilitar, siendo el caso -->
  <div class="row">
    <button class="  ui circular massive right floated teal trash outline icon button" onclick="$('#eliminar').modal('show');" >
      <i class="trash outline icon"></i>
    </button>
  </div>
  <!-- Fin del botón de eliminar o rehabilitar, siendo el caso -->

  <div style="visibility:hidden">
    <br />..
  </div>

  <!-- Inicio de botón de regresar al index -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="{{route('ordenes.index')}}">
      <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Fin de botón de regresar al index -->
@stop

@section('titulo_seccion')
  {{$orden_de_compra->programa->tipo}} {{$orden_de_compra->mes}} del {{$orden_de_compra->anio}}
@stop

@section('scripts')
   <script>
      $( document ).ready(function() {
         // Inicialización de elementos de Semantic UI
         $('.ui.modal').modal();
      });
   </script>
@stop

@section('contenido')

   <!-- Inicio del Modal de confirmacion de regreso a index -->
   <div class="ui small modal" id="eliminar">
      <i class="close icon"></i>
      <div class="ui icon header">
         <i class="archive icon"></i>
         Eliminar Orden de Compra
      </div>
      <div class=" content">
         <p>¿Seguro que deseas eliminar la orden de compra?</p>
      </div>
      <div class="actions">
           <div class="ui negative cancel button">
             <i class="remove icon"></i>
             No
           </div>
           <button class="ui positive submit button "  type="submit" form="vista" value="delete">
             <i class="checkmark icon"></i>
             Si
           </button>
      </div>
   </div>
   <!-- Final del Modal de confirmacion de regreso a index -->


   <!--Inicio de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
   <form id="vista" class="ui form" action="{{route('ordenes.destroy', $orden_de_compra->id)}}" method="POST" >
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="_method" value="DELETE">

      <!--Inicio de la tabla sin filtrado de eliminados-->
      <table class="ui selectable sortable teal celled table">
        <thead>
          <tr>
            <th class="two wide center aligned">No.</th>
            <th class="six wide center aligned">Productos</th>
            <th class="three wide center aligned">Marca</th>
            <th class="one wide center aligned">Presentacion</th>
            <th class="one wide center aligned">Contenido Neto</th>
            <th class="one wide center aligned">Unidad de Medida</th>
            <th class="one wide center aligned">Cantidad</th>
            <th class="one wide center aligned">Precio</th>
            <th class="one wide center aligned">Costo Total</th>
          </tr>
        </thead>
        <tbody>
          @foreach($productos as $producto)
            <tr>
              <td class="center aligned">{{($i++)+1}}</td>
              <td class="center aligned">{{$producto->nombre}}</td>
              <td class="center aligned">{{$producto->marca}}</td>
              <td class="center aligned">{{$producto->presentacion->nombre}}</td>
              <td class="center aligned">{{$producto->contenido_neto}}</td>
              <td class="center aligned">{{$producto->unidad_de_medida->nombre}}</td>
              <td class="center aligned">{{$cantidades[$i -1]}}</td>
              <td class="center aligned">{{$producto->precio}}</td>
              <td class="center aligned">{{$precios[$i-1]}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <!--Final de la tabla sin filtrado de eliminados-->

   </form>
   <!--Final de forma de visualizacion que utiliza el metodo destroy para eliminar Producto-->
@stop
