@extends('layouts.master')

@section('botones')
  <!-- Inicio del botón de guardado -->
  <div class="row">
    <button id="save" class="ui circular massive right floated teal save icon submit button" onclick="envio()">
    <i class="save icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
  <div style="visibility:hidden">
    <br />..
  </div>
  <!-- Inicio del botón de regresar -->
  <div class="row">
    <button class=" ui circular massive right floated chevron left icon button" onclick="$('#regresar').modal('show');">
    <i class="chevron left icon"></i>
    </button>
  </div>
  <!-- Final del botón de guardado -->
@stop

@section('titulo_seccion')
  Registrar Orden de Compra
@stop

@section('scripts')
  <script type="text/javascript">
    // Función para verificar si el form esta validado
    function envio(){
      $('#registro_form').form('validate form');
      //Si es true, se mostrara el modal con el id "guardar"
      if($('#registro_form').form('is valid')){
        $('#guardar').modal('show');
      }
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      //Inicialización de elementos de Semantic UI
      $('.ui.modal').modal();
      $('.menu .item').tab();
      $('select').dropdown();
      //Validaciones dentro de la forma con id "registro_form"
      $('#registro_form').form({
        inline: true,
        fields:{
          no_folio:{
            identifier: 'no_folio',
            rules:[{
              type: 'empty',
              prompt: 'Número de folio necesario'
            }]
          },
          programa_id:{
            identifier: 'programa_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un programa'
            }]
          },
          municipio_id:{
            identifier: 'municipio_id',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un municipio'
            }]
          },
          mes:{
            identifier: 'mes',
            rules:[{
              type: 'empty',
              prompt: 'Seleccione un mes'
            }]
          },
          anio:{
            identifier: 'anio',
            rules:[{
              type   : 'empty',
              prompt : 'Seleccione un año'
            }]
          }
        }
      });
    });
  </script>
@stop

@section('contenido')
   <!--Inicio del Modal de confirmacion de regresar a index-->
  <div class="ui small modal" id="regresar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Descartar registro
    </div>
    <div class=" content">
        <p>¿Estás seguro que deseas regresar?. Se perderan todos los datos sin guardar.</p>
    </div>
    <div class="actions">
      <div class="ui negative cancel button">
        <i class="remove icon"></i>
        No
      </div>
      <div class="ui positive button" onclick=window.location.href="{{route('ordenes.index')}}">
        <i class="checkmark icon"></i>
        Si
      </div>
    </div>
  </div>
  <!--Final del Modal de confirmacion de regresar a index-->

  <!--Inicio del Modal de confirmacion de envio de formulario-->
  <div class="ui modal" id="guardar">
    <i class="close icon"></i>
    <div class="ui icon header">
      <i class="archive icon"></i>
      Guardar registro nuevo
    </div>
    <div class="content">
        <p>¿Deseas guardar el registro de la localidad?</p>
    </div>
    <div class="actions">
        <div class="ui negative cancel button">
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui positive submit button" type="submit" form="registro_form">
          <i class="checkmark icon"></i>
          Si
        </button>
    </div>
  </div>
  <!--Final del Modal de confirmacion de envio de formulario-->

  <!--Inicio del Contenedor de forma de registro-->
  <div class="ui grid container">
    <div class="row">
      <div class="column">
        <!--Inicio de la forma de registro-->
        <form id="registro_form" class="ui form" action="{{route('ordenes.store')}}" method="POST">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="_method" value="POST">
          <h4 class="ui horizontal divider header">
            <i class="tag icon">
            </i>
            Datos generales
          </h4>
          <!-- Inicio de la Seccion de datos generales -->
            <!-- Inicio de la primera fila -->
            <div class="three fields">
              <!-- Input text para el campo de "Nombre" -->
              <div class="seven wide required field">
                <label class="prompt">Número de Folio</label>
                <input type="text" placeholder="Ej. 4563156..." name="no_folio">
              </div>

              <!-- Select para el campo de "Presentacion" -->
              <div class="three wide required field">
                <label class="prompt">Programas Alimenticios</label>
                <select class="ui fluid dropdown" name="programa_id">
                  <option value=""></option>
                  @foreach($programas as $programa)
                  <option value="{{$programa->id_programa}}">{{$programa->tipo}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- Final de la primera fila -->

            <!-- Inicio de la segunda fila -->
              <div class="two fields">

                 <!-- Input text para el campo de "Municipio" -->
                 <div class="three wide required field">
                    <label class="prompt">Municipio</label>
                    <select class="ui fluid dropdown" name="municipio_id">
                      <option value=""></option>
                      @foreach($municipios as $municipio)
                      <option value="{{$municipio->id}}">{{$municipio->nombre}}</option>
                      @endforeach
                    </select>
                 </div>

                 <!-- Input text para el campo de "Provedor" -->
                 <div class="three wide required field">
                    <label class="prompt">Mes</label>
                    <select class="ui fluid dropdown" name="mes">
                      <option value=""></option>
                      @foreach($meses as $mes)
                      <option value="{{$mes}}">{{$mes}}</option>
                      @endforeach
                    </select>
                 </div>

                 <!-- Select para el campo de "Unidad de Medida" -->
                 <div class="two wide required field">
                   <label class="prompt">Año</label>
                   <select class="ui fluid dropdown" name="anio">
                     <option value=""></option>
                     @foreach($anios as $anio)
                     <option value="{{$anio}}">{{$anio}}</option>
                     @endforeach
                   </select>
                 </div>

              </div>
            <!-- Final de la segunda fila -->
          <!-- Final de la Seccion de datos generales -->
        </form>
        <!--Final de la forma de registro-->
      </div>
    </div>
  </div>
  <!--Final del Contenedor de forma de registro-->
@stop
