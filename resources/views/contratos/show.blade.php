@extends('layouts.master')
@section('botones')
    <div class="row">
      <button class="  ui circular massive right floated teal write icon submit button"  onclick=window.location.href="/contrato/{{$contrato->id_contrato}}/edit">
      <i class="write icon"></i>
      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">
      <script>
      function activarModal(){
        $('.small.modal').modal('show');
      }
      </script>
      <button class="  ui circular massive right floated teal trash outline icon button"  onclick="activarModal()" class="ui icon button" >
        <i class="trash outline icon"></i>

      </button>
    </div>
    <div style="visibility:hidden">
      <br />..
    </div>
    <div class="row">

      <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/provedor/{{$contrato->id_provedor}}">
      <i class="chevron left icon"></i>
      </button>
    </div>
  @stop




  @section('contenido')


<form id="vista" action="{{route('contrato.destroy',$contrato->id_contrato)}}" method="POST" >
  <input type="hidden" name="_method" value="DELETE">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <script type="text/javascript">

  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.accordion').accordion();
    $('.ui.form')
      .form();
  });

    $('.ui.modal')
    .modal('show')
    ;

  </script>

  <div class="ui small modal">
    <i class="close icon"></i>
    <div class="header">
      Borrar Contrato
    </div>
    <div class="image content">
      <div class="image">
        <i class="trash icon"></i>
      </div>
      <div class="description">
        <h3>Esta seguro que quiere borrar el contrato?</h3>
      </div>
    </div>
    <div class="actions">
      <div class="two fluid ui buttons">
        <div class="ui red cancel button" >
          <i class="remove icon"></i>
          No
        </div>
        <button class="ui green submit button" type="submit" form="vista" value="delete">
          <i class="checkmark icon"></i>
          Si
        </button>
      </div>
    </div>
  </div>

  <div class="ui styled fluid accordion ">
      <div class="title active ">
        <i class="dropdown icon"></i>
        Datos generales
      </div>
      <div class="content active ui container grid">
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Nombre del contrato:</strong> {{$contrato->nombre}}
          </div>
        </div>
        <div class="row">
          <div class=" ui twelve wide column">
            <strong>Fecha de inicio:</strong> {{$contrato->fechainicio}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Fecha de finalizacion:</strong> {{$contrato->fechafin}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column">
            <strong>Monto Asignado:</strong> {{$contrato->montoasignado}}
          </div>
        </div>
        <div class="row">
          <div class="ui twelve wide column " style="max-width:800px;
    word-wrap:break-word;">
            <strong>Observacion:</strong>
            <p>
             {{$contrato->observacion}}
           </p>
          </div>
        </div>
      </div>
  </div>
{{method_field('DELETE')}}
</form>
    @stop

    @section('titulo_seccion')
      Mostrar Contrato
    @stop
