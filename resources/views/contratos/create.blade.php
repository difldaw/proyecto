<?php
  define('FROM', $_GET['prov']);

 ?>
@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class=" ui circular massive right floated teal save icon submit button"  type="submit" form="crear">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/provedor/<?=FROM?>">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop


  @section('contenido')

  <script type="text/javascript">

  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.dropdown')
    .dropdown()
    ;
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form({
        inline : true,
          fields: {
            nombre: {
              identifier: 'nombre',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Nombre necesario'
                }
              ]
            },
            fechainicio: {
              identifier: 'fechainicio',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Fecha de inicio necesaria'
                }
              ]
            },
            fechafin: {
              identifier: 'fechafin',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Fecha de finalizacion necesaria'
                }
              ]
            },
            montominimo: {
              identifier: 'montominimo',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Monto Asignado necesaria'
                }
              ]
            }
          }
      });


      /*
      $('#eventForm').formValidation({
        fields: {

          fechafin: {
            validators: {
              date: {
                min: 'fechainicio'
                ...
              }
            }
          }
        }
      });
      */
  });


  </script>

        <div class="ui grid container">

          <div class="row">
            <div class="column">

              <form id="crear" class="ui form" action="{{ url('/contrato') }}" method="POST" >
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>

                <div class="three fields">

                    <div class="eight wide required field">
                      <label class="prompt">Nombre</label>
                      <div class="ui left icon input">
                        <input type="text" placeholder="Nombre de contrato" name="nombre">
                      </div>
                    </div>

                    <div class="four wide required field">
                      <label class="prompt">Fecha de Inicio</label>
                      <div class="ui left icon input">
                        <input type="date" placeholder="" name="fechainicio">
                      </div>
                    </div>


                    <div class="four wide required field">
                      <label class="prompt">Fecha de Finalizacion</label>
                      <div class="ui left icon input">
                        <input type="date" placeholder="" name="fechafin">
                      </div>
                    </div>

                  </div>
                  <div class="one fields">
                <div class="eight wide required field">
                  <label class="prompt">Monto Asignado</label>
                  <div class="ui left icon input">
                    <input type="number" min="0" placeholder="Monto Asignado" name="montoasignado">
                  </div>
                </div>
                </div>
                <div class="one fields">
                <div class="fourteen wide required field">
                  <label class="prompt">Observacion</label>
                    <div class="ui left icon input">
                      <textarea type="text" placeholder="Observacion" name="observacion"></textarea>
                    </div>
                  </div>
                </div>


                <div class="four wide required field">
                  <label class="prompt">Provedor ID</label>
                    <div class="ui left icon input">
                    <input type="number" value="<?=FROM?>" name="provedor_id" readonly>
                </div>

                  </br></br>
              </form>
          </div>
        </div>

      </div>
    </div>

  @stop

  @section('titulo_seccion')
    Crear Contrato
  @stop
