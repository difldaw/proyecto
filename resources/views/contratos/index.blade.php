@extends('layouts.master')
@section('botones')
    <button class="ui circular massive right floated pink plus icon button" onclick=window.location.href="/contrato/create">
    <i class="plus icon"></i>
  </button>
  @stop
  @section('titulo_seccion')
    Contratos
  @stop

  @section('scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
        $("#todas").hide();
        $("#escuelas").show();
      });
    </script>
    <script>
      function eliminados(){
        if($('.ui.checkbox').checkbox('is checked')){
          $("#todas").show();
          $("#escuelas").hide();
        }else{
          $("#escuelas").show();
          $("#todas").hide();
        }
      }
    </script>
  @stop

  @section('contenido')

      <table class="ui fixed sortable teal selectable celled table" id="todas">
        <thead>
          <tr>
            <th class="four wide">Nombre</th>
            <th class="four wide">Fecha de Inicio</th>
            <th class="four wide">Fecha de Finalizacion</th>
            <th class="four wide">Monto Asignado</th>
            <th class="one wide"></th>
          </tr>
        </thead>
        <tbody>

            @foreach($contratos as $contrato)<tr>
              <td>{{$contrato->nombre}}</td>
              <td>{{$contrato->fechainicio}}</td>
              <td>{{$contrato->fechafin}}</td>
              <td>{{$contrato->montoasignado}}</td>
              <td onclick=window.location.href="/contrato/{{$contrato->id_contrato}}" class=" selectable center aligned">
                <i class="info circle big icon "></i>
              </td>
            </tr>@endforeach

        </tbody>
        <tfoot>
          <tr>
            <th colspan="6">
              <div class="ui right floated pagination menu">

                <a href="{!! $contratos->previousPageUrl() !!}" class="icon item">
                  <i class="left chevron icon "></i>
                </a>
                @for ($pages = 1; $pages <= $contratos->lastPage(); $pages++)
                    <a href="{!! $contratos->url($pages) !!}"class="item ">{{$pages}}</a>
                @endfor

                <a href="{!! $contratos->nextPageUrl() !!}"class="icon item">
                  <i class="right chevron icon "></i>
                </a>
              </div>
            </th>
          </tr>
        </tfoot>
      </table>

  @stop
