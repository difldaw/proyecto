@extends('layouts.master')

@section('botones')
  <div class="row">
    <button class="  ui circular massive right floated teal save icon submit button"  type="submit" form="editar">
    <i class="save icon"></i>
    </button>
  </div>
  <div style="visibility:hidden">
    <br />..
  </div>
  <div class="row">

    <button class=" ui circular massive right floated chevron left icon button" onclick=window.location.href="/contrato/{{$contrato->id_contrato}}">
    <i class="chevron left icon"></i>
    </button>
  </div>
@stop

  @section('contenido')


  <script type="text/javascript">


  $( document ).ready(function() {
    $('.menu .item').tab();
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form();

      $('.ui.dropdown')
      .dropdown()
      ;
      $('.ui.checkbox').checkbox();
      $('.ui.form')
        .form({
          inline : true,
            fields: {
              nombre: {
                identifier: 'nombre',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }
                ]
              },
              fechainicio: {
                identifier: 'fechainicio',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Fecha de inicio necesaria'
                  }
                ]
              },
              fechafin: {
                identifier: 'fechafin',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Fecha de finalizacion necesaria'
                  }
                ]
              },
              montoasignado: {
                identifier: 'montoasignado',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Monto Asignado necesario'
                  }
                ]
              }
            }
        });
  });


  </script>
    <form id="editar" class="ui form" action="{{route('contrato.update',$contrato->id_contrato)}}" method="POST">
      <input type="hidden" name="_method" value="PATCH">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui grid container">

          <div class="row">
            <div class="column">

                <h4 class="ui horizontal divider header">
                  <i class="tag icon"></i>
                  Datos generales
                </h4>
              </h4>

              <div class="three fields">

                  <div class="eight wide required field">
                    <label class="prompt">Nombre</label>
                    <div class="ui left icon input">
                      <input type="text"  value="{{$contrato->nombre}}"placeholder="Nombre de contrato" name="nombre">
                    </div>
                  </div>

                  <div class="four wide required field">
                    <label class="prompt">Fecha de Inicio</label>
                    <div class="ui left icon input">
                      <input type="date"  value="{{$contrato->fechainicio}}" placeholder="" name="fechainicio">
                    </div>
                  </div>


                  <div class="four wide required field">
                    <label class="prompt">Fecha de Finalizacion</label>
                    <div class="ui left icon input">
                      <input type="date"  value="{{$contrato->fechafin}}" placeholder="" name="fechafin">
                    </div>
                  </div>

                </div>
                <div class="one fields">

              <div class="eight wide required field">
                <label class="prompt">Monto Asignado</label>
                <div class="ui left icon input">
                  <input type="number" min="0" value="{{$contrato->montoasignado}}" placeholder="Monto Asignado" name="montoasignado">
                </div>
              </div>
              </div>

              <div class="one fields">
              <div class="fourteen wide required field">
                <label class="prompt">Observacion</label>
                <div class="ui left icon input">
                  <textarea type="text" value="{{$contrato->observacion}}" placeholder="Observacion" name="observacion">{{$contrato->observacion}}</textarea>
                </div>

              </div>
              </div>

                <div class="one fields">
                <div class="eight wide required field">
                  <label class="prompt">Provedor ID</label>
                    <div class="ui left icon input">
                      <input type="number" value="{{$contrato->provedor_id}}"name="provedor_id" readonly>
                    </div>
                  </div>

                  </div
                </br>
          </div>
        </div>

      </div>
      {{method_field('PATCH')}}
    </form>
  @stop

  @section('titulo_seccion')
    Editar Contrato
  @stop
