<!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SEDIF - Padrón Beneficiarios</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

      <link href="<?php echo e(asset('/css/semantic.min.css')); ?>" rel="stylesheet" />
      <link href="<?php echo e(asset('/components/icon.min.css')); ?>" rel="stylesheet" />
      <link href="<?php echo e(asset('/components/form.min.css')); ?>" rel="stylesheet" />
      <link href="<?php echo e(asset('/components/table.min.css')); ?>" rel="stylesheet" />

      <script src="<?php echo e(asset('/js/semantic.min.js')); ?>"></script>
      <script src="<?php echo e(asset('/components/form.js')); ?>"></script>
      <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    </style>
    <?php echo $__env->yieldContent('scripts'); ?>
    <?php echo $__env->yieldContent('css'); ?>
    </head>

    <body>
      <div class="ui grid">

          <?php echo $__env->make('layouts.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="eleven wide streched  column" style="padding-top:2em;">
          
              <h1><?php echo $__env->yieldContent('titulo_seccion'); ?></h1>

            <?php echo $__env->yieldContent('contenido'); ?>

        </div>


        <div class="column" style="padding-top:5em;">
          <?php echo $__env->yieldContent('botones'); ?>

        </div>

      </div>
    </body>

  </html>
