
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Login Example - Semantic</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/semantic.min.css')); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/components/icon.min.css')); ?>">
  <script src="<?php echo e(asset('/js/semantic.min.js')); ?>"></script>
  <script src="<?php echo e(asset('/components/form.min.js')); ?>"></script>
  <script src="<?php echo e(asset('/components/transition.min.js')); ?>"></script>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          inline:true,
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Ingresa tu correo electrónico'
                },
                {
                  type   : 'email',
                  prompt : 'Ingresa un correo válido'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Ingresa tu contraseña'
                },
                {
                  type   : 'length[6]',
                  prompt : 'La contraseña debe ser de 6 carácteres'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        BIENVENIDO AL PADRÓN DE BENEFICIARIOS
      </div>
    </h2>
    <form class="ui large form" method="POST" action="<?php echo e(url('/login')); ?>">
        <?php echo csrf_field(); ?>

      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="Correo Electrónico" value="<?php echo e(old('email')); ?>">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Constraseña">
          </div>
        </div>
        <div>
            <input type="checkbox" name="remember"> Recordar Datos
        </div>
        <br />
        <div class="ui fluid large pink submit button">Ingresar</div>
      </div>


    </form>
  </div>
</div>

</body>

</html>
