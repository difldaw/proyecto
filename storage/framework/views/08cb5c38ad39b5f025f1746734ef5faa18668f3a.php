<?php $path = Route::getCurrentRoute()->getPath(); $usr = Auth::user();?>


<div class="three wide  column">
    <div class="ui vertical inverted pointing  left  fluid   menu">

        <div class="ui card fluid" >
          <div class=" image" >
            <img src="<?php echo e(asset('/img/logo.png')); ?>">
          </div>
          <div class="content">
            <a href="<?php echo e(route('perfil',Auth::user()->id)); ?>"class="header"><?php echo e(Auth::user()->nombre); ?><br />  <?php echo e(Auth::user()->apellido_p); ?> <?php echo e(Auth::user()->apellido_m); ?></a>
            <div class="meta">
              <span class="date">
                <?php if(Auth::user()->tipo_usuario==1): ?>
                  Administrador
                <?php elseif(Auth::user()->tipo_usuario==2): ?>
                  Usuario Central
                <?php elseif(Auth::user()->tipo_usuario==3): ?>
                  Encargado(a) de <?php echo e(Auth::user()->municipio->nombre); ?>

                <?php endif; ?>
              </span>
            </div>
          </div>
        </div>



      <?php if($path == "/"): ?>
        <a href="<?php echo e(url('/')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('/')); ?>" class="item ">
      <?php endif; ?>
        <i class="home icon"></i> Inicio
      </a>

      <?php if (Gate::check('crud-usuarios')): ?>
        <?php if(str_contains($path,'usuario')): ?>
          <a href="<?php echo e(url('usuario')); ?>" class="item active">
        <?php else: ?>
          <a href="<?php echo e(url('usuario')); ?>" class="item ">
        <?php endif; ?>
          <i class="user icon"></i> Usuarios
        </a>
      <?php endif; ?>


      <?php if(str_contains($path,'beneficiario')): ?>
        <a href="<?php echo e(url('beneficiario')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('beneficiario')); ?>" class="item ">
      <?php endif; ?>
        <i class="child icon"></i> Beneficiarios
      </a>


      <?php if(str_contains($path,'localidad')): ?>
        <a href="<?php echo e(url('localidad')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('localidad')); ?>" class="item ">
      <?php endif; ?>
        <i class="marker icon"></i> Localidades
      </a>


      <?php if(str_contains($path,'provedor')): ?>
        <a href="<?php echo e(url('provedor')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('provedor')); ?>" class="item ">
      <?php endif; ?>
        <i class="shipping icon"></i> Provedores
      </a>


      <?php if(str_contains($path,'escuela')): ?>
        <a href="<?php echo e(url('escuela')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('escuela')); ?>" class="item ">
      <?php endif; ?>
        <i class="university icon"></i> Escuelas
      </a>



      <?php if (Gate::check('crud-productos')): ?>
        <?php if(str_contains($path,'producto')): ?>
          <a href="<?php echo e(url('producto')); ?>" class="item active">
        <?php else: ?>
          <a href="<?php echo e(url('producto')); ?>" class="item ">
        <?php endif; ?>
          <i class="book icon"></i>  Inventario Productos
        </a>
      <?php endif; ?>

      <?php if(str_contains($path,'programa')): ?>
        <a href="<?php echo e(url('programa')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('programa')); ?>" class="item ">
      <?php endif; ?>
        <i class="coffee icon"></i> Programas Alimenticios
      </a>



      <?php if(str_contains($path,'reporte')): ?>
        <a href="<?php echo e(url('reporte')); ?>" class="item active">
      <?php else: ?>
        <a href="<?php echo e(url('reporte')); ?>" class="item ">
      <?php endif; ?>
        <i class="line chart icon"></i> Reportes
      </a>

      <a href="<?php echo e(url('logout')); ?>" class="item">
        <i class="sign out icon"></i> Salir
      </a>




    </div>
  </div>
